﻿using AutoMapper;
using BusinessLogic.Modules;
using BusinessLogic.Modules.AgencyModule;
using BusinessLogic.Modules.CardModule;
using BusinessLogic.Modules.CarouselModule;
using BusinessLogic.Modules.LogModule;
using BusinessLogic.Modules.UnknownIntentModule;
using BusinessLogic.Modules.UserModule;
using Database;
using Database.Core;
using Database.Core.Repositories;
using Database.Persistence;
using Database.Persistence.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ApiCard
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddDbContext<CarlitosBnbContext>(o => o.UseSqlServer(this.Configuration.GetConnectionString("CarlitosBnb")));
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            //services.AddTransient<IService<LogInfo>, LogErrorService>();
            services.AddTransient<IService<UnknownIntentInfo>, RegisterUnknownIntentService>();
            services.AddTransient<IService<GetUserByChannelIdDto>, GetUserByChannelIdService>();
            services.AddTransient<IService<GetCarouseldDto>, GetCarouselByIntentService>();
            services.AddTransient<IService<AgenciesByDepartamentDto>, GetAgenciesByDepartamentService>();
            services.AddTransient<IService<AgencyDto>, GetAgenciesNearbyService>();
            services.AddTransient<IService<AtmDto>, GetAtmsNearbyService>();
            services.AddTransient<IService<AtmByDepartamentDto>, GetAtmsByDepartamentService>();
            services.AddTransient<IService<UserInfo>, SaveUserService>();
            services.AddTransient<IService<UserAcceptContractDto>, UserAcceptContractService>();
            services.AddTransient<IService<GetCarouseldDto>, GetCarouselByIntentService>();
            services.AddTransient<IService<GetCardDto>, GetCardByIntentService>();
            services.AddTransient<IService<CardInfo>, CreateCardService>();

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ICardRepository, CardRepository>();
            services.AddTransient<ICarouselRepository, CarouselRepository>();
            services.AddTransient<IAgencyRepository, AgencyRepository>();
            services.AddTransient<ILogRepository, LogRepository>();
            services.AddTransient<IUnknownIntentionRepository, UnknownIntentionRepository>();
            services.AddTransient<IApplicationRepository, ApplicationRepository>();
            services.AddTransient<IApplicationUserRepository, ApplicationUserRepository>();

            services.AddAutoMapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
