﻿using BusinessLogic.Modules;
using BusinessLogic.Modules.CardModule;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ApiCard.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class CardController : Controller
    {
        private readonly IService<CardInfo> cardService;

        public CardController(IService<CardInfo> cardService)
        {
            this.cardService = cardService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CardInfo cardInfo)
        {
            try
            {
                this.cardService.Data = cardInfo;
                var result = await this.cardService.Process();
                return Ok();
            }
            catch (Exception e)
            {
                return base.BadRequest(e);
            }
        }

        [HttpGet]
        public IActionResult Test()
        {
            return Ok(new { Server = "Ok" });
        }
    }
}