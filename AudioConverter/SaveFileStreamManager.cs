﻿namespace AudioConverter
{
    using System;
    using System.IO;

    class SaveFileStreamManager : FileStreamManager
    {
        private readonly byte[] bytes;

        private SaveFileStreamManager(byte[] bytes, string path) : base(path)
        {
            this.bytes = bytes;
        }

        public override State Process()
        {
            try
            {
                File.WriteAllBytes(this.Path, this.bytes);
                return State.Ok();
            }
            catch (Exception e)
            {
                return State.Fail(e.Message);
            }
        }

        public static State Save(byte[] bytes, string path)
        {
            return new SaveFileStreamManager(bytes, path).Process();
        }

        public static State Save(Stream stream, string path)
        {
            return Save(stream.ToByteArray(), path);
        }
    }
}
