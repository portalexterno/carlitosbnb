﻿namespace AudioConverter
{
    using System;
    using System.IO;

    public class AudioConverter
    {
        private readonly Stream inputAudio;
        private readonly string pathToFfmpeg;
        private readonly string pathToInput;
        private readonly string pathToOutput;

        private AudioConverter(Stream inputAudio, string inputExtension, string outputExtension, string pathToFfmpeg)
        {
            var folderAudioPath = Path.GetTempPath();
            var guid = Guid.NewGuid();
            this.inputAudio = inputAudio;
            this.pathToFfmpeg = pathToFfmpeg;
            this.pathToInput = $@"{folderAudioPath}/{guid}.{inputExtension}";
            this.pathToOutput = $@"{folderAudioPath}/{guid}.{outputExtension}";
        }

        private State convert(out Stream outputAudio)
        {
            outputAudio = null;
            var saverStream = SaveFileStreamManager.Save(this.inputAudio, this.pathToInput);

            if (!saverStream.IsOk)
            {
                this.cleanFiles();
                return State.Fail(saverStream.Message);
            }

            var audioConverter = ConvertManager.Execute(this.pathToInput, this.pathToOutput, this.pathToFfmpeg);
            if (!audioConverter.IsOk)
            {
                this.cleanFiles();
                return State.Fail(audioConverter.Message);
            }

            var readerStream = ReadFileStreamManager.Execute(this.pathToOutput, out outputAudio);
            if (!readerStream.IsOk)
            {
                this.cleanFiles();
                return State.Fail(readerStream.Message);
            }

            this.cleanFiles();
            return State.Ok();
        }

        private void cleanFiles()
        {
            DeleteFileStreamManager.Execute(this.pathToInput);
            DeleteFileStreamManager.Execute(this.pathToOutput);
        }

        public static State Convert(Stream inputAudio, string inputExtension, string outputExtension, string pathToFfmpeg, out Stream outputAudio)
        {
            return new AudioConverter(inputAudio, inputExtension, outputExtension, pathToFfmpeg).convert(out outputAudio);
        }
    }
}
