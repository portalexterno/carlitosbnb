﻿namespace AudioConverter
{
    using System;
    using System.Diagnostics;

    class ConvertManager : IFileStreamProcess
    {
        private readonly string pathToInput;
        private readonly string pathToOutput;
        private readonly string pathToffmpeg;

        private ConvertManager(string pathToInput, string pathToOutput, string pathToffmpeg)
        {
            this.pathToInput = pathToInput;
            this.pathToOutput = pathToOutput;
            this.pathToffmpeg = pathToffmpeg;
        }

        public State Process()
        {
            var ffmpeg = new Process
            {
                StartInfo = { UseShellExecute = false, RedirectStandardError = true, FileName = pathToffmpeg }
            };

            var arguments = $@"-i ""{this.pathToInput}"" ""{this.pathToOutput}""";
            ffmpeg.StartInfo.Arguments = arguments;

            try
            {
                if (!ffmpeg.Start())
                {
                    Debug.WriteLine("Error starting");
                    return State.Fail("Error starting");
                }

                var reader = ffmpeg.StandardError;
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    Debug.WriteLine(line);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return State.Fail(e.Message);
            }
            finally
            {
                ffmpeg.Close();
            }

            return State.Ok();
        }

        public static State Execute(string pathToInput, string pathToOutput, string pathToffmpeg)
        {
            return new ConvertManager(pathToInput, pathToOutput, pathToffmpeg).Process();
        }
    }
}
