﻿namespace AudioConverter
{
    using System;
    using System.IO;

    class ReadFileStreamManager : FileStreamManager
    {
        private ReadFileStreamManager(string path) : base(path)
        {
        }

        public Stream OutPut { get; private set; }

        public override State Process()
        {
            try
            {
                this.OutPut = File.OpenRead(this.Path);
                return State.Ok();
            }
            catch (Exception e)
            {
                return State.Fail(e.Message);
            }
        }

        public static State Execute(string path, out Stream stream)
        {
            var manager = new ReadFileStreamManager(path);
            var state = manager.Process();
            stream = manager.OutPut;
            return state;
        }
    }
}
