﻿namespace AudioConverter
{
    using System;
    using System.IO;

    class DeleteFileStreamManager : FileStreamManager
    {
        private DeleteFileStreamManager(string path) : base(path)
        {
        }

        public override State Process()
        {
            try
            {
                if (File.Exists(this.Path))
                    File.Delete(this.Path);

                return State.Ok();
            }
            catch (Exception e)
            {
                return State.Fail(e.Message);
            }
        }

        public static State Execute(string path)
        {
            return new DeleteFileStreamManager(path).Process();
        }
    }
}
