﻿namespace AudioConverter
{
    abstract class FileStreamManager : IFileStreamProcess
    {
        public string Path { get; protected set; }

        public FileStreamManager(string path)
        {
            this.Path = path;
        }

        public abstract State Process();
    }
}
