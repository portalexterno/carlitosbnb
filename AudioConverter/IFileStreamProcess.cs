﻿namespace AudioConverter
{
    interface IFileStreamProcess
    {
        State Process();
    }
}
