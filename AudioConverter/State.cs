﻿namespace AudioConverter
{
    public class State
    {
        private State(bool isOk, string message = null)
        {
            this.IsOk = isOk;
            this.Message = message;
        }

        public bool IsOk { get; private set; }

        public string Message { get; private set; }

        public static State Ok()
        {
            return new State(true);
        }

        public static State Fail(string message = null)
        {
            return new State(false, message);
        }
    }
}
