﻿namespace CarlitosBnb
{
    using CarlitosBnb.Dialogs;
    using CarlitosBnb.Dialogs.AgenciesAndAtm.Agencies;
    using CarlitosBnb.Dialogs.AgenciesAndAtm.Atm;
    using CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Balances;
    using CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements;
    using CarlitosBnb.Dialogs.BnbPoints;
    using CarlitosBnb.Dialogs.CreditCardExtract;
    using CarlitosBnb.Dialogs.EnableDisableInternetPayments;
    using CarlitosBnb.Dialogs.ExchangeRate;
    using CarlitosBnb.Dialogs.LockUnlockCard;
    using CarlitosBnb.Dialogs.Survey;
    using CarlitosBnb.Middlewares.DialogFlow;
    using Google.Cloud.Dialogflow.V2;
    using Microsoft.Bot;
    using Microsoft.Bot.Builder;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class CreditCardExtractInfo
    {
        public string CreditCardSelected { get; set; }

        public string Period { get; set; }
    }

    //public class Session
    //{
    //    //[JsonProperty("LastSession")]
    //    public DateTime? LastSession { get; set; }

    //    public int MyProperty { get; set; }

    //    public static Session Create(DateTime lastSession)
    //    {
    //        return new Session()
    //        {
    //            LastSession = lastSession,
    //            MyProperty = 8
    //        };
    //    }
    //}

    public class ProfileSession
    {
        public string DocumentNumber { get; set; }

        public string DocumentExtension { get; set; }

        public string DocumentComplement { get; set; }

        public List<Dialogs.Common.Client> Clients { get; set; }
        public string Action { get; set; }

        public bool HasSession
        {
            get
            {
                if (this.Clients == null)
                {
                    return false;
                }

                return this.Clients.Count > 0 && !string.IsNullOrEmpty(this.DocumentNumber);
            }
        }

        public long DocumentNumberLong
        {
            get
            {
                return string.IsNullOrEmpty(this.DocumentNumber) ? 0 : long.Parse(this.DocumentNumber);
            }
        }

        public string CardId { get; set; }
        public List<string> ActiveCardIdList { get; set; }

        public bool HasCreditCards { get; set; }
    }

    public class EnableInternetPaymentData
    {
        public EnableInternetPaymentData()
        {
            this.Card = new CardResult();
        }

        public CardResult Card { get; set; }

        public string DebitAccount { get; set; }

        public decimal Amount { get; set; }

        public string DateInitial { get; set; }

        public string DateEnd { get; set; }
    }

    public class CarlitosBnbBot : IBot
    {
        public const string MAIN = "MainId";

        public const string CONFIGURATION_ID = "configuration";

        public const string SERVICE_PROVIDER_ID = "serviceProvider";

        public CarlitosBnbBot(IServiceProvider serviceProvider, IConfiguration configuration, IHttpClientFactory httpClientFactory)
        {
            this.serviceProvider = serviceProvider;
            this.configuration = configuration;
            this.httpClientFactory = httpClientFactory;
        }

        private IServiceProvider serviceProvider;

        private IConfiguration configuration;
        private readonly IHttpClientFactory httpClientFactory;

        private DialogSet dialogs { get; } = ComposeMainDialog();

        public async Task OnTurn(ITurnContext turnContext)
        {
            if (turnContext.Activity.Type == ActivityTypes.Message)
            {
                turnContext.Services.Add(CONFIGURATION_ID, configuration);
                turnContext.Services.Add("HttpClientFactory", this.httpClientFactory);
                var resultDialogFlow = turnContext.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);
                var conversation = ConversationState<ConversationInfo>.Get(turnContext);
                var dialogContainer = this.dialogs.CreateContext(turnContext, conversation);
                dialogContainer.Context.Services.Add(SERVICE_PROVIDER_ID, serviceProvider);

                //await turnContext.SendActivity($"From Id: {turnContext.Activity.From.Id} Name: {turnContext.Activity.From.Name}");
                //await turnContext.SendActivity($"Recipient Id: {turnContext.Activity.Recipient.Id} Name: {turnContext.Activity.Recipient.Name}");
                //await turnContext.SendActivity($"ChannelId: {turnContext.Activity.ChannelId} ConversationId: {turnContext.Activity.Conversation.Id}");
                //if (!conversation.ContainsKey("dialogStack") || (conversation["dialogStack"] == null) || (conversation["dialogStack"] as List<DialogInstance>).Count <= 0)
                if (turnContext.Responded || (resultDialogFlow.Intent != null && !string.IsNullOrWhiteSpace(conversation.DialogflowTextResponse)) || (resultDialogFlow.Parameters != null && resultDialogFlow.Parameters.Fields.ContainsKey("Dialog")))
                {
                    if (conversation.IsContractAccepted)
                    {
                        await dialogContainer.Begin(MAIN);
                    }
                    else
                    {
                        await dialogContainer.Begin(ContractDialogContainer.Id);
                    }
                }
                else
                {

                    await dialogContainer.Continue();
                }
            }
        }

        //static CancellationTokenSource CancelToken = new CancellationTokenSource();
        //static Task Delay = Task.Delay(15 * 1000, CancelToken.Token);

        private static DialogSet ComposeMainDialog()
        {
            var dialogs = new DialogSet();

            dialogs.Add("MainId", new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    var result = dc.Context.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);
                    var dialog = DialogFlowDialogContainer.Id;
                    if(result.Parameters != null && result.Parameters.Fields.ContainsKey("Dialog"))
                    {
                        dialog = result.Parameters.Fields["Dialog"].StringValue;
                    }

                    await dc.Begin(dialog, dc.ActiveDialog.State);

                    //if(!string.IsNullOrWhiteSpace(result.FulfillmentText))
                    //{
                    //    CancelToken.Cancel();
                    //}

                    //await Delay.ContinueWith(t => {
                    //    dc.Replace(SurveyDialogContainer.Id);
                    //});                    
                }
            });

            dialogs.Add(ContractDialogContainer.Id, ContractDialogContainer.Instance);
            dialogs.Add(CarouselDialogContainer.Id, CarouselDialogContainer.Instance);
            dialogs.Add(CardDialogContainer.Id, CardDialogContainer.Instance);
            dialogs.Add(DialogFlowDialogContainer.Id, DialogFlowDialogContainer.Instance);
            dialogs.Add(BalanceDialogContainerV2.Id, BalanceDialogContainerV2.Instance);
            dialogs.Add(MovementsDialogContainerV2.Id, MovementsDialogContainerV2.Instance);
            dialogs.Add(AgencyDialogContainer.Id, AgencyDialogContainer.Instance);
            dialogs.Add(AgenciesDepartamentDialogContainer.Id, AgenciesDepartamentDialogContainer.Instance);
            dialogs.Add(AtmDialogContainer.Id, AtmDialogContainer.Instance);
            dialogs.Add(AtmsDepartamentDialogContainer.Id, AtmsDepartamentDialogContainer.Instance);
            dialogs.Add(ExchangeRateDialogContainer.Id, ExchangeRateDialogContainer.Instance);
            dialogs.Add(LockUnlockCardDialogContainer.Id, LockUnlockCardDialogContainer.Instance);
            dialogs.Add(SurveyDialogContainer.Id, SurveyDialogContainer.Instance);
            dialogs.Add(EnableDisableInternetPaymentsContainer.Id, EnableDisableInternetPaymentsContainer.Instance);
            dialogs.Add(BnbPointsContainer.Id, BnbPointsContainer.Instance);
            dialogs.Add(CreditCardExtractContainer.Id, CreditCardExtractContainer.Instance);
            return dialogs;
        }
    }
}