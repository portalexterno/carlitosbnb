﻿namespace CarlitosBnb.Dialogs
{
    using BusinessLogic.Modules;
    using BusinessLogic.Modules.CarouselModule;
    using CarlitosBnb.Core;
    using CarlitosBnb.Core.Factories;
    using CarlitosBnb.Middlewares.DialogFlow;
    using Google.Cloud.Dialogflow.V2;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;

    public class CarouselDialogContainer : DialogContainer
    {
        public const string Id = "Carousel";

        public static CarouselDialogContainer Instance { get; } = new CarouselDialogContainer();

        private CarouselDialogContainer() : base(Id)
        {
            this.Dialogs.Add(Id, new WaterfallStep[]
            {
                async (dc, args, next) =>
                {
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var queryResult = dc.Context.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);

                    var result = await ManagerScope.Process(dc.Context, new GetCarouseldDto(){
                        Intent = queryResult.Intent.IntentName.IntentId
                    });

                    if(!result.IsOk)
                    {
                        await dc.Context.SendActivity("Lo siento no puedo obtener el carousel en este momento");
                        //conversationInfo.Clear();
                        await dc.End();
                        return;
                    }

                    var resultCarousel = result as Result<CarouselInfo>;

                    var creator = new CarouselCreator();
                    var message = creator.Create(resultCarousel.Body);
                    await dc.Context.SendActivity(message);
                    //conversationInfo.Clear();
                    await dc.End();
                }
            });
        }
    }
}
