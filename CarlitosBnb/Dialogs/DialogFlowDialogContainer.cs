﻿namespace CarlitosBnb.Dialogs
{
    using BusinessLogic.Modules;
    using BusinessLogic.Modules.CardModule;
    using CarlitosBnb.Core;
    using CarlitosBnb.Core.Factories;
    using CarlitosBnb.Middlewares.DialogFlow;
    using Google.Cloud.Dialogflow.V2;
    using Microsoft.Bot.Builder;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using System.Threading.Tasks;

    public class DialogFlowDialogContainer : DialogContainer
    {
        public const string Id = "DialogFlow";

        public const string MESSAGE_NOT_RESPONSE = "Ups no comprendo esa referencia, Te mostraré las opciones que tengo para ti.";

        public static DialogFlowDialogContainer Instance { get; } = new DialogFlowDialogContainer();

        private DialogFlowDialogContainer() : base(Id)
        {
            this.Dialogs.Add(Id, new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var result = dc.Context.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);
                    //conversationInfo.DialogflowTextResponse = result.FulfillmentText;
                    if(string.IsNullOrEmpty(result.FulfillmentText))
                    {
                        await dc.Context.SendActivity(MESSAGE_NOT_RESPONSE);
                        var resultGetCard = await ManagerScope.Process(dc.Context, new GetCardDto(){
                            Intent = "b8fe65ab-ebee-44c3-a715-6bce21e7e7c4"
                        });

                        if(!resultGetCard.IsOk)
                        {
                            await dc.Context.SendActivity("Ups, al parecer no pude encontrar opciones, por favor intenta nuevamente");
                            //conversationInfo.Clear();
                            await dc.End();
                            return;
                        }

                        var card = (resultGetCard as Result<CardInfo>).Body;
                        var creator = new CardCreator();
                        creator.UserName = dc.Context.Activity.From.FirstName();
                        var message = creator.Create(card);
                        await dc.Context.SendActivity(message);
                    }
                    else
                    {
                        //if(result.FulfillmentText == "Hola, en que puedo ayudarte?")
                        //{
                        //    await System.Threading.Tasks.Task.Delay(5*1000);
                        //    await dc.Context.SendActivity("TImer finished!");
                        //}

                        await dc.Context.SendActivity(result.FulfillmentText);
                    }

                    //conversationInfo.Clear();
                    //await dc.End();
                    dc.EndAll();
                }


            });
        }

        private async Task SendMessageAsync(ITurnContext turnContext)
        {
            await turnContext.SendActivity($"Timer finished!");
        }
    }
}
