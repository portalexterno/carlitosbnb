﻿namespace CarlitosBnb.Dialogs.LockUnlockCard
{
    public class RequestDebitCardLockRequest
    {
        public string AppKey { get; set; }

        public long Document { get; set; }
    }
}
