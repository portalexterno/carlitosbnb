﻿namespace CarlitosBnb.Dialogs.LockUnlockCard
{
    using CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers;
    using CarlitosBnb.SeedWorks;
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Builder.Prompts.Choices;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    public class VerifyOtpWaterfallSteps
    {
        public const string Id = "VerifyOtp";

        public string Type { get; set; }

        private bool isLock { get { return Type == "LOCK"; } }

        public struct Texts
        {
            public const string YOU_WANT_TO_CONTINUE = "¿Desea continuar?";

            public const string INVALID_OTP_CODE = "Disculpa este no parece ser un código valido.";

            public const string FAILED_VALIDATE_OTP_CODE = "Disculpa, no pude validar el código, por favor intenta más tarde";

            public const string VALID_OTP_CODE = "El código se validó con éxito. La tarjeta ha sido bloqueada";

            public const string INPUT_OTP_CODE = "Se envió un código al número de celular registrado, por favor ingresa el código enviado";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string MAX_RETRY_VALIDATE_OTP_CODE = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";
        }

        public static VerifyOtpWaterfallSteps Instance { get; } = new VerifyOtpWaterfallSteps();

        private struct Inputs
        {
            public const string OTP_CODE = "optCode";
            public const string SELECT_FOR_CONTINUE = "selectForContinue";
        }

        public WaterfallStep[] WaterFalls => new WaterfallStep[]
            {
                this.InputOtp,
                this.VerifyOtp,
                this.RetryOtp
            };


        private async Task InputOtp(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            dc.Context.GetConversationState<ConversationInfo>().HasExpectedResponse();
            await dc.Prompt(Inputs.OTP_CODE, Texts.INPUT_OTP_CODE, new PromptOptions()
            {
                RetryPromptString = Texts.INVALID_OTP_CODE
            });
        }

        private async Task VerifyOtp(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {

            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            //var clients = conversationInfo.Clients;

            var otpCode = conversationInfo.IsAudio ? new string(args["Value"].ToString().Where(c => char.IsDigit(c)).ToArray())
                : args["Value"].ToString();

            if (!Regex.IsMatch(otpCode, BalancesDialogContainer.PATTERN_OTP_CODE))
            {
                await this.actionInvalidDocument(dc, conversationInfo, otpCode, configuration);
                return;
            }


            //Llamar al servicio
            var response = await Connection.Process(
                new ConnectorRestPost<CompleteDebitCardTempLockRequest, BaseApiResponse<string>>(
                    HttpClientFactory.Create(),
                    configuration.GetValue<string>(isLock ? "Services:CompleteDebitCardTempLock" : "Services:CompleteDebitCardTempUnLock"),
                    new CompleteDebitCardTempLockRequest()
                    {
                        AppKey = "xyzudkk125yujks52*01",
                        Document = conversationInfo.ProfileSession.DocumentNumberLong,
                        Otp = int.Parse(otpCode)
                    }
                    ));

            if (!response.IsOk)
            {
                await dc.Context.SendActivity("Tuve un problema al realizar tu solicitud, por favor intenta nuevamente");
                dc.EndAll();
                return;
            }

            var responseBaseApiResponse = response as Response<BaseApiResponse<string>>;
            if (!responseBaseApiResponse.Body.Success)
            {
                await dc.Context.SendActivity("Tuve un problema al realizar tu solicitud, por favor intenta nuevamente");
                dc.EndAll();
                return;
            }

            await dc.Context.SendActivity(responseBaseApiResponse.Body.Body);
            await dc.Context.SendActivity($"{dc.Context.Activity.From.FirstName()}, dime en que más te puedo ayudar.");
            dc.EndAll();
            conversationInfo.IsCompletedTask = true;
        }

        private async Task RetryOtp(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            //if(args.ContainsKey("Activity"))
            //{

            //    return;
            //}

            //var choice = (FoundChoice)args["Value"];
            var value = (Activity)args["Activity"];

            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            //var client = conversationInfo["clients"] as Clients;

            //if (choice.Value == "YES")
            if (value.Text == "YES" || value.Text.ToLower() == "si")
            {
                await dc.Replace(Id, dc.ActiveDialog.State);
            }
            else
            {
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                await dc.End();
            }
        }

        private async Task actionInvalidDocument(DialogContext dc, ConversationInfo conversationInfo, string otpCode, IConfiguration configuration)
        {
            var options = new List<Choice>()
            {
                new Choice()
                {
                    Action = new CardAction(ActionTypes.PostBack, "Si deseo continuar", value: "YES"),
                    Value = "YES",
                    Synonyms = new List<string>{ "Si", "si", "Sí", "sí" }
                },
                new Choice()
                {
                    Action = new CardAction(ActionTypes.PostBack, "No", value: "NO"),
                    Value = "NO",
                    Synonyms = new List<string>{ "No", "no" }
                }
            };

            var actions = new List<CardAction>()
                {
                    new CardAction(type: ActionTypes.PostBack, displayText: "Si deseo continuar" , value: "YES"),
                    new CardAction(type: ActionTypes.PostBack, displayText: "No" , value: "NO"),
                };

            conversationInfo.CounterAttemptsOtpCode++;

            if (conversationInfo.CounterAttemptsOtpCode >= configuration.GetValue<int>("MaxTryCounterOtpCode"))
            {
                await dc.Context.SendActivity(Texts.INVALID_OTP_CODE);
                await dc.Context.SendActivity(Texts.MAX_RETRY_VALIDATE_OTP_CODE);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                conversationInfo.CounterAttemptsOtpCode = 0;
                await dc.End();
                return;
            }

            //var act = Activity.CreateMessageActivity();
            //act.Text = $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }";

            //act.SuggestedActions = new SuggestedActions()
            //{
            //    Actions = new List<CardAction>()
            //    {
            //        new CardAction(type: ActionTypes.PostBack, displayText: "Si deseo continuar" , value: "YES"),
            //        new CardAction(type: ActionTypes.PostBack, displayText: "No" , value: "NO"),
            //    }
            //};

            //await dc.Context.SendActivity(act);
            //var prompt = new Microsoft.Bot.Builder.Prompts.ChoicePrompt();

            //var choices = MessageFactory.SuggestedActions(actions, $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }");
            var choices = ChoiceFactory.SuggestedAction(options, $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }", $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }");
            await dc.Context.SendActivity(choices);


            //await dc.Prompt(BalancesDialogContainer.Inputs.SELECT_FOR_CONTINUE, $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }", new ChoicePromptOptions()
            //{
            //    Choices = options,
            //    RetryPromptActivity = (Activity)act,
            //    RetryPromptString = null,
            //    RetrySpeak = null
            //    //RetryPromptString = $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE } Si/No" + (conversationInfo.IsAudio ? $"Entendí que dijiste {otpCode}" : string.Empty)
            //});
        }
    }
}
