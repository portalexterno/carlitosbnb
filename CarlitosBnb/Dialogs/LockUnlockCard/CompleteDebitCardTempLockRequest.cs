﻿
namespace CarlitosBnb.Dialogs.LockUnlockCard
{
    public class CompleteDebitCardTempLockRequest
    {
        public string AppKey { get; set; }

        public long Document { get; set; }

        public int Otp { get; set; }
    }
}
