﻿using CarlitosBnb.Dialogs.Common;
using CarlitosBnb.SeedWorks;
using Core.Connectors;
using Core.Connectors.Core;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Prompts.Choices;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.LockUnlockCard
{
    public class LoginWaterfallSteps
    {
        public const string Id = "LoginWaterfall";

        private const string PATTERN_DOCUMENT = @"^([0-9]{5,9})$";

        public const string CARD_ID = @"^([0-9]{2,9})$";

        public string Type { get; set; }

        private bool isLock { get { return Type == "LOCK"; } }

        public static LoginWaterfallSteps Instance { get; } = new LoginWaterfallSteps();

        private struct Inputs
        {
            public const string INPUT_DOCUMENT = "inputDocument";

            public const string SELECT_FOR_CONTINUE = "selectForContinue";

            public const string INPUT_CARD_ID = "inputCardId";
        }

        public struct Texts
        {
            public const string NOT_FOUND_BALANCE = "No encontré el saldo de tus cuentas, por favor intenta más tarde";

            public const string YOU_WANT_TO_CONTINUE = "¿Desea continuar?";

            public const string INVALID_DOCUMENT = "Disculpa este no parece ser un ci válido";

            public const string INPUT_DOCUMENT = "¡Ok! por favor, ingresa tu ci";

            public const string FAILED_REST_BALANCES = "Disculpa, pero no puedo obtener tu saldo en este momento, por favor intenta en un momento";

            public const string NOT_FOUND_OWNER_ACCOUNTS = "No encontré tus cuentas, por favor intenta más tarde.";

            public const string NOT_FOUND_ACCOUNTS_BY_DOCUMENT = "No se encontraron cuentas, con el ci especificado";

            public const string UPDATE_DATA_CONTACT = "Por favor comunícate con el contact center (78787272) del Banco para actualizar tu información.";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string MAX_RETRY_VALIDATE_DOCUMENT = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";

            public const string INPUT_CARD_ID = "Adicionalmente, favor ingresa los {0} últimos dígitos de tu tarjeta";

            public const string FAILED_LOGIN = "Disculpa, pero no puedo validar tu sesión ahora, por favor intenta en un momento";

            public const string INVALID_CARD_ID = "Disculpa, no parecen ser dígitos válidos";

            public const string MAX_RETRY_VALIDATE_CARD_ID = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";
        }

        public WaterfallStep[] WaterFalls => new WaterfallStep[]
            {
                this.InputDocument,
                this.ValidateDocument,
                this.RetryValidateDocument,
                this.InputCardId,
                this.ValidateCardId,
                this.RetryValidateCardId
            };


        private async Task InputDocument(DialogContext dc, object args, SkipStepFunction next)
        {
            await dc.Prompt(Inputs.INPUT_DOCUMENT, Texts.INPUT_DOCUMENT);
        }

        private async Task ValidateDocument(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var document = new string(args["Value"].ToString().Where(c => char.IsDigit(c)).ToArray());
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

            conversationInfo.ProfileSession.DocumentNumber = document;

            if (!Regex.IsMatch(document, PATTERN_DOCUMENT))
            {
                var options = new List<Choice>()
                {
                    new Choice()
                    {
                        Action = new CardAction(title: "Sí deseo continuar", type: ActionTypes.PostBack, value: "YES"),
                        //Value = PromptValue.Create(value: "YES", isNotUnknownIntent: true)
                        Value = "YES",
                        Synonyms = new List<string>{ "Si", "Sí", "si" }
                    },
                    new Choice()
                    {
                        Action = new CardAction(title: "No", type: ActionTypes.PostBack, value: "NO"),
                        //Value = PromptValue.Create(value: "NO", isNotUnknownIntent: true)
                        Value = "NO",
                        Synonyms = new List<string>{ "No", "no" }
                    }
                };

                conversationInfo.CounterAttemptsLogin++;

                if (conversationInfo.CounterAttemptsLogin >= configuration.GetValue<int>("MaxTryCounterLogin"))
                {
                    await dc.Context.SendActivity(Texts.INVALID_DOCUMENT);
                    await dc.Context.SendActivity(Texts.MAX_RETRY_VALIDATE_DOCUMENT);
                    await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                    conversationInfo.CounterAttemptsLogin = 0;
                    await dc.End();
                    return;
                }

                await dc.Prompt(Inputs.SELECT_FOR_CONTINUE, $"{ Texts.INVALID_DOCUMENT }, { Texts.YOU_WANT_TO_CONTINUE } ", new ChoicePromptOptions()
                {
                    Choices = options,
                    RetryPromptActivity = ChoiceFactory.SuggestedAction(options, "Por favor escriba/seleccione una de las opciones: Si/No") as Activity
                    //RetryPromptString = $"{ Texts.INVALID_DOCUMENT }, { Texts.YOU_WANT_TO_CONTINUE } " + (conversationInfo.IsAudio ? $"Entendí que dijiste {document}" : string.Empty)
                });

                return;
            }

            await next();
        }

        private async Task RetryValidateDocument(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            if (args == null)
            {
                await next(args);
                return;
            }

            var choice = (FoundChoice)args["Value"];

            if (choice.Value == "YES")
            {
                await dc.Replace(Id, dc.ActiveDialog.State);
            }
            else
            {
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                await dc.End();
            }
        }

        private async Task InputCardId(DialogContext dc, object args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            //if (HasSession(dc))
            //{
            //    await next();
            //    return;
            //}

            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

            var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("AuthenticationServices");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);

            var responseCardIdUser = await Connection.Process(new ConnectorRestPost<RequestGetCardIdUser, BaseApiResponse<ResponseGetCardIdUser>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:GetCardIdUser"), new RequestGetCardIdUser()
            {
                DocumentNumber = conversationInfo.ProfileSession.DocumentNumber,
                DocumentComplement = string.Empty
            }));

            if (!responseCardIdUser.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_LOGIN);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            var response = responseCardIdUser as Response<BaseApiResponse<ResponseGetCardIdUser>>;
            conversationInfo.ProfileSession.ActiveCardIdList = response.Body.Body.CardIdList;
            conversationInfo.CounterAttemptsLogin = 0;

            if (conversationInfo.ProfileSession.ActiveCardIdList.Count == 0)
            {
                await next();
                return;
            }

            await dc.Prompt(Inputs.INPUT_CARD_ID, string.Format(Texts.INPUT_CARD_ID, configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:CardIdUserDigitNumber")));
        }

        private async Task ValidateCardId(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

            if (conversationInfo.ProfileSession.ActiveCardIdList.Count > 0)
            {
                conversationInfo.ProfileSession.CardId = new string(args["Value"].ToString().Where(c => char.IsDigit(c)).ToArray());

                bool ValidCardId = IsValidCardId(conversationInfo.ProfileSession.CardId, Convert.ToInt32(configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:CardIdUserDigitNumber")), conversationInfo.ProfileSession.ActiveCardIdList);

                if (!Regex.IsMatch(conversationInfo.ProfileSession.CardId, CARD_ID) || !ValidCardId)
                {
                    var options = new List<Choice>()
                    {
                        new Choice()
                        {
                            Action = new CardAction(title: "Sí deseo continuar", type: ActionTypes.PostBack, value: "YES"),
                            Value = "YES",
                            Synonyms = new List<string>{ "Si", "Sí", "si" }
                        },
                        new Choice()
                        {
                            Action = new CardAction(title: "No", type: ActionTypes.PostBack, value: "NO"),
                            Value = "NO",
                            Synonyms = new List<string>{ "No", "no" }
                        }
                    };

                    conversationInfo.CounterAttemptsLogin++;

                    if (conversationInfo.CounterAttemptsLogin >= configuration.GetValue<int>("MaxTryCounterLogin"))
                    {
                        await dc.Context.SendActivity(Texts.INVALID_CARD_ID);
                        await dc.Context.SendActivity(Texts.MAX_RETRY_VALIDATE_CARD_ID);
                        await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                        conversationInfo.CounterAttemptsLogin = 0;
                        await dc.End();
                        return;
                    }

                    await dc.Prompt(Inputs.SELECT_FOR_CONTINUE, $"{ Texts.INVALID_CARD_ID }, { Texts.YOU_WANT_TO_CONTINUE } ", new ChoicePromptOptions()
                    {
                        Choices = options,
                        RetryPromptActivity = ChoiceFactory.SuggestedAction(options, "Por favor escriba/seleccione una de las opciones: Si/No") as Activity
                    });

                    return;
                }
            }

            Response response;
            if (!configuration.GetValue<bool>("Dev:UseHotCodeAuthenticacion"))
            {
                //Llamar al servicio
                response = await Connection.Process(
                new ConnectorRestPost<RequestDebitCardLockRequest, BaseApiResponse<string>>(
                    HttpClientFactory.Create(),
                    configuration.GetValue<string>(isLock ? "Services:RequestDebitCardLock" : "Services:RequestDebitCardUnLock"),
                    new RequestDebitCardLockRequest()
                    {
                        AppKey = "xyzudkk125yujks52*01",
                        Document = long.Parse(conversationInfo.ProfileSession.DocumentNumber)
                    }
                    ));
            }
            else
            {
                response = Response.Ok(new BaseApiResponse<string>()
                {
                    Success = true,
                    Body = "El ci se validó con exito, Te enviaré un código de verificación al teléfono:, 77777777"
                });
            }

            if (!response.IsOk)
            {
                await dc.Context.SendActivity("Tuve un problema al realizar tu solicitud, por favor intenta nuevamente");
                await dc.End();
                return;
            }

            var responseBaseApiResponse = response as Response<BaseApiResponse<string>>;
            if (!responseBaseApiResponse.Body.Success)
            {
                await dc.Context.SendActivity("No se pudo completar tu solicitud");
                await dc.End();
                return;
            }

            var originalMessage = responseBaseApiResponse.Body.Body;

            var phone = $"****{ originalMessage.Substring(originalMessage.Length - 4, 4) }";
            var message = $"Te enviaré un código de verificación al teléfono: { phone }.";
            await dc.Context.SendActivity(message);
            await dc.Replace(VerifyOtpWaterfallSteps.Id, dc.ActiveDialog.State);

        }

        private async Task RetryValidateCardId(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (conversationInfo.ProfileSession.ActiveCardIdList.Count == 0)
            {
                await next();
                return;
            }

            if (args == null)
            {
                await next(args);
                return;
            }

            var choice = (FoundChoice)args["Value"];

            if (choice.Value == "YES")
            {
                await dc.Replace(Id, dc.ActiveDialog.State);
            }
            else
            {
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
            }
        }

        private bool IsValidCardId(string inputValue, int outputLenght, List<string> inputCollection)
        {
            foreach (string item in inputCollection)
            {
                if (item.Substring(item.Length - outputLenght).Equals(inputValue)) return true;
            }
            return false;
        }
    }
}
