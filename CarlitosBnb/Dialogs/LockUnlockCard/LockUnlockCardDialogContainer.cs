﻿using CarlitosBnb.Middlewares.DialogFlow;
using Google.Cloud.Dialogflow.V2;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Prompts;
using Microsoft.Extensions.Configuration;
using Microsoft.Recognizers.Text;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.LockUnlockCard
{
    public class LockUnlockCardDialogContainer : DialogContainer
    {
        public const string Id = "LockUnlockCard";

        public static LockUnlockCardDialogContainer Instance { get; } = new LockUnlockCardDialogContainer();

        private struct Inputs
        {
            public const string INPUT_DOCUMENT = "inputDocument";

            public const string SELECT_FOR_CONTINUE = "selectForContinue";

            public const string OTP_CODE = "optCode";

            public const string INPUT_CARD_ID = "inputCardId";
        }

        public struct Texts
        {
            public const string NOT_FOUND_BALANCE = "No encontré el saldo de tus cuentas, por favor intenta más tarde";

            public const string YOU_WANT_TO_CONTINUE = "¿Desea continuar?";

            public const string INVALID_DOCUMENT = "Disculpa este no parece ser un ci válido";

            public const string INPUT_DOCUMENT = "¡Ok! por favor, ingresa tu ci";

            public const string FAILED_REST_BALANCES = "Disculpa, pero no puedo obtener tu saldo en este momento, por favor intenta en un momento";

            public const string NOT_FOUND_OWNER_ACCOUNTS = "No encontré tus cuentas, por favor intenta más tarde.";

            public const string NOT_FOUND_ACCOUNTS_BY_DOCUMENT = "No se encontraron cuentas, con el ci especificado";

            public const string UPDATE_DATA_CONTACT = "Por favor comunícate con el contact center (78787272) del Banco para actualizar tu información.";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string MAX_RETRY_VALIDATE_DOCUMENT = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";
        }

        private LockUnlockCardDialogContainer() : base(Id)
        {
            this.Dialogs.Add(Id,
                new WaterfallStep[]
                {
                    async(dc, args, next) =>
                    {
                        var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                        var result = dc.Context.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);

                        if (result.Parameters.Fields.ContainsKey("Type"))
                        {
                            LoginWaterfallSteps.Instance.Type = result.Parameters.Fields["Type"].StringValue;
                            VerifyOtpWaterfallSteps.Instance.Type = result.Parameters.Fields["Type"].StringValue;
                        }

                        conversationInfo.ProfileSession.Action = result.Parameters.Fields["Type"].StringValue;
                        await dc.Replace(LoginWaterfallSteps.Id, dc.ActiveDialog.State);
                    }
                }
            );

            this.Dialogs.Add(Inputs.INPUT_DOCUMENT, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(Inputs.SELECT_FOR_CONTINUE, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish, ValidateChoice));
            this.Dialogs.Add(Inputs.OTP_CODE, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(Inputs.INPUT_CARD_ID, new Microsoft.Bot.Builder.Dialogs.TextPrompt());

            this.Dialogs.Add(LoginWaterfallSteps.Id, LoginWaterfallSteps.Instance.WaterFalls);
            this.Dialogs.Add(VerifyOtpWaterfallSteps.Id, VerifyOtpWaterfallSteps.Instance.WaterFalls);
        }

        private async Task ValidateChoice(ITurnContext context, ChoiceResult toValidate)
        {
            var conversationInfo = context.GetConversationState<ConversationInfo>();
            conversationInfo.CounterAttemptsOtpCode++;
            var configuration = context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            //await context.SendActivity($"{conversationInfo.CounterAttemptsOtpCode} intentos");
            if (conversationInfo.CounterAttemptsOtpCode >= configuration.GetValue<int>("MaxTryCounterOtpCode"))
            {
                await context.SendActivity(VerifyOtpWaterfallSteps.Texts.INVALID_OTP_CODE);
                await context.SendActivity(VerifyOtpWaterfallSteps.Texts.MAX_RETRY_VALIDATE_OTP_CODE);
                await context.SendActivity(string.Format(VerifyOtpWaterfallSteps.Texts.CANCEL, context.Activity.From.FirstName()));
                conversationInfo.CounterAttemptsOtpCode = 0;
                return;
            }

            var userMessage = context.Activity.Text;
            if (userMessage == "3")
            {
                toValidate.Status = null;
                await Task.Delay(1);
            }
        }
    }
}
