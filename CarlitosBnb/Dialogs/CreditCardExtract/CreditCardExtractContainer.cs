﻿//using BusinessLogic.Modules.LogModule;
using CarlitosBnb.Core;
using CarlitosBnb.Dialogs.Common;
using CarlitosBnb.SeedWorks;
using Core.Connectors;
using Core.Connectors.Core;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Prompts.Choices;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Recognizers.Text;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.CreditCardExtract
{
    public class CreditCardExtractContainer : DialogContainer
    {
        public const string Id = "CreditCardExtract";

        public struct Inputs
        {
            public const string SELECT_CREDIT_CARD = nameof(SELECT_CREDIT_CARD);

            public const string SELECT_PERIOD = nameof(SELECT_PERIOD);
        }

        public struct Texts
        {
            public const string FAILED_REST_CARD = "Ocurrió un problema al intentar obtener su extracto";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";
        }

        public static CreditCardExtractContainer Instance { get; } = new CreditCardExtractContainer();

        public CreditCardExtractContainer() : base(Id)
        {
            var waterFallSteps = new List<WaterfallStep>();
            waterFallSteps.AddRange(UserProfileDocumentWaterfallSteps.Instance.WaterfallStepsOnlyDocumentNumber);
            waterFallSteps.AddRange(AuthenticationWaterfallSteps.Instance.WaterFalls);
            waterFallSteps.Add(RequestSelectPeriod);
            waterFallSteps.Add(SelectPeriod);
            waterFallSteps.Add(RequestSelectCreditCard);
            waterFallSteps.Add(SelectCreditCard);
            waterFallSteps.Add(GetCreditCardStatement);
            this.Dialogs.Add(Id, waterFallSteps.ToArray());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_DOCUMENT, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.SELECT_FOR_CONTINUE, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish));
            this.Dialogs.Add(AuthenticationWaterfallSteps.Inputs.INPUT_OTP, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(Inputs.SELECT_CREDIT_CARD, new ChoicePrompt(Culture.Spanish) { Style = Microsoft.Bot.Builder.Prompts.ListStyle.SuggestedAction });
            this.Dialogs.Add(Inputs.SELECT_PERIOD, new ChoicePrompt(Culture.Spanish) { Style = Microsoft.Bot.Builder.Prompts.ListStyle.SuggestedAction });
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_CARD_ID, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
        }

        private async Task RequestSelectCreditCard(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

            var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("ClientServices");

            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);
            httpClient.DefaultRequestHeaders.Add("Auth-user", conversationInfo.AuthUserToken);
            Response response;
            List<Card> listCards = new List<Card>();
            if (!configuration.GetValue<bool>("Dev:UseHotCodeCreditCardExtract"))
            {
                response = await Connection.Process(new ConnectorRestPost<RequestGetCreditCards, BaseApiResponse<ResponseGetCreditCards>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:ClientServices:GetCreditCards"), new RequestGetCreditCards()));

                var responseGetCredits = response as Response<BaseApiResponse<ResponseGetCreditCards>>;

                if (!response.IsOk)
                {
                    await dc.Context.SendActivity("No tienes tarjetas de credito");
                    await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                    await dc.End();
                    return;
                }
                listCards = responseGetCredits.Body.Body.Cards;
            }
            else
            {
                //listCards = new List<long>()
                //{
                //    4772170034493618,
                //    4772170083964816
                //};
            }

            var options = new List<Choice>();

            foreach (var card in listCards)
            {
                options.Add(new Choice()
                {
                    Action = new CardAction(title: card.Obfuscated, type: ActionTypes.ImBack, value: card.CardValue),
                    Value = card.CardValue
                });
            }

            await dc.Prompt(Inputs.SELECT_CREDIT_CARD, "Selecciona una tarjeta para obtener el extracto", new ChoicePromptOptions()
            {
                Choices = options,
                RetryPromptString = "Debes seleccionar o escribir una de sus tarjetas de credito, por favor intenta nuevamente"
            });
        }

        private async Task SelectCreditCard(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var cardChoice = (FoundChoice)args["Value"];

            conversationInfo.CreditCardExtractInfo.CreditCardSelected = cardChoice.Value;

            await next();
        }

        private async Task RequestSelectPeriod(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

            var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("ClientServices");

            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);
            httpClient.DefaultRequestHeaders.Add("Auth-user", conversationInfo.AuthUserToken);

            var responsePeriods = await httpClient.PostV2<BaseApiResponse<List<string>>>(configuration.GetValue<string>("Services:BnbChatBotServices:ClientServices:GetPeriods"), new { });

            var listPeriods = responsePeriods.Body;
            var options = new List<Choice>();

            foreach (var period in listPeriods)
            {
                options.Add(new Choice()
                {
                    Action = new CardAction(title: period.ToString(), type: ActionTypes.PostBack, value: period),
                    Value = period.ToString()
                });
            }

            await dc.Prompt(Inputs.SELECT_CREDIT_CARD, "Selecciona un periodo para obtener el extracto", new ChoicePromptOptions()
            {
                Choices = options,
                RetryPromptString = "Debes seleccionar o escribir un periodo, por favor intenta nuevamente"
            });
        }

        private async Task SelectPeriod(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var periodChoice = (FoundChoice)args["Value"];

            conversationInfo.CreditCardExtractInfo.Period = periodChoice.Value;

            await next();
        }

        private async Task GetCreditCardStatement(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            Response responseApi;

            //UseHotCodeCreditCardExtract
            if (!configuration.GetValue<bool>("Dev:UseHotCodeCreditCardExtract"))
            {
                var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("ClientServices");

                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);
                httpClient.DefaultRequestHeaders.Add("Auth-user", conversationInfo.AuthUserToken);

                responseApi = await Connection.Process(new ConnectorRestPost<RequestCreditCardStatement, BaseApiResponse<string>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:ClientServices:creditCardStatement"), new RequestCreditCardStatement()
                {
                    CardNumber = conversationInfo.CreditCardExtractInfo.CreditCardSelected,
                    Period = conversationInfo.CreditCardExtractInfo.Period
                }));
            }
            else
            {
                responseApi = Response.Ok(new BaseApiResponse<string>()
                {
                    Body = "https://repositorioclientes.s3.us-east-2.amazonaws.com/131223LPCARNETDEIDENTIDAD/1010007430/Creditos/TarjetasDeCredito/4972/Extractos/2020/0000004972202007.pdf?X-Amz-Expires=7200&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA26YCD766A3F2Y2YU/20201119/us-east-2/s3/aws4_request&X-Amz-Date=20201119T232117Z&X-Amz-SignedHeaders=host&X-Amz-Signature=3ed7824c5eb6f33b6a3c071dc74ee68e0e54d90e65cc95d281ea8344e5314a99"
                });
            }

            if (!responseApi.IsOk)
            {
                //await ManagerScope.Process(dc.Context, LogInfo.Load(dc.Context.Activity.From.Id, responseApi.Message));

                await dc.Context.SendActivity(Texts.FAILED_REST_CARD);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
                return;
            }

            var url = responseApi as Response<BaseApiResponse<string>>;

            //var heroCard = new HeroCard
            //{
            //    Title = "Tu extracto se generó correctamente, puedes hacer clic en el siguiente boton",
            //    Text = "Tu extracto se generó correctamente, puedes hacer clic en el siguiente boton",
            //    Buttons = new List<CardAction>
            //    {
            //        new CardAction(ActionTypes.OpenUrl, "Extracto", value: url.Body)
            //    }
            //};

            //var extractAttachment = new Attachment();
            //extractAttachment.ContentType = "application/pdf";
            //extractAttachment.ContentUrl = url.Body.Body;
            //extractAttachment.Name = $"Extracto_{conversationInfo.CreditCardExtractInfo.CreditCardSelected}_{conversationInfo.CreditCardExtractInfo.Period}";
            //var extractDocument = MessageFactory.Attachment(extractAttachment, "Extract");
            //var message = MessageFactory.SuggestedActions(new List<CardAction>() { new CardAction(type: ActionTypes.DownloadFile, title: "Extracto", value: url.Body) }).AsMessageActivity();

            //await dc.Context.SendActivity(url.Body.Body);

            var conditions = MessageFactory.Attachment(new HeroCard(
                        title: "Extracto de Tarjeta",
                        buttons: new List<CardAction>()
                        {
                            new CardAction(
                                title: "Ver",
                                type: ActionTypes.OpenUrl,
                                value: url.Body.Body
                                )
                        }
                        ).ToAttachment());

            await dc.Context.SendActivity(conditions);

            //await dc.Context.SendActivity(extractDocument);
            await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
            await dc.End();
        }
    }

    public class ResponsePeriods
    {
        public List<long> MyProperty { get; set; }
    }

    public class RequestGetCreditCards { }
    public class ResponseGetCreditCards
    {
        public List<Card> Cards { get; set; }
    }

    public class RequestCreditCardStatement
    {
        public string Period { get; set; }

        public string CardNumber { get; set; }
    }

    public class ResponseCreditCardStatement
    {

    }

    public class Card
    {
        public string CardValue { get; set; }

        public string Obfuscated { get; set; }
    }
}
