﻿namespace CarlitosBnb.Dialogs
{
    using BusinessLogic.Modules;
    using BusinessLogic.Modules.CardModule;
    using CarlitosBnb.Core;
    using CarlitosBnb.Core.Factories;
    using CarlitosBnb.Middlewares.DialogFlow;
    using Google.Cloud.Dialogflow.V2;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using System;

    public class CardDialogContainer : DialogContainer
    {
        public const string Id = "Card";

        public static CardDialogContainer Instance { get; } = new CardDialogContainer();

        private CardDialogContainer() : base(Id)
        {
            this.Dialogs.Add(Id, new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var serviceProvider = dc.Context.Services.Get<IServiceProvider>(CarlitosBnbBot.SERVICE_PROVIDER_ID);

                    CardInfo card = new CardInfo();
                    //var cardService = scope.ServiceProvider.GetService<ICardService>();

                    var queryResult = dc.Context.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);

                    var result = await ManagerScope.Process(dc.Context, new GetCardDto(){
                        Intent = queryResult.Intent.IntentName.IntentId
                    });

                    if(!result.IsOk)
                    {
                        await dc.Context.SendActivity("Ups, al parecer no pude encontrar opciones, por favor intenta nuevamente");
                        //conversationInfo.Clear();
                        await dc.End();
                        return;
                    }

                    card = (result as Result<CardInfo>).Body;

                    //card = await cardService.GetByIntent(new GetCardDto(){
                    //    Intent = result.Intent.IntentName.IntentId
                    //});

                    var creator = new CardCreator();
                    creator.UserName = dc.Context.Activity.From.FirstName();
                    var message = creator.Create(card);
                    await dc.Context.SendActivity(message);
                    //conversationInfo.Clear();
                    //await dc.Context.SendActivity($"{dc.Context.Activity.From.FirstName()}, dime en que más te puedo ayudar.");
                    await dc.End();
                }
            });
        }
    }
}
