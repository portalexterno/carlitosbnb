﻿using CarlitosBnb.SeedWorks;
using Core.Connectors;
using Core.Connectors.Core;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.Common
{
    public class AuthenticationWaterfallSteps
    {
        public struct Inputs
        {
            public const string INPUT_OTP = nameof(INPUT_OTP);
        }

        public struct Texts
        {
            public const string FAILED_REST_BALANCES = "Disculpa, pero no puedo obtener sesión en este instante, por favor intenta en un momento";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string INPUT_OTP = "Te enviamos un sms ";

            public const string CORRECT_SEND_EMAIL_CELLPHONE = "Te enviaré un código de verificación a tu email: {0} y al teléfono: {1}.";

            public const string CORRECT_SEND_ONLY_EMAIL = "Te enviaré un código de verificación a tu email: {0}.";

            public const string CORRECT_SEND_ONLY_CELLPHONE = "Te enviaré un código de verificación al teléfono: {0}.";

            public const string INPUT_OTP_CODE = "por favor ingresa el código enviado";

            public const string INVALID_OTP_CODE = "Disculpa este no parece ser un código valido.";

            public const string VALID_OTP_CODE = "El código se validó con éxito. Bienvenido!!!";
        }

        public static AuthenticationWaterfallSteps Instance { get; } = new AuthenticationWaterfallSteps();

        public WaterfallStep[] WaterFalls => new WaterfallStep[]
            {
                GetApplicationToken,
                RequestUserToken,
                InputOtpCode,
                GetUserToken
            };

        private async Task GetApplicationToken(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (!string.IsNullOrWhiteSpace(conversationInfo.AuthToken))
            {
                await next();
                return;
            }

            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            Response response;
            if (!configuration.GetValue<bool>("Dev:UseHotCodeAuthenticacion"))
            {
                #region Development
                var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("AuthenticationServices");
                response = await Connection.Process(new ConnectorRestPost<RequestGetApplicationToken, BaseApiResponse<ResponseGetApplicationToken>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:GetTokenApplication"), new RequestGetApplicationToken()
                {
                    ApplicationKey = configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:ApplicationKey"),
                    ApplicationSecret = configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:ApplicationSecret"),
                    ChannelAccountId = dc.Context.Activity.From.Id,
                    ConversationId = dc.Context.Activity.Conversation.Id
                }));

                //var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("BnbChatBotServices");
                //response = await Connection.Process(new ConnectorRestPost<RequestGetApplicationToken, BaseApiResponse<ResponseGetApplicationToken>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:GetTokenApplication"), new RequestGetApplicationToken()
                //{
                //    ApplicationKey = configuration.GetValue<string>("Services:BnbChatBotServices:ApplicationKey"),
                //    ApplicationSecret = configuration.GetValue<string>("Services:BnbChatBotServices:ApplicationSecret"),
                //    ChannelAccountId = dc.Context.Activity.From.Id,
                //    ConversationId = dc.Context.Activity.Conversation.Id
                //}));
                #endregion
            }
            else
            {
                response = Response.Ok(new BaseApiResponse<ResponseGetApplicationToken>()
                {
                    Body = new ResponseGetApplicationToken()
                    {
                        JwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBsaWNhdGlvbklkIjoiZDk3YjgyOGYtNjVlOS00NWEwLTk2YTEtMGQ1NzBkYmJkM2E2IiwiY2hhbm5lbEFjY291bnRJZCI6IjExMTExMTExMTExMTEiLCJuYmYiOjE2MDU3NDQ2ODMsImV4cCI6MTYwNTgzMTA4MywiaWF0IjoxNjA1NzQ0NjgzLCJhdWQiOiJBcHAifQ.xcxgW4SgutP8dtvGz8_GpgRbHca72n_MDWIjSVlmX1I",
                        IsContractAccepted = false
                    },
                    Success = true
                });
            }


            if (!response.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                await dc.End();
                return;
            }

            var responseBalance = response as Response<BaseApiResponse<ResponseGetApplicationToken>>;

            conversationInfo.AuthToken = responseBalance.Body.Body.JwtToken;
            await next();
        }

        private async Task RequestUserToken(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (HasSession(dc))
            {
                await next();
                return;
            }

            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("AuthenticationServices");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);

            Response response;
            if (!configuration.GetValue<bool>("Dev:UseHotCodeAuthenticacion"))
            {
                response = await Connection.Process(new ConnectorRestPost<RequestRequesttUserToken, BaseApiResponse<ResponseRequestUserToken>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:RequestTokenUser"), new RequestRequesttUserToken()
                {
                    DocumentNumber = conversationInfo.ProfileSession.DocumentNumber,
                    DocumentComplement = conversationInfo.ProfileSession.DocumentComplement
                }));
            }
            else
            {
                response = Response.Ok(new BaseApiResponse<ResponseRequestUserToken>()
                {
                    Body = new ResponseRequestUserToken()
                    {
                        Email = "e●●●●●●●●l@gmail.com",
                        Phone = "●●●●7777"
                    }
                });
            }

            if (!response.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                await dc.End();
                return;
            }

            var responseRequestToken = response as Response<BaseApiResponse<ResponseRequestUserToken>>;
            var email = responseRequestToken.Body.Body.Email;
            var phone = responseRequestToken.Body.Body.Phone;
            var message = string.Empty;
            if (!string.IsNullOrWhiteSpace(email) && !string.IsNullOrWhiteSpace(phone))
            {
                message = string.Format(Texts.CORRECT_SEND_EMAIL_CELLPHONE, email, phone);
            }
            else if (!string.IsNullOrWhiteSpace(email))
            {
                message = string.Format(Texts.CORRECT_SEND_ONLY_EMAIL, email);
            }
            else if (!string.IsNullOrWhiteSpace(phone))
            {
                message = string.Format(Texts.CORRECT_SEND_ONLY_CELLPHONE, phone);
            }

            #region Development

            #endregion

            await dc.Context.SendActivity(message);
            await next();
        }


        private async Task InputOtpCode(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (HasSession(dc))
            {
                await next();
                return;
            }

            await dc.Prompt(Inputs.INPUT_OTP, Texts.INPUT_OTP_CODE, new PromptOptions()
            {
                RetryPromptString = Texts.INVALID_OTP_CODE
            });
        }

        private async Task GetUserToken(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (HasSession(dc))
            {
                await next();
                return;
            }

            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var otpCode = conversationInfo.IsAudio ? new string(args["Value"].ToString().Where(c => char.IsDigit(c)).ToArray()) : args["Value"].ToString();

            var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("AuthenticationServices");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);

            Response response;
            if (!configuration.GetValue<bool>("Dev:UseHotCodeAuthenticacion"))
            {
                response = await Connection.Process(new ConnectorRestPost<RequestGetUserToken, BaseApiResponse<string>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:GetTokenUser"), new RequestGetUserToken()
                {
                    DocumentNumber = conversationInfo.ProfileSession.DocumentNumber,
                    DocumentComplement = conversationInfo.ProfileSession.DocumentComplement,
                    OtpCode = int.Parse(otpCode)
                }));
            }
            else
            {
                response = Response.Ok(new BaseApiResponse<string>()
                {
                    Body = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6IjEwMTAwMDc0MzAiLCJwaG9uZSI6IjAyNzg1MjUzICAiLCJlbWFpbCI6IkpBWUJHV18yMDA1QFlBSE9PLkNPTSAgICAgICAgICIsImRvY3VtZW50Ijp7Ik51bWJlciI6IjEzMTIyMyIsIkV4dGVuc2lvbiI6IiJ9LCJuYmYiOjE2MDU3NTY1NjksImV4cCI6MTYwNTc1NzE2OSwiaWF0IjoxNjA1NzU2NTY5LCJpc3MiOiJBcHAiLCJhdWQiOiJVc2VyIn0.eRroDE_ZBcIYrkGi_9ue8WNjd2hbliMs6tRsT4edEs4"
                });
            }

            if (!response.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
                return;
            }

            var responseGetToken = response as Response<BaseApiResponse<string>>;
                       
            conversationInfo.AuthUserToken = responseGetToken.Body.Body;
            await dc.Context.SendActivity(Texts.VALID_OTP_CODE);
            await next();
        }

        private bool HasSession(DialogContext dc)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            if (string.IsNullOrWhiteSpace(conversationInfo.AuthUserToken)
                || conversationInfo.LastSession.Value.AddMinutes(configuration.GetValue<double>("SessionTime")) < DateTime.Now
                )
            {
                return false;
            }

            return true;
        }
    }

    public class RequestGetApplicationToken
    {
        public string ApplicationKey { get; set; }

        public string ApplicationSecret { get; set; }

        public string ChannelAccountId { get; set; }

        public string ConversationId { get; set; }
    }

    public class ResponseGetApplicationToken
    {
        public string JwtToken { get; set; }

        public bool IsContractAccepted { get; set; }
    }


    public class RequestRequesttUserToken
    {
        public string DocumentNumber { get; set; }

        public string DocumentComplement { get; set; }
    }

    public class ResponseRequestUserToken
    {
        public string Phone { get; set; }

        public string Email { get; set; }
    }

    public class RequestGetUserToken
    {
        public string DocumentNumber { get; set; }

        public string DocumentComplement { get; set; }

        public int OtpCode { get; set; }
    }

    public class ResponseGetUserToken
    {

    }

    public class RequestGetCardIdUser
    {
        public string DocumentNumber { get; set; }

        public string DocumentComplement { get; set; }
    }

    public class ResponseGetCardIdUser
    {
        public List<string> CardIdList { get; set; }
        public string DocumentExtension { get; set; }
    }
}
