﻿using Core.Connectors;
using Core.Connectors.Core;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.Common
{
    public class ClientProfileWaterfallSteps
    {
        private struct Inputs
        {
            public const string INPUT_DOCUMENT = "inputDocument";
        }

        public struct Texts
        {
            public const string NOT_FOUND_BALANCE = "No encontré el saldo de tus cuentas, por favor intenta más tarde";

            public const string YOU_WANT_TO_CONTINUE = "¿Desea continuar?";

            public const string INVALID_DOCUMENT = "Disculpa este no parece ser un ci válido";

            public const string INPUT_DOCUMENT = "¡Ok! por favor, ingresa tu ci";

            public const string FAILED_REST_BALANCES = "Disculpa, pero no puedo obtener tu saldo en este momento, por favor intenta en un momento";

            public const string NOT_FOUND_OWNER_ACCOUNTS = "No encontré tus cuentas, por favor intenta más tarde.";

            public const string NOT_FOUND_ACCOUNTS_BY_DOCUMENT = "No se encontraron cuentas, con el ci";

            public const string UPDATE_DATA_CONTACT = "Por favor comunícate con el contact center (78787272) del Banco para actualizar tu información.";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string MAX_RETRY_VALIDATE_DOCUMENT = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";
        }

        public static ClientProfileWaterfallSteps Instance { get; } = new ClientProfileWaterfallSteps();

        public WaterfallStep[] WaterFalls => new WaterfallStep[]
            {
                this.GetClient
            };

        private async Task GetClient(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

            var response = await Connection.Process(new ConnectorRestPost<ClientProfileRequest, ClientProfileResponse>(HttpClientFactory.Create(), configuration.GetValue<string>("Services:ValidateDocument"), new ClientProfileRequest()
            {
                Document = conversationInfo.ProfileSession.DocumentNumberLong,
                Token = "CarlitosBNB"
            }));

            if (!response.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                await dc.End();
                return;
            }

            var responseBalance = response as Response<ClientProfileResponse>;

            if (!responseBalance.Body.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                await dc.End();
                return;
            }

            if (responseBalance.Body.Clients.Count <= 0)
            {
                await dc.Context.SendActivity(Texts.NOT_FOUND_ACCOUNTS_BY_DOCUMENT);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                await dc.End();
                return;
            }

            var clients = new List<Client>(responseBalance.Body.Clients.Where(c => c.Accounts.Count > 0).ToList());

            if (clients == null)
            {
                await dc.Context.SendActivity(Texts.NOT_FOUND_ACCOUNTS_BY_DOCUMENT);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                await dc.End();
                return;
            }

            if (clients.Any(c => string.IsNullOrWhiteSpace(c.CellPhone)) && clients.Any(c => string.IsNullOrWhiteSpace(c.Email)))
            {
                await dc.Context.SendActivity(Texts.UPDATE_DATA_CONTACT);
                await dc.End();
                return;
            }

            conversationInfo.ProfileSession.Clients = clients;
            await next();
        }
    }

    public enum TipoErrorRespuesta
    {
        Ninguno = 0,
        NoControlado = 2,
        ValidacionDatosEntrada = 6
    }

    public class ClientProfileRequest
    {
        [JsonProperty("NumeroDocumento")]
        public long Document { get; set; }

        [JsonProperty("Token")]
        public string Token { get; set; }
    }

    public class ClientProfileResponse
    {
        [JsonProperty("Correcto")]
        public bool IsOk { get; set; }

        [JsonProperty("Mensaje")]
        public string Message { get; set; }

        [JsonProperty("TipoError")]
        public TipoErrorRespuesta TypeError { get; set; }

        [JsonProperty("Clientes")]
        public List<Client> Clients { get; set; }
    }

    public class Client
    {
        [JsonProperty("Celular")]
        public string CellPhone { get; set; }

        public string CellPhoneObfuscate
        {
            get
            {
                if (string.IsNullOrEmpty(this.CellPhone))
                    return string.Empty;

                return string.Format("{0}{1}", new string('●', 4), this.CellPhone.Substring(4, this.CellPhone.Length - 4));
            }
        }

        [JsonProperty("CorreoElectronico")]
        public string Email { get; set; }

        public string EmailObfuscate
        {
            get
            {
                if (string.IsNullOrEmpty(this.Email))
                    return string.Empty;

                if (!Regex.IsMatch(this.Email.Trim(), @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                    return string.Empty;
                //var emailSplit = this.Email.Split('@');
                //return string.Format("{0}{1}@{2}", this.Email.Substring(0, 2), new string('●', emailSplit[0].Length - 2), emailSplit[1]);
                return Regex.Replace(this.Email.Trim(), @"(?<=[\w]{1})[\w-\._\+%]*(?=[\w]{1}@)", m => new string('●', m.Length));
            }
        }

        [JsonProperty("NumeroCliente")]
        public string Number { get; set; }

        [JsonProperty("TipoCliente")]
        public string Type { get; set; }

        [JsonProperty("NombresCliente")]
        public string Name { get; set; }

        [JsonProperty("ApellidoPaternoCliente")]
        public string LastName { get; set; }

        [JsonProperty("ApellidoMaternoCliente")]
        public string SecondLastName { get; set; }

        [JsonProperty("Cuentas")]
        public List<Account> Accounts { get; set; }
    }

    public class Account
    {
        [JsonProperty("Numero")]
        public string Number { get; set; }

        public string NumberObfuscate => $"{this.Number.Substring(0, 2)}{new string('●', 4)}{this.Number.Substring(6, this.Number.Length - 6)}";

        [JsonProperty("MonedaCodigo")]
        public int CurrencyCode { get; set; }

        [JsonProperty("MonedaDesripcion")]
        public string CurrencyDescription { get; set; }

        [JsonProperty("Saldo")]
        public decimal Balance { get; set; }

        [JsonProperty("SaldoCorrecto")]
        public bool IsCorrectBalance { get; set; }

        [JsonProperty("Tipo")]
        public int Type { get; set; }

        public string Label
        {
            get
            {
                return accountTypes.GetValueOrDefault(this.Type, "BNB Puntos No: ");
            }
        }

        private Dictionary<int, string> accountTypes = new Dictionary<int, string>
        {
            { 1, "Cuenta: " },
            { 2, "Crédito No: " }
        };
    }
}
