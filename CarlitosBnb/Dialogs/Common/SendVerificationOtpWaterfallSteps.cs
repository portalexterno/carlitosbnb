﻿using CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers;
using CarlitosBnb.SeedWorks;
using Core.Connectors;
using Core.Connectors.Core;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Prompts.Choices;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.Common
{
    public class SendVerificationOtpWaterfallSteps
    {
        public const string Id = "SendVerificationOtpWaterfallSteps";

        public static SendVerificationOtpWaterfallSteps Instance { get; } = new SendVerificationOtpWaterfallSteps();

        public struct Inputs
        {
            public const string OTP_CODE = "optCode";
            public const string SELECT_FOR_CONTINUE = "selectForContinue";
        }

        public struct Texts
        {
            public const string FAILED_SEND_MESSAGE = "Disculpa, no pude enviar el mensaje, por favor intenta más tarde";

            public const string CORRECT_SEND_EMAIL_CELLPHONE = "Te enviaré un código de verificación a tu email: {0} y al teléfono: {1}.";

            public const string CORRECT_SEND_ONLY_EMAIL = "Te enviaré un código de verificación a tu email: {0}.";

            public const string CORRECT_SEND_ONLY_CELLPHONE = "Te enviaré un código de verificación al teléfono: {0}.";

            public const string INVALID_OTP_CODE = "Disculpa este no parece ser un código valido.";

            public const string INPUT_OTP_CODE = "por favor ingresa el código enviado";

            public const string YOU_WANT_TO_CONTINUE = "¿Desea continuar?";

            public const string FAILED_VALIDATE_OTP_CODE = "Disculpa, no pude validar el código, por favor intenta más tarde";

            public const string VALID_OTP_CODE = "El código se validó con éxito. Bienvenido!!!";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string MAX_RETRY_VALIDATE_OTP_CODE = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";
        }

        public WaterfallStep[] Waterfalls => new WaterfallStep[]
            {
                this.SendMessage,
                this.InputOtp,
                this.VerifyOtp,
                this.RetryOtp
            };

        private async Task SendMessage(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var client = conversationInfo.ProfileSession.Clients;

            if (conversationInfo.CounterAttemptsOtpCode > 0)
            {
                await next();
                return;
            }

            var response = await Connection.Process(
                new ConnectorRestPost<SendAuthenticationMessageRequest, SendAuthenticationMessageResponse>(
                    HttpClientFactory.Create(),
                    configuration.GetValue<string>("Services:SendCodeOtp"), new SendAuthenticationMessageRequest()
                    {
                        Token = ServiceLoginConst.TOKEN,
                        TypeChannel = ServiceLoginConst.CHANNEL,
                        IdUserChannel = dc.Context.Activity.From.Id,
                        NumberClient = client.First().Number,
                        CellPhone = client.First().CellPhone,
                        Email = client.First().Email,
                        SendEmail = !string.IsNullOrEmpty(client.First().Email),
                        SendSMS = !string.IsNullOrEmpty(client.First().CellPhone)
                    }));

            if (!response.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_SEND_MESSAGE);
                await dc.End();
                return;
            }

            var responseSend = response as Response<SendAuthenticationMessageResponse>;

            if (!responseSend.Body.SendCorrectSms && !responseSend.Body.SendCorrectEmail)
            {
                await dc.Context.SendActivity(Texts.FAILED_SEND_MESSAGE);
                await dc.End();
                return;
            }

            if (responseSend.Body.SendCorrectEmail && responseSend.Body.SendCorrectSms)
            {
                await dc.Context.SendActivity(string.Format(Texts.CORRECT_SEND_EMAIL_CELLPHONE, client.First().EmailObfuscate, client.First().CellPhoneObfuscate));
            }
            else if (responseSend.Body.SendCorrectEmail)
            {
                await dc.Context.SendActivity(string.Format(Texts.CORRECT_SEND_ONLY_EMAIL, client.First().EmailObfuscate));
            }
            else if (responseSend.Body.SendCorrectSms)
            {
                await dc.Context.SendActivity(string.Format(Texts.CORRECT_SEND_ONLY_CELLPHONE, client.First().CellPhoneObfuscate));
            }

            await next();
        }

        private async Task InputOtp(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            await dc.Prompt(Inputs.OTP_CODE, Texts.INPUT_OTP_CODE, new PromptOptions()
            {
                RetryPromptString = Texts.INVALID_OTP_CODE
            });
        }

        private async Task VerifyOtp(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var clients = conversationInfo.ProfileSession.Clients;

            var otpCode = conversationInfo.IsAudio ? new string(args["Value"].ToString().Where(c => char.IsDigit(c)).ToArray())
                : args["Value"].ToString();

            if (!Regex.IsMatch(otpCode, BalancesDialogContainer.PATTERN_OTP_CODE))
            {
                await this.actionInvalidDocument(dc, conversationInfo, otpCode, configuration);
                return;
            }

            var response = await Connection.Process(
                new ConnectorRestPost<ValidateOtpCodeRequest, ValidateOtpCodeResponse>(
                    HttpClientFactory.Create(),
                    configuration.GetValue<string>("Services:ValidateOtp"), new ValidateOtpCodeRequest()
                    {
                        AuthenticationCode = otpCode,
                        IdUserChannel = dc.Context.Activity.From.Id,
                        CellPhone = clients.FirstOrDefault().CellPhone,
                        Email = clients.FirstOrDefault().Email,
                        Number = clients.FirstOrDefault().Number,
                        Token = ServiceLoginConst.TOKEN,
                        TypeChannel = ServiceLoginConst.CHANNEL
                    }));

            if (!response.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_VALIDATE_OTP_CODE);
                //conversationInfo.Clear();
                await dc.End();
                return;
            }

            var responsevalidateOtpCode = response as Response<ValidateOtpCodeResponse>;

            if (!responsevalidateOtpCode.Body.IsOk)
            {
                if (responsevalidateOtpCode.Body.TypeError == 5)
                {
                    await dc.Context.SendActivity(responsevalidateOtpCode.Body.Message);
                    //conversationInfo.Clear();
                    await dc.End();
                    return;
                }
                else
                {
                    await this.actionInvalidDocument(dc, conversationInfo, otpCode, configuration);

                    return;
                }
            }

            conversationInfo.CounterAttemptsOtpCode = 0;
            await dc.Context.SendActivity(Texts.VALID_OTP_CODE);
            await next();
        }

        private async Task actionInvalidDocument(DialogContext dc, ConversationInfo conversationInfo, string otpCode, IConfiguration configuration)
        {
            var options = new List<Choice>()
            {
                new Choice()
                {
                    Action = new CardAction(ActionTypes.ImBack, "Si deseo continuar", value: "YES"),
                    Value = "YES",
                    Synonyms = new List<string>{ "Si", "si", "Sí", "sí" }
                },
                new Choice()
                {
                    Action = new CardAction(ActionTypes.ImBack, "No", value: "NO"),
                    Value = "NO",
                    Synonyms = new List<string>{ "No", "no" }
                }
            };

            var actions = new List<CardAction>()
                {
                    new CardAction(type: ActionTypes.ImBack, displayText: "Si deseo continuar" , value: "YES"),
                    new CardAction(type: ActionTypes.ImBack, displayText: "No" , value: "NO"),
                };

            conversationInfo.CounterAttemptsOtpCode++;

            if (conversationInfo.CounterAttemptsOtpCode >= configuration.GetValue<int>("MaxTryCounterOtpCode"))
            {
                await dc.Context.SendActivity(Texts.INVALID_OTP_CODE);
                await dc.Context.SendActivity(Texts.MAX_RETRY_VALIDATE_OTP_CODE);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                conversationInfo.CounterAttemptsOtpCode = 0;
                dc.EndAll();
                return;
            }

            //var act = Activity.CreateMessageActivity();
            //act.Text = $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }";

            //act.SuggestedActions = new SuggestedActions()
            //{
            //    Actions = new List<CardAction>()
            //    {
            //        new CardAction(type: ActionTypes.PostBack, displayText: "Si deseo continuar" , value: "YES"),
            //        new CardAction(type: ActionTypes.PostBack, displayText: "No" , value: "NO"),
            //    }
            //};

            //await dc.Context.SendActivity(act);
            //var prompt = new Microsoft.Bot.Builder.Prompts.ChoicePrompt();

            //var choices = MessageFactory.SuggestedActions(actions, $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }");
            var choices = ChoiceFactory.SuggestedAction(options, $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }", $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }");
            await dc.Context.SendActivity(choices);
        }

        private async Task RetryOtp(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            if (args == null)
            {
                await next();
                return;
            }

            var value = (Activity)args["Activity"];

            if (value.Text == "YES" || value.Text.ToLower() == "si")
            {
                await dc.Begin(Id, dc.ActiveDialog.State);
                return;
            }

            var channelData = JsonConvert.DeserializeObject<ChannelData>(JsonConvert.SerializeObject(value.ChannelData), new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                },
                Formatting = Formatting.Indented
            });

            if (channelData != null && channelData.Message != null)
            {
                if (channelData.Message.QuickReply.Payload == "YES")
                {
                    await dc.Begin(Id, dc.ActiveDialog.State);
                    return;
                }
            }

            await dc.Context.SendActivity(string.Format($"{Texts.CANCEL}", dc.Context.Activity.From.FirstName()));
            conversationInfo.CounterAttemptsOtpCode = 0;
            dc.EndAll();
        }
    }
}
