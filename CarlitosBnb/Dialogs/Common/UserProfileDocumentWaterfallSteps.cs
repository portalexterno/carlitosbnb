﻿using CarlitosBnb.SeedWorks;
using Core.Connectors;
using Core.Connectors.Core;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Prompts.Choices;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.Common
{
    public class UserProfileDocumentWaterfallSteps
    {
        public const string Id = "UserProfileDocument";
        public const string PATTERN_DOCUMENT = @"^([0-9]{5,9})$";
        //public const string PATTERN_DOCUMENT_AND_COMPLEMENT = @"^([0-9]{5,9})(-([a-zA-Z0-9]{1,4}))?$";
        public const string CARD_ID = @"^([0-9]{2,9})$";

        public static bool AskForDocumentExtension = false;
        public readonly List<string> DOCUMENT_EXTENSIONS = new List<string>()
        {
            "LP", "OR", "PO", "CB", "CH", "TJ", "PA", "BE", "SC"
        };

        //private readonly Action<DialogContext, IDictionary<string, object>> nextAction;

        //protected UserProfileDocumentWaterfallSteps(Action<DialogContext, IDictionary<string, object>> nextAction)
        //{
        //    this.nextAction = nextAction;
        //}

        //public static UserProfileDocumentWaterfallSteps Instance(Action<DialogContext, IDictionary<string, object>> nextAction)
        //{
        //    return new UserProfileDocumentWaterfallSteps(nextAction);
        //}

        public static UserProfileDocumentWaterfallSteps Instance { get; } = new UserProfileDocumentWaterfallSteps();

        public struct Inputs
        {
            public const string INPUT_DOCUMENT = "inputDocument";

            public const string SELECT_FOR_CONTINUE = "selectForContinue";

            public const string INPUT_DOCUMENT_EXTENTION = "INPUT_DOCUMENT_EXTENTION";

            public const string INPUT_CARD_ID = "INPUT_CARD_ID";
        }

        public struct Texts
        {
            public const string YOU_WANT_TO_CONTINUE = "¿Desea continuar?";

            public const string INVALID_DOCUMENT = "Disculpa, este no parece ser un ci válido";

            public const string INPUT_DOCUMENT = "¡Ok! por favor, ingresa tu ci";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string MAX_RETRY_VALIDATE_DOCUMENT = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";

            public const string INPUT_DOCUMENT_EXTENTION = "Por favor selecciona la extension de tu ci";

            public const string INPUT_CARD_ID = "Adicionalmente, favor ingresa los {0} últimos dígitos de tu tarjeta";

            public const string FAILED_LOGIN = "Disculpa, pero no puedo validar tu sesión ahora, por favor intenta en un momento";

            public const string INVALID_CARD_ID = "Disculpa, no parecen ser dígitos válidos";

            public const string MAX_RETRY_VALIDATE_CARD_ID = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";
        }

        public WaterfallStep[] WaterfallStepsOnlyDocumentNumber => new WaterfallStep[]
        {
            InputDocument,
            ValidateDocument,
            RetryValidateDocument,
            InputCardId,
            ValidateCardId,
            RetryValidateCardId
        };

        public WaterfallStep[] WaterfallStepsFull => new WaterfallStep[]
        {
            InputDocument,
            ValidateDocument,
            RetryValidateDocument,
            InputDocumentExtension,
            ValidateAndStoreDocumentExtension,
            InputCardId,
            ValidateCardId,
            RetryValidateCardId
        };

        private async Task InputDocument(DialogContext dc, object args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (HasSession(dc))
            {
                await next();
                return;
            }

            await dc.Prompt(Inputs.INPUT_DOCUMENT, Texts.INPUT_DOCUMENT);
        }

        private async Task ValidateDocument(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (HasSession(dc))
            {
                await next();
                return;
            }

            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            //var documentArray = (args["Value"] ?? "").ToString().Split('-');

            conversationInfo.ProfileSession.DocumentNumber = new string(args["Value"].ToString().Where(c => char.IsDigit(c)).ToArray());
            //conversationInfo.ProfileSession.DocumentNumber = (documentArray.Length >= 1) ? documentArray[0] : "";
            //conversationInfo.ProfileSession.DocumentComplement = (documentArray.Length >= 2) ? documentArray[1] : "";


            //if (!Regex.IsMatch(new string(args["Value"].ToString().Where(c => char.IsDigit(c)).ToArray()), PATTERN_DOCUMENT_AND_COMPLEMENT))
            if (!Regex.IsMatch(conversationInfo.ProfileSession.DocumentNumber, PATTERN_DOCUMENT))
            {
                var options = new List<Choice>()
                    {
                        new Choice()
                        {
                            Action = new CardAction(title: "Sí deseo continuar", type: ActionTypes.PostBack, value: "YES"),
                            Value = "YES",
                            Synonyms = new List<string>{ "Si", "Sí", "si" }
                        },
                        new Choice()
                        {
                            Action = new CardAction(title: "No", type: ActionTypes.PostBack, value: "NO"),
                            Value = "NO",
                            Synonyms = new List<string>{ "No", "no" }
                        }
                    };

                conversationInfo.CounterAttemptsLogin++;

                if (conversationInfo.CounterAttemptsLogin >= configuration.GetValue<int>("MaxTryCounterLogin"))
                {
                    await dc.Context.SendActivity(Texts.INVALID_DOCUMENT);
                    await dc.Context.SendActivity(Texts.MAX_RETRY_VALIDATE_DOCUMENT);
                    await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                    conversationInfo.CounterAttemptsLogin = 0;
                    await dc.End();
                    return;
                }

                try
                {
                    await dc.Prompt(Inputs.SELECT_FOR_CONTINUE, $"{ Texts.INVALID_DOCUMENT }, { Texts.YOU_WANT_TO_CONTINUE } ", new ChoicePromptOptions()
                    {
                        Choices = options,
                        RetryPromptActivity = ChoiceFactory.SuggestedAction(options, "Por favor escriba/seleccione una de las opciones: Si/No") as Activity
                    });
                }
                catch (Exception e)
                {
                    var ex = e;
                }

                return;
            }
            else
            {
                await next();
            }
        }

        private async Task RetryValidateDocument(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (HasSession(dc))
            {
                await next();
                return;
            }

            if (args == null)
            {
                await next(args);
                return;
            }

            var choice = (FoundChoice)args["Value"];

            if (choice.Value == "YES")
            {
                await dc.Begin(Id, dc.ActiveDialog.State);
            }
            else
            {
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
            }
        }

        private async Task InputDocumentExtension(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var options = new List<Choice>();

            foreach (var documentExtension in DOCUMENT_EXTENSIONS)
            {
                options.Add(new Choice()
                {
                    Action = new CardAction(title: documentExtension, value: documentExtension),
                    Value = documentExtension
                });
            }

            await dc.Prompt(Inputs.INPUT_DOCUMENT_EXTENTION, Texts.INPUT_DOCUMENT_EXTENTION, new ChoicePromptOptions()
            {
                Choices = options
            });
        }

        private async Task ValidateAndStoreDocumentExtension(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var choice = (FoundChoice)args["Value"];
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            conversationInfo.ProfileSession.DocumentExtension = choice.Value;
            await next();
        }

        private async Task InputCardId(DialogContext dc, object args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (HasSession(dc))
            {
                await next();
                return;
            }

            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("AuthenticationServices");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);

            var responseCardIdUser = await Connection.Process(new ConnectorRestPost<RequestGetCardIdUser, BaseApiResponse<ResponseGetCardIdUser>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:GetCardIdUser"), new RequestGetCardIdUser()
            {
                DocumentNumber = conversationInfo.ProfileSession.DocumentNumber,
                DocumentComplement = string.Empty
            }));

            if (!responseCardIdUser.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_LOGIN);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            var response = responseCardIdUser as Response<BaseApiResponse<ResponseGetCardIdUser>>;
            conversationInfo.ProfileSession.ActiveCardIdList = response.Body.Body.CardIdList;
            conversationInfo.ProfileSession.DocumentExtension = response.Body.Body.DocumentExtension;
            conversationInfo.CounterAttemptsLogin = 0;

            if (conversationInfo.ProfileSession.ActiveCardIdList.Count == 0)
            {
                await next();
                return;
            }

            await dc.Prompt(Inputs.INPUT_CARD_ID, string.Format(Texts.INPUT_CARD_ID, configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:CardIdUserDigitNumber")));
        }

        private async Task ValidateCardId(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (HasSession(dc) || conversationInfo.ProfileSession.ActiveCardIdList.Count == 0)
            {
                await next();
                return;
            }

            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            conversationInfo.ProfileSession.CardId = new string(args["Value"].ToString().Where(c => char.IsDigit(c)).ToArray());

            bool ValidCardId = IsValidCardId(conversationInfo.ProfileSession.CardId, Convert.ToInt32(configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:CardIdUserDigitNumber")), conversationInfo.ProfileSession.ActiveCardIdList);

            if (!Regex.IsMatch(conversationInfo.ProfileSession.CardId, CARD_ID) || !ValidCardId)
            {
                var options = new List<Choice>()
                    {
                        new Choice()
                        {
                            Action = new CardAction(title: "Sí deseo continuar", type: ActionTypes.PostBack, value: "YES"),
                            Value = "YES",
                            Synonyms = new List<string>{ "Si", "Sí", "si" }
                        },
                        new Choice()
                        {
                            Action = new CardAction(title: "No", type: ActionTypes.PostBack, value: "NO"),
                            Value = "NO",
                            Synonyms = new List<string>{ "No", "no" }
                        }
                    };

                conversationInfo.CounterAttemptsLogin++;

                if (conversationInfo.CounterAttemptsLogin >= configuration.GetValue<int>("MaxTryCounterLogin"))
                {
                    await dc.Context.SendActivity(Texts.INVALID_CARD_ID);
                    await dc.Context.SendActivity(Texts.MAX_RETRY_VALIDATE_CARD_ID);
                    await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                    conversationInfo.CounterAttemptsLogin = 0;
                    await dc.End();
                    return;
                }

                try
                {
                    await dc.Prompt(Inputs.SELECT_FOR_CONTINUE, $"{ Texts.INVALID_CARD_ID }, { Texts.YOU_WANT_TO_CONTINUE } ", new ChoicePromptOptions()
                    {
                        Choices = options,
                        RetryPromptActivity = ChoiceFactory.SuggestedAction(options, "Por favor escriba/seleccione una de las opciones: Si/No") as Activity
                    });
                }
                catch (Exception e)
                {
                    var ex = e;
                }

                return;
            }
            else
            {
                await next();
            }
        }

        private async Task RetryValidateCardId(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            if (HasSession(dc) || conversationInfo.ProfileSession.ActiveCardIdList.Count == 0)
            {
                await next();
                return;
            }

            if (args == null)
            {
                await next(args);
                return;
            }

            var choice = (FoundChoice)args["Value"];

            if (choice.Value == "YES")
            {
                await dc.Begin(Id, dc.ActiveDialog.State);
            }
            else
            {
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
            }
        }

        private bool HasSession(DialogContext dc)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            if (string.IsNullOrWhiteSpace(conversationInfo.AuthUserToken)
                || conversationInfo.LastSession.Value.AddMinutes(configuration.GetValue<double>("SessionTime")) < DateTime.Now
                )
            {
                return false;
            }

            return true;
        }

        private bool IsValidCardId(string inputValue, int outputLenght, List<string> inputCollection)
        {
            foreach (string item in inputCollection)
            {
                if (item.Substring(item.Length - outputLenght).Equals(inputValue)) return true;
            }
            return false;
        }
    }
}
