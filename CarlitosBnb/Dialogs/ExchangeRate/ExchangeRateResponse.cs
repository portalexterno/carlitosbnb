﻿namespace CarlitosBnb.Dialogs.ExchangeRate
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class ExchangeRateResponse
    {
        [JsonProperty("DetalleTipoCambio")]
        public List<DetailExchange> Details { get; set; }

        [JsonProperty("Mensaje")]
        public string Message { get; set; }

        [JsonProperty("Correcto")]
        public bool IsOk { get; set; }
    }

    public class DetailExchange
    {
        [JsonProperty("MonedaAbreviacion")]
        public string CurrencyAbbreviation { get; set; }

        [JsonProperty("Moneda")]
        public int Currency { get; set; }

        [JsonProperty("Compra")]
        public double Buy { get; set; }

        [JsonProperty("Venta")]
        public double Sale { get; set; }

        [JsonProperty("Contable")]
        public double Countable { get; set; }

        [JsonProperty("Oficial")]
        public double Official { get; set; }
    }
}
