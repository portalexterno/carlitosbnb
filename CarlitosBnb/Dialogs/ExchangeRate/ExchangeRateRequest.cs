﻿namespace CarlitosBnb.Dialogs.ExchangeRate
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class ExchangeRateRequest
    {
        [JsonProperty("Token")]
        public string Token { get; set; }

        [JsonProperty("Monedas")]
        public List<string> Currencies { get; set; }

        [JsonProperty("Fecha")]
        public string Date { get; set; }
    }
}
