﻿namespace CarlitosBnb.Dialogs.ExchangeRate
{
    using CarlitosBnb.Middlewares.DialogFlow;
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Google.Cloud.Dialogflow.V2;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;

    public class ExchangeRateDialogContainer : DialogContainer
    {
        public const string Id = "ExchangeRate";

        public static ExchangeRateDialogContainer Instance { get; } = new ExchangeRateDialogContainer();

        private const string DIALOGFLOW_CURRENCY = "tipocambio";

        private const string TOKEN = "0000";

        private struct CurrenciesTypes
        {
            public const string DOLAR = "Dolar";
            public const string EURO = "Euro";
            public const string UFV = "Ufv";
        }

        private readonly Dictionary<string, string> currencies = new Dictionary<string, string>
        {
            { CurrenciesTypes.DOLAR, "1" },
            { CurrenciesTypes.EURO, "16" },
            { CurrenciesTypes.UFV, "21" }
        };

        private struct Texts
        {
            public const string FAILED_REST_BALANCES = "Disculpa, pero no puedo obtener el tipo de cambio en este momento, por favor intenta en un momento";
        }

        private ExchangeRateDialogContainer() : base(Id)
        {
            this.Dialogs.Add(Id, new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var result = dc.Context.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);
                    var currency = result.Parameters.Fields[DIALOGFLOW_CURRENCY].StringValue;
                    var listCurrencies = new List<string>();
                    if(string.IsNullOrEmpty(currency))
                    {
                        listCurrencies.Add(currencies[CurrenciesTypes.DOLAR]);
                        listCurrencies.Add(currencies[CurrenciesTypes.EURO]);
                        listCurrencies.Add(currencies[CurrenciesTypes.UFV]);
                    }
                    else
                    {
                        listCurrencies.Add(currencies[currency]);
                    }

                    var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
                    var response = await Connection.Process(new ConnectorRestPost<ExchangeRateRequest, ExchangeRateResponse>(HttpClientFactory.Create(), configuration.GetValue<string>("Services:GetExchangeRate"), new ExchangeRateRequest(){
                        Currencies = listCurrencies,
                        Date = DateTime.Now.ToString("yyyyMMdd"),
                        Token = TOKEN
                    }));

                    if(!response.IsOk)
                    {
                        await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                        //conversationInfo.Clear();
                        await dc.End();
                        //await logService.Error(LogInfo.Load(dc.Context.Activity.From.Id, response));
                        return;
                    }

                    var responseExchange = response as Response<ExchangeRateResponse>;

                    if(!responseExchange.Body.IsOk)
                    {
                        await dc.Context.SendActivity(responseExchange.Message);
                        await dc.End();
                        return;
                    }

                    var attachments = new List<Attachment>();
                    foreach(var exchangeType in responseExchange.Body.Details)
                    {
                        attachments.Add(new HeroCard()
                        {
                            Title = exchangeType.CurrencyAbbreviation,
                            Subtitle = $"Oficial: { exchangeType.Official }   Compra: { exchangeType.Buy }   Venta: { exchangeType.Sale }"
                        }.ToAttachment());
                    }

                    await dc.Context.SendActivity(MessageFactory.Attachment(attachments));
                    await dc.End();
                    conversationInfo.IsCompletedTask = true;
                }
            });
        }
    }
}
