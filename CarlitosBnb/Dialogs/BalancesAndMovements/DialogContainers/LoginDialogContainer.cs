﻿namespace CarlitosBnb.Dialogs.BalanceAndMovements.DialogContainers
{
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Builder.Prompts;
    using Microsoft.Extensions.Configuration;
    using System.Linq;
    using System.Text.RegularExpressions;

    public class LoginDialogContainer : DialogContainer
    {
        public const string Id = "Login";

        private const string PATTERN_DOCUMENT = @"^([0-9]{5,9})$";

        private const string PATTERN_OTP_CODE = @"^([0-9]{4})$";

        private struct Texts
        {
            public const string RETRY_DOCUMENT = "Disculpa este no parece ser un documento válido";

            public const string INPUT_DOCUMENT = "Ingresa tu documento de identidad";

            public const string FAILED_REST_BALANCES = "Disculpa, pero no puedo obtener tu saldo en este momento, por favor intenta en un momento";

            public const string NOT_FOUND_OWNER_ACCOUNTS = "No encontre tus cuentas, por favor intenta más tarde.";

            public const string NOT_FOUND_ACCOUNTS_BY_DOCUMENT = "No se encontraron cuentas, con el número de documento";

            public const string UPDATE_DATA_CONTACT = "Por favor contáctate con el contact center (800-10-7272) del Banco para actualizar tu información.";
        }

        private struct Inputs
        {
            public const string INPUT_DOCUMENT = "inputDocument";
        }

        public static LoginDialogContainer Instance(IConfiguration configuration)
        {
            return new LoginDialogContainer(configuration);
        }

        private LoginDialogContainer(IConfiguration configuration) : base(Id)
        {
            this.Dialogs.Add(Inputs.INPUT_DOCUMENT, new Microsoft.Bot.Builder.Dialogs.TextPrompt(
                async (context, result) =>
                {
                    if (!Regex.IsMatch(result.Value, PATTERN_DOCUMENT))
                    {
                        result.Status = PromptStatus.OutOfRange;
                    }
                }));
            this.Dialogs.Add(Id, new WaterfallStep[]{
                async(dc, args, next) =>
                {
                    await dc.Prompt(Inputs.INPUT_DOCUMENT, Texts.INPUT_DOCUMENT, new PromptOptions(){
                        RetryPromptString = Texts.RETRY_DOCUMENT
                    });
                },
                async(dc, args, next) =>
                {
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var document = args["Value"].ToString();
                    var response = await Connection<BalanceResponse>.Process(new ConnectorRestPost<BalanceRequest, BalanceResponse>(configuration.GetValue<string>("Services:ValidateDocument"), new BalanceRequest(){
                        Document = document,
                        Token = "CarlitosBNB"
                    })) as Response<BalanceResponse>;

                    if(!response.IsOk)
                    {
                        await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                        await dc.End();
                        return;
                    }

                    if(!response.Body.IsOk)
                    {
                        await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                        await dc.End();
                        return;
                    }

                    if(response.Body.Clients.Count <= 0)
                    {
                        await dc.Context.SendActivity(Texts.NOT_FOUND_ACCOUNTS_BY_DOCUMENT);
                        await dc.End();
                        return;
                    }

                    var client = new Client();
                    client = response.Body.Clients.First();

                    if(string.IsNullOrEmpty(client.CellPhone) && string.IsNullOrEmpty(client.Email))
                    {
                        await dc.Context.SendActivity(Texts.UPDATE_DATA_CONTACT);
                        await dc.End();
                        return;
                    }

                    conversationInfo.Add("client", client);

                    await dc.Begin(SendMessageDialogContainer.Id, dc.ActiveDialog.State);
                }
            });
            //this.Dialogs.Add(SendMessageDialogContainer.Id, SendMessageDialogContainer.Instance(configuration));
        }
    }
}
