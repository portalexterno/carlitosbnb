﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers
{
    using CarlitosBnb.Dialogs.Common;
    using CarlitosBnb.SeedWorks;
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Builder.Prompts.Choices;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    public class VerifyOtpWaterfallSteps
    {
        public const string Id = "VerifyOtp";

        public string NextId { get; set; }

        public struct Texts
        {
            public const string YOU_WANT_TO_CONTINUE = "¿Desea continuar?";

            public const string INVALID_OTP_CODE = "Disculpa este no parece ser un código valido.";

            public const string FAILED_VALIDATE_OTP_CODE = "Disculpa, no pude validar el código, por favor intenta más tarde";

            public const string VALID_OTP_CODE = "El código se validó con éxito. Bienvenido!!!";

            public const string INPUT_OTP_CODE = "por favor ingresa el código enviado";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string MAX_RETRY_VALIDATE_OTP_CODE = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";
        }

        private struct Inputs
        {
            public const string OTP_CODE = "optCode";
            public const string SELECT_FOR_CONTINUE = "selectForContinue";
        }

        public WaterfallStep[] WaterFalls => new WaterfallStep[]
            {
                this.InputOtp,
                this.VerifyOtp,
                this.RetryOtp
            };


        private async Task InputOtp(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            dc.Context.GetConversationState<ConversationInfo>().HasExpectedResponse();
            await dc.Prompt(Inputs.OTP_CODE, Texts.INPUT_OTP_CODE, new PromptOptions()
            {
                RetryPromptString = Texts.INVALID_OTP_CODE
            });
        }

        private async Task VerifyOtp(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {

            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var clients = conversationInfo.ProfileSession.Clients;

            var otpCode = conversationInfo.IsAudio ? new string(args["Value"].ToString().Where(c => char.IsDigit(c)).ToArray())
                : args["Value"].ToString();

            if (!Regex.IsMatch(otpCode, BalancesDialogContainer.PATTERN_OTP_CODE))
            {
                await this.actionInvalidDocument(dc, conversationInfo, otpCode, configuration);
                return;
            }

            //var response = await Connection.Process(
            //    new ConnectorRestPost<ValidateOtpCodeRequest, ValidateOtpCodeResponse>(
            //        HttpClientFactory.Create(),
            //        configuration.GetValue<string>("Services:ValidateOtp"), new ValidateOtpCodeRequest()
            //        {
            //            AuthenticationCode = otpCode,
            //            IdUserChannel = dc.Context.Activity.From.Id,
            //            CellPhone = clients.FirstOrDefault().CellPhone,
            //            Email = clients.FirstOrDefault().Email,
            //            Number = clients.FirstOrDefault().Number,
            //            Token = ServiceLoginConst.TOKEN,
            //            TypeChannel = ServiceLoginConst.CHANNEL
            //        }));
            var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("BnbChatBotServices");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);

            var response = await Connection.Process(new ConnectorRestPost<RequestGetUserToken, BaseApiResponse<string>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:GetTokenUser"), new RequestGetUserToken()
            {
                DocumentNumber = conversationInfo.ProfileSession.DocumentNumber,
                DocumentComplement = conversationInfo.ProfileSession.DocumentComplement,
                OtpCode = int.Parse(otpCode)
            }));

            if (!response.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_VALIDATE_OTP_CODE);
                //conversationInfo.Clear();
                await dc.End();
                return;
            }

            //var responsevalidateOtpCode = response as Response<ValidateOtpCodeResponse>;

            //if (!responsevalidateOtpCode.Body.IsOk)
            //{
            //    if (responsevalidateOtpCode.Body.TypeError == 5)
            //    {
            //        await dc.Context.SendActivity(responsevalidateOtpCode.Body.Message);
            //        //conversationInfo.Clear();
            //        await dc.End();
            //        return;
            //    }
            //    else
            //    {
            //        await this.actionInvalidDocument(dc, conversationInfo, otpCode, configuration);

            //        return;
            //    }
            //}

            var responseGetToken = response as Response<BaseApiResponse<string>>;
            conversationInfo.AuthUserToken = responseGetToken.Body.Body;
            conversationInfo.ProfileSession.Clients = clients;
            //conversationInfo.Session = Session.Create(DateTime.Now);
            conversationInfo.LastSession = DateTime.Now;

            await dc.Context.SendActivity(Texts.VALID_OTP_CODE);
            await dc.Replace(this.NextId, dc.ActiveDialog.State);
        }

        private async Task RetryOtp(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            //var value = (Activity)args["Activity"];
            //var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();         
            //if(value.Text == "YES" || value.Text.ToLower() == "si")
            //{
            //    await dc.Replace(Id, dc.ActiveDialog.State);
            //}
            //else
            //{
            //    await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));                
            //    await dc.End();
            //}

            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            if (args == null)
            {
                await next();
                return;
            }

            var value = (Activity)args["Activity"];

            if (value.Text == "YES" || value.Text.ToLower() == "si")
            {
                await dc.Replace(Id, dc.ActiveDialog.State);
                return;
            }

            var channelData = JsonConvert.DeserializeObject<ChannelData>(JsonConvert.SerializeObject(value.ChannelData), new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                },
                Formatting = Formatting.Indented
            });

            if (channelData != null && channelData.Message != null)
            {
                if (channelData.Message.QuickReply.Payload == "YES")
                {
                    await dc.Replace(Id, dc.ActiveDialog.State);
                    return;
                }
            }

            await dc.Context.SendActivity(string.Format($"{Texts.CANCEL}", dc.Context.Activity.From.FirstName()));
            conversationInfo.CounterAttemptsOtpCode = 0;
            dc.EndAll();
        }

        private async Task actionInvalidDocument(DialogContext dc, ConversationInfo conversationInfo, string otpCode, IConfiguration configuration)
        {
            var options = new List<Choice>()
            {
                new Choice()
                {
                    Action = new CardAction(ActionTypes.ImBack, "Si deseo continuar", value: "YES"),
                    Value = "YES",
                    Synonyms = new List<string>{ "Si", "si", "Sí", "sí" }
                },
                new Choice()
                {
                    Action = new CardAction(ActionTypes.ImBack, "No", value: "NO"),
                    Value = "NO",
                    Synonyms = new List<string>{ "No", "no" }
                }
            };

            var actions = new List<CardAction>()
                {
                    new CardAction(type: ActionTypes.ImBack, displayText: "Si deseo continuar" , value: "YES"),
                    new CardAction(type: ActionTypes.ImBack, displayText: "No" , value: "NO"),
                };

            conversationInfo.CounterAttemptsOtpCode++;

            if (conversationInfo.CounterAttemptsOtpCode >= configuration.GetValue<int>("MaxTryCounterOtpCode"))
            {
                await dc.Context.SendActivity(Texts.INVALID_OTP_CODE);
                await dc.Context.SendActivity(Texts.MAX_RETRY_VALIDATE_OTP_CODE);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                conversationInfo.CounterAttemptsOtpCode = 0;
                dc.EndAll();
                return;
            }

            //var act = Activity.CreateMessageActivity();
            //act.Text = $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }";

            //act.SuggestedActions = new SuggestedActions()
            //{
            //    Actions = new List<CardAction>()
            //    {
            //        new CardAction(type: ActionTypes.PostBack, displayText: "Si deseo continuar" , value: "YES"),
            //        new CardAction(type: ActionTypes.PostBack, displayText: "No" , value: "NO"),
            //    }
            //};

            //await dc.Context.SendActivity(act);
            //var prompt = new Microsoft.Bot.Builder.Prompts.ChoicePrompt();

            //var choices = MessageFactory.SuggestedActions(actions, $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }");
            var choices = ChoiceFactory.SuggestedAction(options, $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }", $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }");
            await dc.Context.SendActivity(choices);


            //await dc.Prompt(BalancesDialogContainer.Inputs.SELECT_FOR_CONTINUE, $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE }", new ChoicePromptOptions()
            //{
            //    Choices = options,
            //    RetryPromptActivity = (Activity)act,
            //    RetryPromptString = null,
            //    RetrySpeak = null
            //    //RetryPromptString = $"{ Texts.INVALID_OTP_CODE}, { Texts.YOU_WANT_TO_CONTINUE } Si/No" + (conversationInfo.IsAudio ? $"Entendí que dijiste {otpCode}" : string.Empty)
            //});
        }

        //private async Task<bool> CustomChoicePromtValidator(ChoicePrompt. prompContext, CancellationToken cancellationToken)
        //{

        //}
    }
}
