﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers
{
    using Microsoft.Bot.Builder.Dialogs;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class SendMessageWaterfallSteps
    {
        public const string Id = "SendMessage";

        public static SendMessageWaterfallSteps Instance { get; } = new SendMessageWaterfallSteps();

        public struct Texts
        {
            public const string FAILED_SEND_MESSAGE = "Disculpa, no pude enviar el mensaje, por favor intenta más tarde";

            public const string CORRECT_SEND_EMAIL_CELLPHONE = "Te enviaré un código de verificación a tu email: {0} y al teléfono: {1}.";

            public const string CORRECT_SEND_ONLY_EMAIL = "Te enviaré un código de verificación a tu email: {0}.";

            public const string CORRECT_SEND_ONLY_CELLPHONE = "Te enviaré un código de verificación al teléfono: {0}.";
        }

        public WaterfallStep[] Waterfalls => new WaterfallStep[]
            {
                this.SendMessage
            };

        private async Task SendMessage(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            //var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            //var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            //var client = conversationInfo.ProfileSession.Clients;

            //var response = await Connection.Process(
            //    new ConnectorRestPost<SendAuthenticationMessageRequest, SendAuthenticationMessageResponse>(
            //        HttpClientFactory.Create(),
            //        configuration.GetValue<string>("Services:SendCodeOtp"), new SendAuthenticationMessageRequest()
            //    {
            //        Token = ServiceLoginConst.TOKEN,
            //        TypeChannel = ServiceLoginConst.CHANNEL,
            //        IdUserChannel = dc.Context.Activity.From.Id,
            //        //NumberClient = client.Accounts.Count > 0 ? client.Accounts.First().Number : string.Empty,
            //        NumberClient = client.First().Number,
            //        CellPhone = client.First().CellPhone,
            //        Email = client.First().Email,
            //        SendEmail = !string.IsNullOrEmpty(client.First().Email),
            //        SendSMS = !string.IsNullOrEmpty(client.First().CellPhone)
            //    }));

            //if (!response.IsOk)
            //{
            //    await dc.Context.SendActivity(Texts.FAILED_SEND_MESSAGE);                
            //    await dc.End();
            //    return;
            //}

            //var responseSend = response as Response<SendAuthenticationMessageResponse>;

            //if (!responseSend.Body.SendCorrectSms && !responseSend.Body.SendCorrectEmail)
            //{
            //    await dc.Context.SendActivity(Texts.FAILED_SEND_MESSAGE);
            //    await dc.End();
            //    return;
            //}

            //if (responseSend.Body.SendCorrectEmail && responseSend.Body.SendCorrectSms)
            //{
            //    await dc.Context.SendActivity(string.Format(Texts.CORRECT_SEND_EMAIL_CELLPHONE, client.First().EmailObfuscate, client.First().CellPhoneObfuscate));
            //}
            //else if (responseSend.Body.SendCorrectEmail)
            //{
            //    await dc.Context.SendActivity(string.Format(Texts.CORRECT_SEND_ONLY_EMAIL, client.First().EmailObfuscate));
            //}
            //else if (responseSend.Body.SendCorrectSms)
            //{
            //    await dc.Context.SendActivity(string.Format(Texts.CORRECT_SEND_ONLY_CELLPHONE, client.First().CellPhoneObfuscate));
            //}

            //conversationInfo.CounterAttemptsOtpCode = 0;
            await dc.Replace(VerifyOtpWaterfallSteps.Id, dc.ActiveDialog.State);
        }
    }
}
