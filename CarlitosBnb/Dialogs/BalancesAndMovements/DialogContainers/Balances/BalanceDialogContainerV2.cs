﻿using BusinessLogic.Modules;
using CarlitosBnb.Core.Factories;
using CarlitosBnb.Dialogs.Common;
using CarlitosBnb.SeedWorks;
using Core.Connectors;
using Core.Connectors.Core;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Recognizers.Text;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Balances
{
    public class BalanceDialogContainerV2 : DialogContainer
    {
        public const string Id = "Balances";

        public const int DOCUMENT_TYPE_CI = 1;
        public const int CHANNEL_CARLITOS_BNB = 6;

        public struct Texts
        {
            public const string NOT_FOUND_BALANCE = "No encontré el saldo de tus cuentas, por favor intenta más tarde";

            public const string YOU_WANT_TO_CONTINUE = "¿Desea continuar?";

            public const string INVALID_DOCUMENT = "Disculpa este no parece ser un ci válido";

            public const string INPUT_DOCUMENT = "¡Ok! por favor, ingresa tu ci";

            public const string FAILED_REST_BALANCES = "Disculpa, pero no puedo obtener tu saldo en este momento, por favor intenta en un momento";

            public const string NOT_FOUND_OWNER_ACCOUNTS = "No encontré tus cuentas, por favor intenta más tarde.";

            public const string NOT_FOUND_ACCOUNTS_BY_DOCUMENT = "No se encontraron cuentas, con el ci especificado";

            public const string UPDATE_DATA_CONTACT = "Por favor comunícate con el contact center (78787272) del Banco para actualizar tu información.";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string MAX_RETRY_VALIDATE_DOCUMENT = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";
        }

        public static BalanceDialogContainerV2 Instance { get; } = new BalanceDialogContainerV2();

        public BalanceDialogContainerV2() : base(Id)
        {
            var waterFallSteps = new List<WaterfallStep>();
            waterFallSteps.AddRange(UserProfileDocumentWaterfallSteps.Instance.WaterfallStepsOnlyDocumentNumber);
            waterFallSteps.AddRange(AuthenticationWaterfallSteps.Instance.WaterFalls);
            waterFallSteps.Add(RequestClientBalance);
            waterFallSteps.Add(RequestClientCreditCards);
            waterFallSteps.Add(RequestClientBnbPoints);
            waterFallSteps.Add(EndBalanceDialog);

            this.Dialogs.Add(Id, waterFallSteps.ToArray());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_DOCUMENT, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.SELECT_FOR_CONTINUE, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish));
            this.Dialogs.Add(AuthenticationWaterfallSteps.Inputs.INPUT_OTP, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_CARD_ID, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
        }

        private async Task RequestClientBalance(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            conversationInfo.ProfileSession.Action = "Saldo";
            bool hasCollectionsAvailable = false;

            var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("ClientServices");

            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);
            httpClient.DefaultRequestHeaders.Add("Auth-user", conversationInfo.AuthUserToken);

            var response = await httpClient.PostV2<BaseApiResponse<List<AccountBalanceModel>>>(configuration.GetValue<string>("Services:BnbChatBotServices:ClientServices:ClientBalance"), new { });

            if (!response.Success)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            #region Retrieve Collection Info

            var httpClientCollection = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("ClientServices");

            httpClientCollection.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);
            httpClientCollection.DefaultRequestHeaders.Add("Auth-user", conversationInfo.AuthUserToken);

            var responseCollection = await httpClient.PostV2<BaseApiResponse<List<GetCollectionsByDocumentResult>>>(configuration.GetValue<string>("Services:BnbChatBotServices:ClientServices:ClientCollections"), new GetCollectionsByDocumentCommandRequest { DocumentNumber = conversationInfo.ProfileSession.DocumentNumber });

            if (responseCollection.Success && responseCollection.Body != null && responseCollection.Body.Count > 0)
            {
                hasCollectionsAvailable = true;
            }

            #endregion

            var messages = new List<IMessageActivity>();
            foreach (var balances in response.Body)
            {
                GetCollectionsByDocumentResult collectionFound = hasCollectionsAvailable ? responseCollection.Body.Find(r => r.Operation.ToString().Equals(balances.Number)) : null;

                var creator = new CardCreator();
                creator.UserName = dc.Context.Activity.From.FirstName();
                if (balances.Type.Equals(1))
                {
                    messages.Add(creator.Create(new BusinessLogic.Modules.CardModule.CardInfo()
                    {
                        Title = $"Cuenta { balances.Number }",
                        SubTitle = $"Saldo: { balances.CurrencyDescription } { balances.Balance.ToString("N", new CultureInfo("en-US")) }",
                        Buttons = new List<BusinessLogic.Modules.CardModule.CardActionInfo>()
                                {
                                    new BusinessLogic.Modules.CardModule.CardActionInfo()
                                    {
                                        Title = "Ver Movimientos",
                                        Type = ActionTypes.PostBack,
                                        Value = $"movimientos { balances.Number } { balances.Type }"
                                    }
                                },
                        Type = "HERO_CARD"
                    }));
                }
                if (balances.Type.Equals(2))
                {
                    messages.Add(creator.Create(new BusinessLogic.Modules.CardModule.CardInfo()
                    {
                        Title = $"Nro. de Operación { balances.Number }",
                        SubTitle = $"Saldo: { balances.CurrencyDescription } { balances.Balance.ToString("N", new CultureInfo("en-US")) } ({ collectionFound.Situation })",
                        Text = $"Contacto Funcionario: { collectionFound.ExecutiveName } ({ collectionFound.ExecutivePhone })",
                        Buttons = new List<BusinessLogic.Modules.CardModule.CardActionInfo>()
                                {
                                    new BusinessLogic.Modules.CardModule.CardActionInfo()
                                    {
                                        Title = "Ver Movimientos",
                                        Type = ActionTypes.PostBack,
                                        Value = $"movimientos { balances.Number } { balances.Type }"
                                    }
                                },
                        Type = "HERO_CARD"
                    }));
                }

            }

            await dc.Context.SendActivities(messages.ToArray());
            await next();
        }

        private async Task RequestClientCreditCards(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

            try
            {
                var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("ClientServices");

                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);
                httpClient.DefaultRequestHeaders.Add("Auth-user", conversationInfo.AuthUserToken);

                Response response;
                List<Card> listCards = new List<Card>();
                if (!configuration.GetValue<bool>("Dev:UseHotCodeCreditCardExtract"))
                {
                    response = await Connection.Process(new ConnectorRestPost<RequestGetCreditCards, BaseApiResponse<ResponseGetCreditCards>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:ClientServices:GetCreditCards"), new RequestGetCreditCards()));
                    var responseGetCredits = response as Response<BaseApiResponse<ResponseGetCreditCards>>;

                    if (response.IsOk && responseGetCredits.Body.Body != null && responseGetCredits.Body.Body.Cards.Count > 0)
                    {
                        conversationInfo.ProfileSession.HasCreditCards = true;

                        var messages = new List<IMessageActivity>();

                        var creator = new CardCreator();
                        creator.UserName = dc.Context.Activity.From.FirstName();
                        messages.Add(creator.Create(new BusinessLogic.Modules.CardModule.CardInfo()
                        {
                            Title = $"Tarjeta de Crédito",
                            SubTitle = $"Número: { responseGetCredits.Body.Body.Cards.ToList().First().Obfuscated }",
                            Buttons = new List<BusinessLogic.Modules.CardModule.CardActionInfo>()
                                {
                                    new BusinessLogic.Modules.CardModule.CardActionInfo()
                                    {
                                        Title = "Ver Extracto Tarjeta Crédito",
                                        Type = ActionTypes.PostBack,
                                        Value = $"Creditcardextract"
                                    }
                                },
                            Type = "HERO_CARD"
                        }));

                        await dc.Context.SendActivities(messages.ToArray());
                    }
                }
                else
                {
                    conversationInfo.ProfileSession.HasCreditCards = false;
                }
            }
            catch(Exception)
            {
                
            }
            finally
            {
                await next();
            }
        }

        private async Task RequestClientBnbPoints(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

            try
            {
                if (conversationInfo.ProfileSession.HasCreditCards)
                {
                    var httpClient = HttpClientFactory.Create();
                    var urlApiCustomerData = configuration.GetValue<string>("Services:BnbPointsCustomerData");
                    var responseApiCustomerData = await httpClient.Post<CustomerDataDto, BaseApiResponse<CustomerDataResponse>>(urlApiCustomerData, new CustomerDataDto()
                    {
                        DocumentNumber = conversationInfo.ProfileSession.DocumentNumberLong,
                        DocumentExtention = conversationInfo.ProfileSession.DocumentExtension,
                        DocumentType = DOCUMENT_TYPE_CI
                    });

                    if (!responseApiCustomerData.IsOk)
                    {
                        throw new Exception("No se encontro informacion de cliente");
                    }

                    var responseCustomerData = responseApiCustomerData as Result<BaseApiResponse<CustomerDataResponse>>;
                    if (responseCustomerData == null || !responseCustomerData.Body.Success)
                    {
                        throw new Exception("Informacion de cliente incompleta");
                    }

                    var urlApiBalancePoint = configuration.GetValue<string>("Services:BnbPointsBalancePoint");
                    var responseApiBalancePoint = await httpClient.Post<BalancePointDto, BaseApiResponse<BalancePointResponse>>(urlApiBalancePoint, new BalancePointDto()
                    {
                        LoyaltyId = responseCustomerData.Body.Body.Data[0].AtcAccount,
                        ChannelId = CHANNEL_CARLITOS_BNB
                    });

                    if (!responseApiBalancePoint.IsOk)
                    {
                        throw new Exception("No se encontro saldo de Puntos BNB");
                    }

                    var responseBalancePoint = responseApiBalancePoint as Result<BaseApiResponse<BalancePointResponse>>;
                    if (responseBalancePoint == null || !responseBalancePoint.Body.Success)
                    {
                        throw new Exception("Saldo de Puntos BNB incorrecto");
                    }

                    var messages = new List<IMessageActivity>();

                    var creator = new CardCreator();
                    creator.UserName = dc.Context.Activity.From.FirstName();
                    messages.Add(creator.Create(new BusinessLogic.Modules.CardModule.CardInfo()
                    {
                        Title = $"Puntos BNB",
                        SubTitle = $"Saldo: { responseBalancePoint.Body.Body.Data.TotalPoint.ToString("N", new CultureInfo("en-US")) }",
                        Buttons = new List<BusinessLogic.Modules.CardModule.CardActionInfo>()
                                {
                                    new BusinessLogic.Modules.CardModule.CardActionInfo()
                                    {
                                        Title = "Ver Movimientos",
                                        Type = ActionTypes.PostBack,
                                        Value = $"movimientos { responseCustomerData.Body.Body.Data[0].AtcAccount } { 3 }"
                                    }
                                },
                        Type = "HERO_CARD"
                    }));

                    await dc.Context.SendActivities(messages.ToArray());
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                await next();
            }
        }

        private async Task EndBalanceDialog(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

            await dc.Context.SendActivity($"{dc.Context.Activity.From.FirstName()}, dime en que más te puedo ayudar.");
            await dc.End();
            conversationInfo.IsCompletedTask = true;
        }
    }

    public class CustomerDataDto
    {
        public CustomerDataDto()
        {
            this.Complement = string.Empty;
        }

        public long DocumentNumber { get; set; }

        public string DocumentExtention { get; set; }

        public string Complement { get; set; }

        public int DocumentType { get; set; }
    }

    public class CustomerDataResponse
    {
        public List<DataResponse> Data { get; set; }

        public class DataResponse
        {
            public long AtcAccount { get; set; }
        }
    }

    public class BalancePointDto
    {
        public long LoyaltyId { get; set; }

        public int ChannelId { get; set; }
    }

    public class BalancePointResponse
    {
        public DataResponse Data { get; set; }

        public class DataResponse
        {
            public decimal TotalPoint { get; set; }
        }
    }

    public class Card
    {
        public string CardValue { get; set; }

        public string Obfuscated { get; set; }
    }

    public class RequestGetCreditCards { }
    public class ResponseGetCreditCards
    {
        public List<Card> Cards { get; set; }
    }

    public class AccountBalanceModel
    {
        [JsonProperty("type")]
        public int Type { get; set; }

        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("currencyDescription")]
        public string CurrencyDescription { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }

    public class GetCollectionsByDocumentCommandRequest
    {
        [JsonProperty("DocumentNumber")]
        public string DocumentNumber { get; set; }
    }

    public class GetCollectionsByDocumentResult
    {
        [JsonProperty("Operation")]
        public long Operation { get; set; }

        [JsonProperty("BalanceInDollars")]
        public decimal BalanceInDollars { get; set; }

        [JsonProperty("BalanceOriginal")]
        public decimal BalanceOriginal { get; set; }

        [JsonProperty("Currency")]
        public string Currency { get; set; }

        [JsonProperty("Situation")]
        public string Situation { get; set; }

        [JsonProperty("ExecutiveName")]
        public string ExecutiveName { get; set; }

        [JsonProperty("executivePhone")]
        public string ExecutivePhone { get; set; }
    }
}
