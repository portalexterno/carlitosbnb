﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class GetAccountsResponse
    {
        [JsonProperty("Cuentas")]
        public List<Account> Accounts { get; set; }

        [JsonProperty("Correcto")]
        public bool IsOk { get; set; }

        [JsonProperty("Mensaje")]
        public string Message { get; set; }

        [JsonProperty("TipoError")]
        public int TypeError { get; set; }
    }

    public class Account
    {
        [JsonProperty("Numero")]
        public string Number { get; set; }

        public string NumberObfuscate => $"{this.Number.Substring(0, 2)}{new string('●', this.Number.Length / 2)}{this.Number.Substring(2 + this.Number.Length / 2, this.Number.Length - (2 + this.Number.Length / 2))}";

        [JsonProperty("MonedaCodigo")]
        public int CurrencyCode { get; set; }

        [JsonProperty("MonedaDesripcion")]
        public string CurrencyDescription { get; set; }

        [JsonProperty("Saldo")]
        public double Balance { get; set; }

        [JsonProperty("Tipo")]
        public int Type { get; set; }
    }
}
