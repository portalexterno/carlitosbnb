﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers
{
    using CarlitosBnb.Core.Factories;
    using Microsoft.Bot.Builder;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Builder.Prompts;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Recognizers.Text;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Threading.Tasks;

    public class BalancesDialogContainer : DialogContainer
    {
        public const string Id = "Balances";

        public const string PATTERN_DOCUMENT = @"^([0-9]{5,9})$";

        public const string PATTERN_OTP_CODE = @"^([0-9]{4})$";

        public struct Texts
        {
            public const string FAILED_REST_BALANCES = "Disculpa, pero no puedo obtener tu saldo en este momento, por favor intenta en un momento";

            public const string NOT_FOUND_OWNER_ACCOUNTS = "No encontré tus cuentas, por favor intenta más tarde.";

            public const string NOT_FOUND_BALANCE = "No encontré el saldo de tus cuentas, por favor intenta más tarde";
        }

        public struct Inputs
        {
            public const string INPUT_DOCUMENT = "inputDocument";

            public const string OTP_CODE = "optCode";
            public const string SELECT_FOR_CONTINUE = "selectForContinue";
        }

        public static BalancesDialogContainer Instance { get; } = new BalancesDialogContainer();

        private BalancesDialogContainer() : base(Id)
        {
            this.Dialogs.Add(Id, new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    var serviceProvider = dc.Context.Services.Get<IServiceProvider>(CarlitosBnbBot.SERVICE_PROVIDER_ID);
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

                    conversationInfo.ProfileSession.Action = "Saldo";
                    //if(!conversationInfo.ContainsKey(ConversationInfo.NameKeys.CLIENTS) || !conversationInfo.ContainsKey(ConversationInfo.NameKeys.LAST_SESSION))
                    if(!conversationInfo.ProfileSession.HasSession)
                    {
                        //conversationInfo.CounterAttemptsLogin = 0;                        
                        await dc.Replace(LoginWaterfallSteps.Id, dc.ActiveDialog.State);
                        return;
                    }

                    var clients = conversationInfo.ProfileSession.Clients;
                    //if(!conversationInfo.LastSession.HasValue || conversationInfo.LastSession.Value.AddMinutes(configuration.GetValue<double>("SessionTime")) < DateTime.Now)
                    if(!conversationInfo.LastSession.HasValue || conversationInfo.LastSession.Value.AddMinutes(configuration.GetValue<double>("SessionTime")) < DateTime.Now)
                    {
                        conversationInfo.ProfileSession.Clients.Clear();
                        conversationInfo.Account = string.Empty;
                        //conversationInfo.CounterAttemptsLogin = 0;                        
                        await dc.Replace(LoginWaterfallSteps.Id, dc.ActiveDialog.State);
                        return;
                    }

                    //var url = configuration.GetValue<string>("Services:GetAccountsClient");
                    //var response = await Connection.Process(
                    //    new ConnectorRestPost<GetAccountsRequest, GetAccountsResponse>(url, new GetAccountsRequest(){
                    //        Token = ServiceLoginConst.TOKEN,
                    //        IdUserChannel = dc.Context.Activity.From.Id,
                    //        TypeChannel = ServiceLoginConst.CHANNEL,
                    //        client = client.Number
                    //    }));

                    //if(!response.IsOk)
                    //{
                    //    await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                    //    await ManagerScope.Process(dc.Context, LogInfo.Load(dc.Context.Activity.From.Id, response));
                    //    await dc.End();
                    //    return;
                    //}

                    //logService.Dispose();

                    //var responseGetAccounts = response as Response<GetAccountsResponse>;

                    //if(!responseGetAccounts.Body.IsOk)
                    //{
                    //    if(responseGetAccounts.Body.TypeError == ServiceLoginConst.SESSION_EXPIRED)
                    //    {
                    //        dc.ActiveDialog.State.Clear();
                    //        await dc.Replace(LoginWaterfallSteps.Id, new Dictionary<string, object>());
                    //    }
                    //    else
                    //    {
                    //        await dc.Context.SendActivity(Texts.NOT_FOUND_BALANCE);
                    //        await dc.End();
                    //    }

                    //    return;
                    //}

                    //if(responseGetAccounts.Body.Accounts.Count <= 0)
                    //{
                    //    await dc.Context.SendActivity(Texts.NOT_FOUND_OWNER_ACCOUNTS);
                    //    await dc.End();
                    //    return;
                    //}

                    var messages = new List<IMessageActivity>();
                    //foreach(var account in responseGetAccounts.Body.Accounts)
                    foreach(var client in clients)
                    {
                        foreach(var account in client.Accounts)
                        {
                            var creator = new CardCreator();
                            creator.UserName = dc.Context.Activity.From.FirstName();
                            messages.Add(creator.Create(new BusinessLogic.Modules.CardModule.CardInfo()
                            {
                                Title = $"{ account.Label } { account.Number }",
                                SubTitle = $"Saldo: { account.CurrencyDescription } { account.Balance.ToString("N", new CultureInfo("en-US")) }",
                                Buttons = new List<BusinessLogic.Modules.CardModule.CardActionInfo>()
                                {
                                    new BusinessLogic.Modules.CardModule.CardActionInfo()
                                    {
                                        Title = "Ver Movimientos",
                                        Type = ActionTypes.PostBack,
                                        Value = $"movimientos { account.Number }"
                                    }
                                },
                                Type = "HERO_CARD"
                            }));
                        }
                    }

                    await dc.Context.SendActivities(messages.ToArray());
                    await dc.Context.SendActivity($"{dc.Context.Activity.From.FirstName()}, dime en que más te puedo ayudar.");
                    //conversationInfo.Clear();                    
                    await dc.End();
                    conversationInfo.IsCompletedTask = true;
                }
            });

            this.Dialogs.Add(Inputs.INPUT_DOCUMENT, new Microsoft.Bot.Builder.Dialogs.TextPrompt());

            this.Dialogs.Add(LoginWaterfallSteps.Id, LoginWaterfallSteps.Instance.WaterFalls);

            this.Dialogs.Add(Inputs.OTP_CODE, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(Inputs.SELECT_FOR_CONTINUE, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish, ValidateChoice) { Style = ListStyle.SuggestedAction, RecognizerOptions = new Microsoft.Bot.Builder.Prompts.Choices.FindChoicesOptions() { AllowPartialMatches = false, NoAction = true, NoValue = true } });

            this.Dialogs.Add(SendMessageWaterfallSteps.Id, SendMessageWaterfallSteps.Instance.Waterfalls);
            //var verifyOtp = VerifyOtpWaterfallSteps.Instance;
            var verifyOtp = new VerifyOtpWaterfallSteps();
            verifyOtp.NextId = Id;
            this.Dialogs.Add(VerifyOtpWaterfallSteps.Id, verifyOtp.WaterFalls);
        }

        private async Task ValidateChoice(ITurnContext context, ChoiceResult toValidate)
        {
            var conversationInfo = context.GetConversationState<ConversationInfo>();
            conversationInfo.CounterAttemptsOtpCode++;
            var configuration = context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            //await context.SendActivity($"{conversationInfo.CounterAttemptsOtpCode} intentos");
            if (conversationInfo.CounterAttemptsOtpCode >= configuration.GetValue<int>("MaxTryCounterOtpCode"))
            {
                await context.SendActivity(VerifyOtpWaterfallSteps.Texts.INVALID_OTP_CODE);
                await context.SendActivity(VerifyOtpWaterfallSteps.Texts.MAX_RETRY_VALIDATE_OTP_CODE);
                await context.SendActivity(string.Format(VerifyOtpWaterfallSteps.Texts.CANCEL, context.Activity.From.FirstName()));
                conversationInfo.CounterAttemptsOtpCode = 0;
                return;
            }

            var userMessage = context.Activity.Text;
            if (userMessage == "3")
            {
                toValidate.Status = null;
                await Task.Delay(1);
            }
        }
    }
}
