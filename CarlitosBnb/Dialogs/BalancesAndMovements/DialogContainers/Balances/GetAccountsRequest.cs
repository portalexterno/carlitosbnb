﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements
{
    using Newtonsoft.Json;

    public class GetAccountsRequest
    {
        [JsonProperty("Token")]
        public string Token { get; set; }

        [JsonProperty("IdentificadorCanal")]
        public string IdUserChannel { get; set; }

        [JsonProperty("TipoCanal")]
        public string TypeChannel { get; set; }

        [JsonProperty("Cliente")]
        public string client { get; set; }
    }
}
