﻿namespace CarlitosBnb.Dialogs.BalanceAndMovements.DialogContainers
{
    using Microsoft.Bot.Builder.Dialogs;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ValidateCodeOtpDialogSet : DialogSet
    {
        public const string Id = "ValidateCodeOtp";

        public ValidateCodeOtpDialogSet()
        {
            this.Add(Id, new WaterfallStep[]
            {

            });
        }
    }
}
