﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers
{
    using Newtonsoft.Json;

    public class BalanceRequest
    {
        [JsonProperty("NumeroDocumento")]
        public string Document { get; set; }

        [JsonProperty("Token")]
        public string Token { get; set; }
    }
}
