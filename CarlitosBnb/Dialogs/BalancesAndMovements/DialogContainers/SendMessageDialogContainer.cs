﻿namespace CarlitosBnb.Dialogs.BalanceAndMovements.DialogContainers
{
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using PromptStatus = Microsoft.Bot.Builder.Prompts.PromptStatus;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Bot.Builder.Prompts.Choices;
    using Microsoft.Bot.Schema;

    public class SendMessageDialogContainer : DialogContainer
    {
        public const string Id = "SendMessage";

        private const string PATTERN_OTP_CODE = @"^([0-9]{4})$";

        private struct Texts
        {
            public const string FAILED_SEND_MESSAGE = "Disculpa, no pude enviar el mensaje, por favor intenta más tarde";

            public const string CORRECT_SEND_EMAIL_CELLPHONE = "El documento se validó con éxito.Te enviaré un código de verificación a tu email: {0} y al teléfono: {1}.";

            public const string CORRECT_SEND_ONLY_EMAIL = "El documento se validó con éxito. Te enviaré un código de verificación a tu email: {0}.";

            public const string CORRECT_SEND_ONLY_CELLPHONE = "El documento se validó con éxito. Te enviaré un código de verificación al teléfono: {0}.";

            public const string WAIT_SEND_CODE = "Espera un momento e ingresa el código enviado...";

            public const string RETRY_OTP_CODE = "Disculpa este no parece ser un código valido., Deseas Continuar?";

            public const string FAILED_VALIDATE_OTP_CODE = "Disculpa, no pude validar el código, por favor intenta más tarde";

            public const string INVALID_OTP_CODE = "Disculpa este código no es Valido!. Deseas continuar?";

            public const string VALID_OTP_CODE = "El código se validó con éxito. Bienvenido!!!";

            public const string INPUT_OTP_CODE = "por favor ingresa el código enviado";
        }

        private struct Inputs
        {
            public const string OTP_CODE = "optCode";
            public const string SELECT_FOR_CONTINUE = "selectForContinue";
        }

        public static SendMessageDialogContainer Instance(IConfiguration configuration)
        {
            return new SendMessageDialogContainer(configuration);
        }

        private SendMessageDialogContainer(IConfiguration configuration) : base(Id)
        {
            this.Dialogs.Add(Inputs.OTP_CODE, new TextPrompt(
                async (context, result) =>
                {
                    if (!Regex.IsMatch(result.Value, PATTERN_OTP_CODE))
                    {
                        result.Status = PromptStatus.OutOfRange;
                    }
                }));
            this.Dialogs.Add(Inputs.SELECT_FOR_CONTINUE, new ChoicePrompt("es"));
            this.Dialogs.Add(Id, new WaterfallStep[]{
                async(dc, args, next) =>
                {
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var client = conversationInfo["client"] as Client;

                    var responseSend = await Connection<SendAuthenticationMessageResponse>.Process(
                        new ConnectorRestPost<SendAuthenticationMessageRequest, SendAuthenticationMessageResponse>(configuration.GetValue<string>("Services:SendCodeOtp"), new SendAuthenticationMessageRequest()
                    {
                        Token = ServiceLoginConst.TOKEN,
                        TypeChannel = ServiceLoginConst.CHANNEL,
                        IdUserChannel = dc.Context.Activity.From.Id,
                        NumberClient = client.Accounts.Count > 0 ? client.Accounts.First().Number : string.Empty,
                        CellPhone = client.CellPhone,
                        Email = client.Email,
                        SendEmail = !string.IsNullOrEmpty(client.Email),
                        SendSMS = !string.IsNullOrEmpty(client.CellPhone)
                    })) as Response<SendAuthenticationMessageResponse>;

                    if(!responseSend.IsOk)
                    {
                        await dc.Context.SendActivity(Texts.FAILED_SEND_MESSAGE);
                        await dc.End();
                        return;
                    }

                    if(!responseSend.Body.SendCorrectSms && !responseSend.Body.SendCorrectEmail)
                    {
                        await dc.Context.SendActivity(Texts.FAILED_SEND_MESSAGE);
                        await dc.End();
                        return;
                    }

                    if(responseSend.Body.SendCorrectEmail && responseSend.Body.SendCorrectSms)
                    {
                        await dc.Context.SendActivity(string.Format(Texts.CORRECT_SEND_EMAIL_CELLPHONE, hideEmail(client.Email), hideCellPhone(client.CellPhone)));
                    }
                    else if(responseSend.Body.SendCorrectEmail)
                    {
                        await dc.Context.SendActivity(string.Format(Texts.CORRECT_SEND_ONLY_EMAIL, hideEmail(client.Email)));
                    }
                    else if(responseSend.Body.SendCorrectSms)
                    {
                        await dc.Context.SendActivity(string.Format(Texts.CORRECT_SEND_ONLY_CELLPHONE, hideCellPhone(client.CellPhone)));
                    }

                    await dc.Begin("VerifyOtp", dc.ActiveDialog.State);
                    //await dc.Prompt(Inputs.OTP_CODE, Texts.WAIT_SEND_CODE, new PromptOptions(){
                    //    RetryPromptString = Texts.RETRY_OTP_CODE
                    //});

                    //await dc.Context.SendActivity(Texts.WAIT_SEND_CODE);
                },
                //async(dc, args, next) =>
                //{
                //    var otpCode = args["Value"].ToString();
                //    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                //    var client = conversationInfo["client"] as Client;

                //    var responsevalidateOtpCode = await Connection<ValidateOtpCodeResponse>.Process(
                //        new ConnectorRestPost<ValidateOtpCodeRequest, ValidateOtpCodeResponse>(configuration.GetValue<string>("Services:ValidateOtp"), new ValidateOtpCodeRequest()
                //        {
                //            AuthenticationCode = otpCode,
                //            IdUserChannel = dc.Context.Activity.From.Id,
                //            CellPhone = client.CellPhone,
                //            Email = client.Email,
                //            Number = client.Number,
                //            Token = ServiceLoginConst.TOKEN,
                //            TypeChannel = ServiceLoginConst.CHANNEL
                //        })) as Response<ValidateOtpCodeResponse>;

                //    if(!responsevalidateOtpCode.IsOk)
                //    {
                //        await dc.Context.SendActivity(Texts.FAILED_VALIDATE_OTP_CODE);
                //        await dc.End();
                //        return;
                //    }

                //    if(!responsevalidateOtpCode.Body.IsOk)
                //    {
                //        if(responsevalidateOtpCode.Body.TypeError == 5)
                //        {
                //            await dc.Context.SendActivity(responsevalidateOtpCode.Body.Message);
                //            await dc.End();
                //            return;
                //        }
                //        else
                //        {
                //            var options = new List<Choice>()
                //            {
                //                new Choice()
                //                {
                //                    Action = new CardAction(ActionTypes.PostBack, "Si deseo continuar"),
                //                    Value = "YES"
                //                },
                //                new Choice()
                //                {
                //                    Action = new CardAction(ActionTypes.PostBack, "No"),
                //                    Value = "NO"
                //                }
                //            };

                //            await dc.Prompt(Inputs.SELECT_FOR_CONTINUE, Texts.INVALID_OTP_CODE, new ChoicePromptOptions(){
                //                Choices = options,
                //                RetryPromptString = Texts.INVALID_OTP_CODE
                //            });

                //            return;
                //        }
                //    }
                //},

            });

            this.Dialogs.Add("VerifyOtp", new WaterfallStep[] {
                async(dc, args, next) =>
                {
                    await dc.Prompt(Inputs.OTP_CODE, Texts.INPUT_OTP_CODE, new PromptOptions(){
                        RetryPromptString = Texts.RETRY_OTP_CODE
                    });
                },
                async(dc, args, next) =>
                {
                    var otpCode = args["Value"].ToString();
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var client = conversationInfo["client"] as Client;

                    var responsevalidateOtpCode = await Connection<ValidateOtpCodeResponse>.Process(
                        new ConnectorRestPost<ValidateOtpCodeRequest, ValidateOtpCodeResponse>(configuration.GetValue<string>("Services:ValidateOtp"), new ValidateOtpCodeRequest()
                        {
                            AuthenticationCode = otpCode,
                            IdUserChannel = dc.Context.Activity.From.Id,
                            CellPhone = client.CellPhone,
                            Email = client.Email,
                            Number = client.Number,
                            Token = ServiceLoginConst.TOKEN,
                            TypeChannel = ServiceLoginConst.CHANNEL
                        })) as Response<ValidateOtpCodeResponse>;

                    if(!responsevalidateOtpCode.IsOk)
                    {
                        await dc.Context.SendActivity(Texts.FAILED_VALIDATE_OTP_CODE);
                        await dc.End();
                        return;
                    }

                    if(!responsevalidateOtpCode.Body.IsOk)
                    {
                        if(responsevalidateOtpCode.Body.TypeError == 5)
                        {
                            await dc.Context.SendActivity(responsevalidateOtpCode.Body.Message);
                            await dc.End();
                            return;
                        }
                        else
                        {
                            var options = new List<Choice>()
                            {
                                new Choice()
                                {
                                    Action = new CardAction(ActionTypes.PostBack, "Si deseo continuar"),
                                    Value = "YES"
                                },
                                new Choice()
                                {
                                    Action = new CardAction(ActionTypes.PostBack, "No"),
                                    Value = "NO"
                                }
                            };

                            await dc.Prompt(Inputs.SELECT_FOR_CONTINUE, Texts.INVALID_OTP_CODE, new ChoicePromptOptions(){
                                Choices = options,
                                RetryPromptString = Texts.INVALID_OTP_CODE
                            });

                            return;
                        }
                    }

                    await dc.Replace(BalancesDialogContainer.Id, dc.ActiveDialog.State);
                },
                async(dc, args, next) =>
                {
                    var choice = (FoundChoice)args["Value"];
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var client = conversationInfo["client"] as Client;

                    if(choice.Value == "YES")
                    {
                        await dc.Replace("VerifyOtp", dc.ActiveDialog.State);
                    }
                    else
                    {
                        await dc.End();
                    }
                }
            });

            //this.Dialogs.Add(BalancesDialogContainer.Id, BalancesDialogContainer.Instance(configuration));
        }

        private static string hideEmail(string email)
        {
            var emailSplit = email.Split('@');

            return string.Format("{0}{1}@{2}", email.Substring(0, 2), new string('●', emailSplit[0].Length - 2), emailSplit[1]);
        }

        private static string hideCellPhone(string cellPhone)
        {
            return string.Format("{0}{1}", new string('●', 4), cellPhone.Substring(4, cellPhone.Length - 4));
        }
    }
}
