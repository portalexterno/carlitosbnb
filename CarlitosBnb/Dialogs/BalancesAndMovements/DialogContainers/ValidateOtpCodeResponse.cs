﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers
{
    using Newtonsoft.Json;

    public class ValidateOtpCodeResponse
    {
        [JsonProperty("Correcto")]
        public bool IsOk { get; set; }

        [JsonProperty("Mensaje")]
        public string Message { get; set; }

        [JsonProperty("TipoError")]
        public int TypeError { get; set; }
    }
}
