﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers
{
    using Newtonsoft.Json;

    public class ValidateOtpCodeRequest
    {
        [JsonProperty("Token")]
        public string Token { get; set; }

        [JsonProperty("IdentificadorUsuarioCanal")]
        public string IdUserChannel { get; set; }

        [JsonProperty("TipoCanal")]
        public string TypeChannel { get; set; }

        [JsonProperty("NumeroCliente")]
        public string Number { get; set; }

        [JsonProperty("NumeroCelular")]
        public string CellPhone { get; set; }

        [JsonProperty("Correo")]
        public string Email { get; set; }

        [JsonProperty("CodigoAutenticacion")]
        public string AuthenticationCode { get; set; }
    }
}
