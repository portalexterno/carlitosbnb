﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers
{
    using CarlitosBnb.Dialogs.Common;
    using CarlitosBnb.SeedWorks;
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Builder.Prompts.Choices;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    public class LoginWaterfallSteps
    {
        public const string Id = "LoginWaterfall";

        public static LoginWaterfallSteps Instance { get; } = new LoginWaterfallSteps();

        private struct Inputs
        {
            public const string INPUT_DOCUMENT = "inputDocument";
        }

        public struct Texts
        {
            public const string NOT_FOUND_BALANCE = "No encontré el saldo de tus cuentas, por favor intenta más tarde";

            public const string YOU_WANT_TO_CONTINUE = "¿Desea continuar?";

            public const string INVALID_DOCUMENT = "Disculpa este no parece ser un ci válido";

            public const string INPUT_DOCUMENT = "¡Ok! por favor, ingresa tu ci";

            public const string FAILED_REST_BALANCES = "Disculpa, pero no puedo obtener tu saldo en este momento, por favor intenta en un momento";

            public const string NOT_FOUND_OWNER_ACCOUNTS = "No encontré tus cuentas, por favor intenta más tarde.";

            public const string NOT_FOUND_ACCOUNTS_BY_DOCUMENT = "No se encontraron cuentas, con el número de documento";

            public const string UPDATE_DATA_CONTACT = "Por favor comunícate con el contact center (78787272) del Banco para actualizar tu información.";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string MAX_RETRY_VALIDATE_DOCUMENT = "Has superado los intentos permitidos, por favor intenta más tarde, gracias";
        }

        public WaterfallStep[] WaterFalls => new WaterfallStep[]
            {
                this.InputDocument,
                this.ValidateDocument,
                this.RetryValidateDocument
            };


        private async Task InputDocument(DialogContext dc, object args, SkipStepFunction next)
        {
            await dc.Prompt(Inputs.INPUT_DOCUMENT, Texts.INPUT_DOCUMENT);
        }

        private async Task ValidateDocument(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var document = new string(args["Value"].ToString().Where(c => char.IsDigit(c)).ToArray());
            conversationInfo.ProfileSession.DocumentNumber = document;

            #region Development
            var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("AuthenticationServices");
            var responseApplicationToken = await Connection.Process(new ConnectorRestPost<RequestGetApplicationToken, BaseApiResponse<ResponseGetApplicationToken>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:GetTokenApplication"), new RequestGetApplicationToken()
            {
                ApplicationKey = configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:ApplicationKey"),
                ApplicationSecret = configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:ApplicationSecret"),
                ChannelAccountId = dc.Context.Activity.From.Id,
                ConversationId = dc.Context.Activity.Conversation.Id
            }));

            //var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("BnbChatBotServices");
            //var responseApplicationToken = await Connection.Process(new ConnectorRestPost<RequestGetApplicationToken, BaseApiResponse<ResponseGetApplicationToken>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:GetTokenApplication"), new RequestGetApplicationToken()
            //{
            //    ApplicationKey = configuration.GetValue<string>("Services:BnbChatBotServices:ApplicationKey"),
            //    ApplicationSecret = configuration.GetValue<string>("Services:BnbChatBotServices:ApplicationSecret"),
            //    ChannelAccountId = dc.Context.Activity.From.Id,
            //    ConversationId = dc.Context.Activity.Conversation.Id
            //}));
            #endregion

            if (!responseApplicationToken.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                dc.EndAll();
                return;
            }

            var r = responseApplicationToken as Response<BaseApiResponse<ResponseGetApplicationToken>>;
            conversationInfo.AuthToken = r.Body.Body.JwtToken;

            if (!Regex.IsMatch(document, BalancesDialogContainer.PATTERN_DOCUMENT))
            {
                var options = new List<Choice>()
                    {
                        new Choice()
                        {
                            Action = new CardAction(title: "Sí deseo continuar", type: ActionTypes.PostBack, value: "YES"),
                            //Value = PromptValue.Create(value: "YES", isNotUnknownIntent: true)
                            Value = "YES",
                            Synonyms = new List<string>{ "Si", "Sí", "si" }
                        },
                        new Choice()
                        {
                            Action = new CardAction(title: "No", type: ActionTypes.PostBack, value: "NO"),
                            //Value = PromptValue.Create(value: "NO", isNotUnknownIntent: true)
                            Value = "NO",
                            Synonyms = new List<string>{ "No", "no" }
                        }
                    };

                conversationInfo.CounterAttemptsLogin++;

                if (conversationInfo.CounterAttemptsLogin >= configuration.GetValue<int>("MaxTryCounterLogin"))
                {
                    await dc.Context.SendActivity(Texts.INVALID_DOCUMENT);
                    await dc.Context.SendActivity(Texts.MAX_RETRY_VALIDATE_DOCUMENT);
                    await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                    conversationInfo.CounterAttemptsLogin = 0;
                    dc.EndAll();
                    return;
                }

                try
                {
                    await dc.Prompt(BalancesDialogContainer.Inputs.SELECT_FOR_CONTINUE, $"{ Texts.INVALID_DOCUMENT }, { Texts.YOU_WANT_TO_CONTINUE } ", new ChoicePromptOptions()
                    {
                        Choices = options,
                        RetryPromptActivity = ChoiceFactory.SuggestedAction(options, "Por favor escriba/seleccione una de las opciones: Si/No") as Activity
                        //RetryPromptString = $"{ Texts.INVALID_DOCUMENT }, { Texts.YOU_WANT_TO_CONTINUE } " + (conversationInfo.IsAudio ? $"Entendí que dijiste {document}" : string.Empty)
                    });
                }
                catch (Exception e)
                {
                    var ex = e;
                }

                return;
            }

            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);

            #region Development
            var responseRequestUserToken = await Connection.Process(new ConnectorRestPost<RequestRequesttUserToken, BaseApiResponse<ResponseRequestUserToken>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:AuthenticationServices:RequestTokenUser"), new RequestRequesttUserToken()
            {
                DocumentNumber = conversationInfo.ProfileSession.DocumentNumber,
                DocumentComplement = string.Empty
            }));

            //var responseRequestUserToken = await Connection.Process(new ConnectorRestPost<RequestRequesttUserToken, BaseApiResponse<ResponseRequestUserToken>>(httpClient, configuration.GetValue<string>("Services:BnbChatBotServices:RequestTokenUser"), new RequestRequesttUserToken()
            //{
            //    DocumentNumber = conversationInfo.ProfileSession.DocumentNumber,
            //    DocumentComplement = string.Empty
            //}));
            #endregion

            if (!responseRequestUserToken.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                dc.EndAll();
                return;
            }

            var response = await Connection.Process(new ConnectorRestPost<BalanceRequest, BalanceResponse>(HttpClientFactory.Create(), configuration.GetValue<string>("Services:ValidateDocument"), new BalanceRequest()
            {
                Document = document,
                Token = "CarlitosBNB"
            }));

            if (!response.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                //conversationInfo.Clear();
                dc.EndAll();
                return;
            }

            var responseBalance = response as Response<BalanceResponse>;

            if (!responseBalance.Body.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_BALANCES);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            if (responseBalance.Body.Clients.Count <= 0)
            {
                await dc.Context.SendActivity(Texts.NOT_FOUND_ACCOUNTS_BY_DOCUMENT);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            var clients = new List<Common.Client>(responseBalance.Body.Clients.Where(c => c.Accounts.Count > 0).ToList());

            if (clients == null)
            {
                await dc.Context.SendActivity(Texts.NOT_FOUND_ACCOUNTS_BY_DOCUMENT);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            if (clients.Any(c => string.IsNullOrWhiteSpace(c.CellPhone)) && clients.Any(c => string.IsNullOrWhiteSpace(c.Email)))
            {
                await dc.Context.SendActivity(Texts.UPDATE_DATA_CONTACT);
                dc.EndAll();
                return;
            }

            conversationInfo.ProfileSession.Clients = clients;

            await dc.Replace(SendMessageWaterfallSteps.Id, dc.ActiveDialog.State);
        }

        private async Task RetryValidateDocument(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var choice = (FoundChoice)args["Value"];

            if (choice.Value == "YES")
            {
                await dc.Replace(Id, dc.ActiveDialog.State);
            }
            else
            {
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                dc.EndAll();
            }
        }
    }
}
