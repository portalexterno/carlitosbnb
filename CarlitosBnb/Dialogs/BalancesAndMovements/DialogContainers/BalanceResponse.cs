﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    public enum TipoErrorRespuesta
    {
        Ninguno = 0,
        NoControlado = 2,
        ValidacionDatosEntrada = 6
    }

    public class BalanceResponse
    {
        [JsonProperty("Correcto")]
        public bool IsOk { get; set; }

        [JsonProperty("Mensaje")]
        public string Message { get; set; }

        [JsonProperty("TipoError")]
        public TipoErrorRespuesta TypeError { get; set; }

        [JsonProperty("Clientes")]
        public List<Common.Client> Clients { get; set; }
    }

    public class Client
    {
        [JsonProperty("Celular")]
        public string CellPhone { get; set; }

        public string CellPhoneObfuscate
        {
            get
            {
                if (string.IsNullOrEmpty(this.CellPhone))
                    return string.Empty;

                return string.Format("{0}{1}", new string('●', 4), this.CellPhone.Substring(4, this.CellPhone.Length - 4));
            }
        }

        [JsonProperty("CorreoElectronico")]
        public string Email { get; set; }

        public string EmailObfuscate
        {
            get
            {
                if (string.IsNullOrEmpty(this.Email))
                    return string.Empty;

                if (!Regex.IsMatch(this.Email.Trim(), @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                    return string.Empty;
                //var emailSplit = this.Email.Split('@');
                //return string.Format("{0}{1}@{2}", this.Email.Substring(0, 2), new string('●', emailSplit[0].Length - 2), emailSplit[1]);
                return Regex.Replace(this.Email.Trim(), @"(?<=[\w]{1})[\w-\._\+%]*(?=[\w]{1}@)", m => new string('●', m.Length));
            }
        }

        [JsonProperty("NumeroCliente")]
        public string Number { get; set; }

        [JsonProperty("TipoCliente")]
        public string Type { get; set; }

        [JsonProperty("NombresCliente")]
        public string Name { get; set; }

        [JsonProperty("ApellidoPaternoCliente")]
        public string LastName { get; set; }

        [JsonProperty("ApellidoMaternoCliente")]
        public string SecondLastName { get; set; }

        [JsonProperty("Cuentas")]
        public List<Account> Accounts { get; set; }
    }

    public class Account
    {
        [JsonProperty("Numero")]
        public string Number { get; set; }

        public string NumberObfuscate => $"{this.Number.Substring(0, 2)}{new string('●', 4)}{this.Number.Substring(6, this.Number.Length - 6)}";

        [JsonProperty("MonedaCodigo")]
        public int CurrencyCode { get; set; }

        [JsonProperty("MonedaDesripcion")]
        public string CurrencyDescription { get; set; }

        [JsonProperty("Saldo")]
        public decimal Balance { get; set; }

        [JsonProperty("SaldoCorrecto")]
        public bool IsCorrectBalance { get; set; }

        [JsonProperty("Tipo")]
        public int Type { get; set; }

        public string Label
        {
            get
            {
                return accountTypes.GetValueOrDefault(this.Type, "BNB Puntos No: ");
            }
        }

        private Dictionary<int, string> accountTypes = new Dictionary<int, string>
        {
            { 1, "Cuenta: " },
            { 2, "Crédito No: " }
        };
    }
}
