﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers
{
    using Newtonsoft.Json;

    public class SendAuthenticationMessageRequest
    {
        [JsonProperty("Token")]
        public string Token { get; set; }

        [JsonProperty("IdentificadorUsuarioCanal")]
        public string IdUserChannel { get; set; }

        [JsonProperty("TipoCanal")]
        public string TypeChannel { get; set; }

        [JsonProperty("NumeroCliente")]
        public string NumberClient { get; set; }

        [JsonProperty("NumeroCelular")]
        public string CellPhone { get; set; }

        [JsonProperty("Correo")]
        public string Email { get; set; }

        [JsonProperty("EnviarSMS")]
        public bool SendSMS { get; set; }

        [JsonProperty("EnviarCorreo")]
        public bool SendEmail { get; set; }
    }
}
