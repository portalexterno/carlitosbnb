﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers
{
    using Newtonsoft.Json;

    public class SendAuthenticationMessageResponse
    {
        [JsonProperty("EnvioCorrectoSMS")]
        public bool SendCorrectSms { get; set; }

        [JsonProperty("EnvioCorrectoCorreo")]
        public bool SendCorrectEmail { get; set; }

        [JsonProperty("Correcto")]
        public bool IsOk { get; set; }

        [JsonProperty("Mensaje")]
        public string Message { get; set; }

        [JsonProperty("TipoError")]
        public int TypeError { get; set; }
    }
}
