﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    using Newtonsoft.Json;

    public class GetSavingsAccountMovementsRequest : GetMovementsRequest
    {
        [JsonProperty("NumeroCuenta")]
        public string Account { get; set; }
    }
}
