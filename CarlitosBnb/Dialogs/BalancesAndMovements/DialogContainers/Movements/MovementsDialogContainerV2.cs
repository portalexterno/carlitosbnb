﻿using CarlitosBnb.Core.Factories;
using CarlitosBnb.Dialogs.Common;
using CarlitosBnb.Middlewares.DialogFlow;
using CarlitosBnb.SeedWorks;
using Google.Cloud.Dialogflow.V2;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Recognizers.Text;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    public class MovementsDialogContainerV2 : DialogContainer
    {
        public const string Id = "Movements";

        private struct Texts
        {
            public const string FAILED_REST_MOVEMENTS = "Disculpa, pero no puedo obtener tus movimientos en este momento, por favor intenta mas tarde.";

            public const string NOT_FOUND_MOVEMENTS = "No encontré los movimientos, por favor intenta mas tarde.";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";
        }

        public static MovementsDialogContainerV2 Instance { get; } = new MovementsDialogContainerV2();

        public MovementsDialogContainerV2() : base(Id)
        {
            var waterFallSteps = new List<WaterfallStep>();
            waterFallSteps.AddRange(UserProfileDocumentWaterfallSteps.Instance.WaterfallStepsOnlyDocumentNumber);
            waterFallSteps.AddRange(AuthenticationWaterfallSteps.Instance.WaterFalls);
            waterFallSteps.Add(RequestClientMovements);
            this.Dialogs.Add(Id, waterFallSteps.ToArray());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_DOCUMENT, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.SELECT_FOR_CONTINUE, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish));
            this.Dialogs.Add(AuthenticationWaterfallSteps.Inputs.INPUT_OTP, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_CARD_ID, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
        }

        private async Task RequestClientMovements(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var queryResult = dc.Context.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);
            conversationInfo.ProfileSession.Action = "Movimientos";
            if (queryResult.Parameters != null && queryResult.Parameters.Fields.ContainsKey("account"))
            {
                conversationInfo.Account = queryResult.Parameters.Fields["account"].NumberValue.ToString();
            }

            var httpClient = dc.Context.Services.Get<IHttpClientFactory>("HttpClientFactory").CreateClient("ClientServices");

            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", conversationInfo.AuthToken);
            httpClient.DefaultRequestHeaders.Add("Auth-user", conversationInfo.AuthUserToken);

            var response = await httpClient.PostV2<BaseApiResponse<List<AccountMovementModel>>>(configuration.GetValue<string>("Services:BnbChatBotServices:ClientServices:ClientMovements"), new ClientMovementsDto
            {
                Account = conversationInfo.Account,
                Type = Convert.ToInt32(queryResult.Parameters.Fields["type"].NumberValue)
            });

            if (!response.Success)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_MOVEMENTS);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            var messages = new List<IMessageActivity>();

            foreach (var movement in response.Body.Take(configuration.GetValue<int>("NumberMovements")))
            {
                var creator = new CardCreator();
                creator.UserName = dc.Context.Activity.From.FirstName();
                var date = Convert.ToDateTime(movement.Date);
                messages.Add(creator.Create(new BusinessLogic.Modules.CardModule.CardInfo()
                {
                    Title = $"{ date.ToShortDateString() } {movement.Time}        { movement.Amount }",
                    SubTitle = $"{ movement.MovementType }     { movement.Description }",
                    Type = "HERO_CARD"
                }));
            }

            await dc.Context.SendActivities(messages.ToArray());
            await dc.Context.SendActivity($"{dc.Context.Activity.From.FirstName()}, dime en que más te puedo ayudar.");
            await dc.End();
            conversationInfo.IsCompletedTask = true;
        }
    }

    public class ClientMovementsDto
    {
        [JsonProperty("account")]
        public string Account { get; set; }

        [JsonProperty("type")]
        public int Type { get; set; }
    }

    public class AccountMovementModel
    {
        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("movementType")]
        public string MovementType { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
