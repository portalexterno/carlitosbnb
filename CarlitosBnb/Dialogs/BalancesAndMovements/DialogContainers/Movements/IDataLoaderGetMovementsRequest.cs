﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    public interface IDataLoaderGetMovementsRequest
    {
        GetMovementsRequest Load(Client client);
    }

    public class FactoryDataLoaderGetMovementsRequest
    {
        public GetMovementsRequest Create(IDataLoaderGetMovementsRequest loader, Client client)
        {
            return loader.Load(client);
        }
    }

    //public class DataLoaderSavingsAccountMovement : IDataLoaderGetMovementsRequest
    //{
    //    public GetMovementsRequest Load(Client client, string account)
    //    {
    //        return new GetSavingsAccountMovementsRequest()
    //        {
    //            Account = account,
    //            IdUserChannel = 
    //        };
    //    }
    //}
}
