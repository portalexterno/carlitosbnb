﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    using Newtonsoft.Json;

    public class GetMovementsRequest
    {
        [JsonProperty("Token")]
        public string Token { get; set; }

        [JsonProperty("IdentificadorCanal")]
        public string IdUserChannel { get; set; }

        [JsonProperty("TipoCanal")]
        public string TypeChannel { get; set; }
    }
}
