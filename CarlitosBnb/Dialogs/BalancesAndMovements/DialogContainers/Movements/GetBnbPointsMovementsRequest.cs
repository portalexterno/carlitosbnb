﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    using Newtonsoft.Json;

    public class GetBnbPointsMovementsRequest : GetMovementsRequest
    {
        [JsonProperty("NroCuenta")]
        public string Account { get; set; }
    }
}