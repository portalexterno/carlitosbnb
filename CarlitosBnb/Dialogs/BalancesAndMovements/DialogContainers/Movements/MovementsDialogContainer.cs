﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    using CarlitosBnb.Core.Factories;
    using CarlitosBnb.Middlewares.DialogFlow;
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Google.Cloud.Dialogflow.V2;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;

    public class MovementsDialogContainer : DialogContainer
    {
        public const string Id = "Movements";

        private const string PATTERN_DOCUMENT = @"^([0-9]{5,9})$";

        private const string PATTERN_OTP_CODE = @"^([0-9]{4})$";

        public static MovementsDialogContainer Instance { get; } = new MovementsDialogContainer();

        private struct Texts
        {
            public const string FAILED_REST_MOVEMENTS = "Disculpa, pero no puedo obtener tus movimientos en este momento, por favor intenta mas tarde.";

            public const string NOT_FOUND_MOVEMENTS = "No encontré los movimientos, por favor intenta mas tarde.";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";
        }

        private struct Inputs
        {
            public const string INPUT_DOCUMENT = "inputDocument";

            public const string OTP_CODE = "optCode";
            public const string SELECT_FOR_CONTINUE = "selectForContinue";
        }

        public MovementsDialogContainer() : base(Id)
        {
            this.Dialogs.Add(Id, new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
                    var result = dc.Context.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);

                    conversationInfo.ProfileSession.Action = "Movimientos";
                    if(result.Parameters != null && result.Parameters.Fields.ContainsKey("number"))
                    {
                        var numberAccountFromDialogFlow = result.Parameters.Fields["number"].NumberValue.ToString();
                        if(!conversationInfo.ContainsKey(ConversationInfo.NameKeys.ACCOUNT))
                        {
                            conversationInfo.Add(ConversationInfo.NameKeys.ACCOUNT, numberAccountFromDialogFlow);
                        }
                        else
                        {
                            conversationInfo.Account = numberAccountFromDialogFlow;
                        }
                    }

                    //if(!conversationInfo.ContainsKey(ConversationInfo.NameKeys.CLIENTS))
                    if(!conversationInfo.ProfileSession.HasSession)
                    {
                        conversationInfo.CounterAttemptsLogin = 0;
                        await dc.Replace(LoginWaterfallSteps.Id, dc.ActiveDialog.State);
                        return;
                    }

                    var clients = conversationInfo.ProfileSession.Clients;
                    if(!conversationInfo.LastSession.HasValue || conversationInfo.LastSession.Value.AddMinutes(configuration.GetValue<double>("SessionTime")) < DateTime.Now)
                    {
                        conversationInfo.CounterAttemptsLogin = 0;
                        await dc.Replace(LoginWaterfallSteps.Id, dc.ActiveDialog.State);
                        return;
                    }

                    var numberAccount = conversationInfo.Account;

                    //var a = from c in clients.SelectMany(cli => cli.Accounts)
                    //        where c.Number == numberAccount
                    //        select c;

                    var account = clients.SelectMany(c => c.Accounts).SingleOrDefault(a => a.Number == numberAccount);

                    if(account == null)
                    {
                        await dc.Context.SendActivity("No pude encontrar los datos de tu cuenta, por favor intenta nuevamente");
                        return;
                    }

                    string url;
                    GetMovementsRequest request;
                    Response responseGetMovements;

                    if(account.Type == 1)
                    {
                        request = new GetSavingsAccountMovementsRequest()
                        {
                            Account = account.Number,
                            IdUserChannel = dc.Context.Activity.From.Id,
                            Token = ServiceLoginConst.TOKEN,
                            TypeChannel = ServiceLoginConst.CHANNEL
                        };
                        url = configuration.GetValue<string>("Services:GetSavingsMovements");
                        responseGetMovements = await Connection.Process(
                        new ConnectorRestPost<GetMovementsRequest, GetMovementsResponse<SavingsAccountMovement>>(HttpClientFactory.Create(), url, request));
                    }
                    else if(account.Type == 2)
                    {
                        request = new GetCreditMovementsRequest()
                        {
                            IdUserChannel = dc.Context.Activity.From.Id,
                            NumberClient = account.Number,
                            NumberOperation = account.Number,
                            Token = ServiceLoginConst.TOKEN,
                            TypeChannel = ServiceLoginConst.CHANNEL
                        };
                        url = configuration.GetValue<string>("Services:GetCreditMovements");
                        responseGetMovements = await Connection.Process(
                        new ConnectorRestPost<GetMovementsRequest, GetCreditMovementsResponse<CreditMovement>>(HttpClientFactory.Create(), url, request));
                    }
                    else
                    {
                        request = new GetBnbPointsMovementsRequest()
                        {
                            IdUserChannel = dc.Context.Activity.From.Id,
                            Token = ServiceLoginConst.TOKEN,
                            TypeChannel = ServiceLoginConst.CHANNEL,
                            Account = account.Number
                        };
                        url = configuration.GetValue<string>("Services:GetBnbPointsMovements");
                        responseGetMovements = await Connection.Process(
                        new ConnectorRestPost<GetMovementsRequest, GetPointMovementsResponse<BnbPointsMovement>>(HttpClientFactory.Create(), url, request));
                    }

                    if(!responseGetMovements.IsOk)
                    {
                        await dc.Context.SendActivity(Texts.FAILED_REST_MOVEMENTS);
                        return;
                    }

                    var messages = new List<IMessageActivity>();

                    if(account.Type == 1)
                    //if(responseGetMovements is Response<GetMovementsResponse<SavingsAccountMovement>>)
                    {
                        var savingAccountMovements = responseGetMovements as Response<GetMovementsResponse<SavingsAccountMovement>>;

                        if(!savingAccountMovements.Body.IsOk)
                        {
                            if(savingAccountMovements.Body.TypeError == ServiceLoginConst.SESSION_EXPIRED)
                            {
                                dc.ActiveDialog.State.Clear();
                                //await dc.Replace(LoginDialogContainer.Id, new Dictionary<string, object>());
                                await dc.Replace(LoginWaterfallSteps.Id, new Dictionary<string, object>());
                            }
                            else
                            {
                                await dc.Context.SendActivity(Texts.NOT_FOUND_MOVEMENTS);
                                //conversationInfo.Clear();
                                await dc.End();
                            }

                            return;
                        }


                        if(savingAccountMovements.Body.Movements.Count <= 0)
                        {
                            await dc.Context.SendActivity(Texts.NOT_FOUND_MOVEMENTS);
                            //conversationInfo.Clear();
                            await dc.End();
                            return;
                        }


                        foreach(var movement in savingAccountMovements.Body.Movements.Take(configuration.GetValue<int>("NumberMovements")))
                        {
                            var creator = new CardCreator();
                            creator.UserName = dc.Context.Activity.From.FirstName();
                            var date = Convert.ToDateTime(movement.Date);
                            messages.Add(creator.Create(new BusinessLogic.Modules.CardModule.CardInfo()
                            {
                                Title = $"{ date.ToShortDateString() } {movement.Time}        { movement.Currency }.{movement.Amount.ToString("N", new CultureInfo("en-US"))}",
                                SubTitle = $"{ movement.Description }     { movement.Office }",
                                Type = "HERO_CARD"
                            }));
                        }
                    }
                    else if(account.Type == 2)
                    //else if(responseGetMovements is Response<GetMovementsResponse<CreditMovement>>)
                    {
                        var creditMovements = responseGetMovements as Response<GetCreditMovementsResponse<CreditMovement>>;
                        if(!creditMovements.Body.IsOk)
                        {
                            if(creditMovements.Body.TypeError == ServiceLoginConst.SESSION_EXPIRED)
                            {
                                dc.ActiveDialog.State.Clear();
                                //await dc.Replace(LoginDialogContainer.Id, new Dictionary<string, object>());
                                await dc.Replace(LoginWaterfallSteps.Id, new Dictionary<string, object>());
                            }
                            else
                            {
                                await dc.Context.SendActivity(Texts.NOT_FOUND_MOVEMENTS);
                                //conversationInfo.Clear();
                                await dc.End();
                            }

                            return;
                        }


                        if(creditMovements.Body.Movements.Count <= 0)
                        {
                            await dc.Context.SendActivity(Texts.NOT_FOUND_MOVEMENTS);
                            //conversationInfo.Clear();
                            await dc.End();
                            return;
                        }

                        foreach(var movement in creditMovements.Body.Movements.Take(configuration.GetValue<int>("NumberMovements")))
                        {
                            var creator = new CardCreator();
                            creator.UserName = dc.Context.Activity.From.FirstName();
                            var date = Convert.ToDateTime(movement.Date);
                            messages.Add(creator.Create(new BusinessLogic.Modules.CardModule.CardInfo()
                            {
                                Title = $"{ date.ToShortDateString() }        {date.ToLongTimeString()}        { movement.Currency }.{movement.Quota.ToString("N", new CultureInfo("en-US"))}",
                                SubTitle = $"Nro Operación: { movement.Operation }\n",
                                Type = "HERO_CARD"
                            }));
                        }
                    }
                    //else if(responseGetMovements is Response<GetMovementsResponse<BnbPointsMovement>>)
                    else
                    {
                        var bnbPointsMovements = responseGetMovements as Response<GetPointMovementsResponse<BnbPointsMovement>>;

                        if(!bnbPointsMovements.Body.IsOk)
                        {
                            if(bnbPointsMovements.Body.TypeError == ServiceLoginConst.SESSION_EXPIRED)
                            {
                                dc.ActiveDialog.State.Clear();
                                //await dc.Replace(LoginDialogContainer.Id, new Dictionary<string, object>());
                                await dc.Replace(LoginWaterfallSteps.Id, new Dictionary<string, object>());
                            }
                            else
                            {
                                await dc.Context.SendActivity(Texts.NOT_FOUND_MOVEMENTS);
                                //conversationInfo.Clear();
                                await dc.End();
                            }

                            return;
                        }


                        if(bnbPointsMovements.Body.Movements.Count <= 0)
                        {
                            await dc.Context.SendActivity(Texts.NOT_FOUND_MOVEMENTS);
                            //conversationInfo.Clear();
                            await dc.End();
                            return;
                        }

                        foreach(var movement in bnbPointsMovements.Body.Movements.Take(configuration.GetValue<int>("NumberMovements")))
                        {
                            var creator = new CardCreator();
                            creator.UserName = dc.Context.Activity.From.FirstName();
                            var date = Convert.ToDateTime(movement.Date);
                            messages.Add(creator.Create(new BusinessLogic.Modules.CardModule.CardInfo()
                            {
                                Title = $"{ date.ToShortDateString() }        Punto(s). { movement.Points }\n",
                                SubTitle = $"Total Consumo: { movement.AmountCalculation }",
                                Type = "HERO_CARD"
                            }));
                        }
                    }
                    //else
                    //{
                    //    await dc.Context.SendActivity($"No pude reconocer el tipo de tu cuenta, por favor intenta nuevamente");
                    //    await dc.End();
                    //    return;
                    //}

                    await dc.Context.SendActivities(messages.ToArray());
                    await dc.Context.SendActivity($"{dc.Context.Activity.From.FirstName()}, dime en que más te puedo ayudar.");
                    //conversationInfo.Clear();
                    conversationInfo.ProfileSession.Clients = clients;
                    await dc.End();
                    conversationInfo.IsCompletedTask = true;
                }
            });

            this.Dialogs.Add(Inputs.INPUT_DOCUMENT, new Microsoft.Bot.Builder.Dialogs.TextPrompt());

            this.Dialogs.Add(LoginWaterfallSteps.Id, LoginWaterfallSteps.Instance.WaterFalls);

            this.Dialogs.Add(Inputs.OTP_CODE, new TextPrompt());
            this.Dialogs.Add(Inputs.SELECT_FOR_CONTINUE, new ChoicePrompt("es"));

            this.Dialogs.Add(SendMessageWaterfallSteps.Id, SendMessageWaterfallSteps.Instance.Waterfalls);
            //var verifyOtp = VerifyOtpWaterfallSteps.Instance;
            var verifyOtp = new VerifyOtpWaterfallSteps();
            verifyOtp.NextId = Id;
            this.Dialogs.Add(VerifyOtpWaterfallSteps.Id, verifyOtp.WaterFalls);
        }
    }
}
