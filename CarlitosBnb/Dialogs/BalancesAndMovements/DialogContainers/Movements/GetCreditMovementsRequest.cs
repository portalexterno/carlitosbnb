﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    using Newtonsoft.Json;

    public class GetCreditMovementsRequest : GetMovementsRequest
    {
        [JsonProperty("Nro_Cliente")]
        public string NumberClient { get; set; }

        [JsonProperty("Nro_Operacion")]
        public string NumberOperation { get; set; }
    }
}
