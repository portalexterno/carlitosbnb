﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    using Newtonsoft.Json;
    using System;

    public class CreditMovement : IMovement
    {
        private const string CURRENCY_BOL = "Bs";

        private const string CURRENCY_SUS = "$us";

        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Operaion")]
        public long Operation { get; set; }

        [JsonProperty("Correlativo")]
        public int Correlative { get; set; }

        [JsonProperty("Fecha")]
        public DateTime Date { get; set; }

        [JsonProperty("Cuota")]
        public double Quota { get; set; }

        [JsonProperty("CodigoMoneda")]
        public int CurrencyCode { get; set; }

        public string Currency => this.CurrencyCode == 0 ? CURRENCY_BOL : CURRENCY_SUS;
    }
}
