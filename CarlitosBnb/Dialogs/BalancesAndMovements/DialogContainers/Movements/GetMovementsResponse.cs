﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class GetMovementsResponse
    {
        [JsonProperty("Correcto")]
        public bool IsOk { get; set; }

        [JsonProperty("Mensaje")]
        public string Message { get; set; }

        [JsonProperty("TipoError")]
        public int TypeError { get; set; }
    }

    public class GetMovementsResponse<Movement> : GetMovementsResponse
        where Movement : IMovement
    {
        [JsonProperty("Movimientos")]
        public List<Movement> Movements { get; set; }
    }

    public class GetCreditMovementsResponse<Movement> : GetMovementsResponse
        where Movement : IMovement
    {
        [JsonProperty("MovimientosCredito")]
        public List<Movement> Movements { get; set; }
    }

    public class GetPointMovementsResponse<Movement> : GetMovementsResponse
        where Movement : IMovement
    {
        [JsonProperty("MovimientosCuentaPuntos")]
        public List<Movement> Movements { get; set; }
    }
}

