﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    using Newtonsoft.Json;
    using System;

    public interface IMovement { }
    public class BnbPointsMovement : IMovement
    {
        [JsonProperty("NroCuenta")]
        public long NumberAccount { get; set; }

        [JsonProperty("Fecha")]
        public DateTime Date { get; set; }

        [JsonProperty("Producto")]
        public string Product { get; set; }

        [JsonProperty("subProducto")]
        public int SubProduct { get; set; }

        [JsonProperty("Proceso")]
        public string Process { get; set; }

        [JsonProperty("tipoTransaccion")]
        public int TypeTransaction { get; set; }

        [JsonProperty("Referencia")]
        public string Reference { get; set; }

        [JsonProperty("Cantidad")]
        public int Quantity { get; set; }

        [JsonProperty("porcentajeCalculo")]
        public int PercentageCalculation { get; set; }

        [JsonProperty("importeCalculo")]
        public double AmountCalculation { get; set; }

        [JsonProperty("Puntos")]
        public double Points { get; set; }
    }
}
