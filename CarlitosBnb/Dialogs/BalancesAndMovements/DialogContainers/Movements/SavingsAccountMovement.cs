﻿namespace CarlitosBnb.Dialogs.BalancesAndMovements.DialogContainers.Movements
{
    using Newtonsoft.Json;

    public class SavingsAccountMovement : IMovement
    {
        [JsonProperty("Correlativo")]
        public string Correlative { get; set; }

        [JsonProperty("Fecha")]
        public string Date { get; set; }

        [JsonProperty("Hora")]
        public string Time { get; set; }

        [JsonProperty("Oficina")]
        public string Office { get; set; }

        [JsonProperty("Descripcion")]
        public string Description { get; set; }

        [JsonProperty("Referencia")]
        public string Reference { get; set; }

        [JsonProperty("Monto")]
        public double Amount { get; set; }

        [JsonProperty("Saldo")]
        public double Balance { get; set; }

        [JsonProperty("TipoMovimiento")]
        public string MovementType { get; set; }

        [JsonProperty("Moneda")]
        public string Currency { get; set; }
    }
}
