﻿using CarlitosBnb.SeedWorks;
using Core.Connectors;
using Core.Connectors.Core;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.Survey
{
    public class SurveyDialogContainer : DialogContainer
    {
        public const string Id = "Survey";

        public const string INPUT_DOCUMENT_STEP = "INPUT_DOCUMENT_STEP";

        public struct Texts
        {

            public const string FAILED_REST_API = "Disculpa, ocurrió un problema al solicitar la encuesta";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";
        }

        public static SurveyDialogContainer Instance { get; } = new SurveyDialogContainer();

        public SurveyDialogContainer() : base(Id)
        {
            var waterfallSteps = new List<WaterfallStep>()
            {
                InitialStep,
            };

            this.Dialogs.Add(Id, waterfallSteps.ToArray()
            );
        }

        private async Task InitialStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            //var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            //var url = configuration.GetValue<string>("Services:SurveyRequest");
            //var response = await Connection.Process(new ConnectorRestPost<SurveyApiRequest, BaseApiResponse<string>>(HttpClientFactory.Create(), url, new SurveyApiRequest()
            //{
            //    Channel = "CarlitosBnb",
            //    Identifier = "CarlitosBnb",
            //    UserChannelId = dc.Context.Activity.From.Id,
            //    SurveyId = configuration.GetValue<long>("SurveyId"),
            //    Product = conversationInfo.CurrentIntent
            //}));

            //if (!response.IsOk)
            //{
            //    await dc.Context.SendActivity(Texts.FAILED_REST_API);
            //    await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
            //    await dc.End();
            //    return;
            //}

            //var responseRestApi = response as Response<BaseApiResponse<string>>;

            //if (!responseRestApi.Body.Success)
            //{
            //    await dc.Context.SendActivity(conversationInfo.DialogflowTextResponse);
            //    //await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
            //    await dc.End();
            //    return;
            //}

            //await dc.Context.SendActivity("Por favor ayúdame a contestar la siguiente encuesta, para brindarte un mejor servicio");
            //await dc.Context.SendActivity(responseRestApi.Body.Body);
            
            await dc.Context.SendActivity(conversationInfo.DialogflowTextResponse);
            await dc.End();
            conversationInfo.IsCompletedTask = true;
        }
    }
}
