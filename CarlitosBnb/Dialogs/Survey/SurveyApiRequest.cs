﻿namespace CarlitosBnb.Dialogs.Survey
{
    public class SurveyApiRequest
    {
        public long SurveyId { get; set; }

        public string Channel { get; set; }

        public string UserChannelId { get; set; }

        public string Identifier { get; set; }

        public string Product { get; set; }
    }
}
