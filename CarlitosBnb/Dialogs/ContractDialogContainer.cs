﻿namespace CarlitosBnb.Dialogs
{
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Schema;
    using Microsoft.Bot.Builder.Prompts.Choices;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using BusinessLogic.Modules.UserModule;
    using BusinessLogic.Modules.CardModule;
    using CarlitosBnb.Core.Factories;
    using Microsoft.Recognizers.Text;
    using CarlitosBnb.Core;
    using BusinessLogic.Modules;
    using System.Text.RegularExpressions;
    using CarlitosBnb.ApiServices;
    using Microsoft.Bot.Builder;

    public class ContractDialogContainer : DialogContainer
    {
        public const string Id = "Contract";

        public static ContractDialogContainer Instance { get; } = new ContractDialogContainer();

        private const string CONTRACT_CHOICE_PROMPT = "ChoiceOptionsContract";

        private const string CONTRACT_YES = "CONTRACT_YES";
        private const string CONTRACT_NOT = "CONTRACT_NOT";

        private static List<string> SYNONYMS_CONTRACT_YES = new List<string>()
        {
            "Si", "claro", "por supuesto", "de acuerdo", "si acepto", "acepto", "si"
        };

        private static List<string> SYNONYMS_CONTRACT_NOT = new List<string>()
        {
            "no", "No"
        };

        private static List<Choice> optionsContractAccepted = new List<Choice>()
        {
            new Choice()
            {
                Action = new CardAction(title: "Sí acepto", type: ActionTypes.ImBack, value: CONTRACT_YES),
                Synonyms = SYNONYMS_CONTRACT_YES,
                Value = CONTRACT_YES
            },
            new Choice()
            {
                Action = new CardAction(title: "No acepto", type: ActionTypes.ImBack, value: CONTRACT_NOT),
                Synonyms = SYNONYMS_CONTRACT_NOT,
                Value = CONTRACT_NOT
            }
        };

        public ContractDialogContainer() : base(Id)
        {
            this.Dialogs.Add(CONTRACT_CHOICE_PROMPT, new ChoicePrompt(culture: Culture.Spanish)
            {
                RecognizerOptions = new FindChoicesOptions()
                {
                    Locale = Culture.Spanish,

                }
            });
            //    , validator: async (context, result) =>
            //{

            //}));

            this.Dialogs.Add(Id, new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    var card = MessageFactory.Attachment(new HeroCard(
                        title: $"¡Hola! {dc.Context.Activity.From.FirstName() }",
                        subtitle: "Soy Carlitos. El chatbot del Banco Nacional de Bolivia.",
                        images: new List<CardImage>() { new CardImage($"http://carlitosbnbmigracion.azurewebsites.net/Content/images/CarlitosBotIcon.png") }
                        ).ToAttachment());

                    await dc.Context.SendActivity(card);

                    await dc.Context.SendActivity("- Antes de Comenzar debes estar de acuerdo con los términos y condiciones que se describen en el pdf que te mando a continuación.");

                    var conditions = MessageFactory.Attachment(new HeroCard(
                        title: "Tërminos y condiciones",
                        buttons: new List<CardAction>()
                        {
                            new CardAction(
                                title: "Ver",
                                type: ActionTypes.OpenUrl,
                                value: "http://carlitosbnbmigracion.azurewebsites.net/Content/pdf/Terminosycondiciones.pdf"
                                )
                        }
                        ).ToAttachment());

                    await dc.Context.SendActivity(conditions);

                    ConversationState<ConversationInfo>.Get(dc.Context).SkipDialogFlow();

                    await dc.Prompt(CONTRACT_CHOICE_PROMPT, "¿Aceptas los términos y condiciones?", new ChoicePromptOptions()
                    {
                        Choices = optionsContractAccepted,
                        RetryPromptActivity = ChoiceFactory.SuggestedAction(optionsContractAccepted, "Por favor seleccione/escriba una de las opciones Si/No") as Activity
                    });
                },
                async(dc, args, next) =>
                {
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var selectedOption = (FoundChoice)args["Value"];
                    if(selectedOption.Value == CONTRACT_YES)
                    {
                        //var result = await ManagerScope.Process(dc.Context, new UserAcceptContractDto{
                        //    ChannelAccountId = dc.Context.Activity.From.Id
                        //});

                        //if(!result.IsOk)
                        //{
                        //    await dc.Context.SendActivity("Perdón ocurrió un problema al intentar aceptar el contrato, intente nuevamente");                            
                        //    await dc.End();
                        //    return;
                        //}
                        var authenticationService = dc.Context.Services.Get<IAuthenticationService>();
                        //var serviceProvider = dc.Context.Services.Get<IServiceProvider>(CarlitosBnbBot.SERVICE_PROVIDER_ID);
                        var getCardService = dc.Context.Services.Get<IService<GetCardDto>>();
                        authenticationService.SetAuthToken(conversationInfo.AuthToken);
                        await authenticationService.AcceptContract();
                        conversationInfo.IsContractAccepted = true;

                        await dc.Context.SendActivity("¡Muy bien! Aceptaste los términos y condiciones");
                        await dc.Context.SendActivity("¡Bienvenido! Estas son las opciones que tengo para ti");

                        //var resultGetCard = await ManagerScope.Process(dc.Context, new GetCardDto(){
                        //    Intent = "b8fe65ab-ebee-44c3-a715-6bce21e7e7c4"
                        //}, serviceProvider);
                        getCardService.Data = new GetCardDto()
                        {
                            Intent = "b8fe65ab-ebee-44c3-a715-6bce21e7e7c4"
                        };

                        var resultGetCard = await getCardService.Process();

                        if(!resultGetCard.IsOk)
                        {
                            await dc.Context.SendActivity("Ups, al parecer no pude encontrar opciones, por favor intenta nuevamente");
                            //conversationInfo.Clear();
                            await dc.End();
                            return;
                        }

                        var card = (resultGetCard as Result<CardInfo>).Body;
                        var creator = new CardCreator();
                        creator.UserName = dc.Context.Activity.From.FirstName();
                        var message = creator.Create(card);
                        await dc.Context.SendActivity(message);
                    }
                    else
                    {
                        await dc.Context.SendActivity("Lo siento, si no estás de acuerdo con los términos y condiciones del servicio no puedes seguir interactuando commigo");
                    }

                    //conversationInfo.Clear();
                    await dc.End();
                }
            });
        }
    }
}
