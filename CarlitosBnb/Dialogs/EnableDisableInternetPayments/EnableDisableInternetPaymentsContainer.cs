﻿//using BusinessLogic.Modules.LogModule;
using CarlitosBnb.Core;
using CarlitosBnb.Dialogs.Common;
using CarlitosBnb.SeedWorks;
using Core.Connectors;
using Core.Connectors.Core;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Prompts;
using Microsoft.Bot.Builder.Prompts.Choices;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Recognizers.Text;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.EnableDisableInternetPayments
{
    public class EnableDisableInternetPaymentsContainer : DialogContainer
    {
        public const string Id = "EnableDisableInternetPayments";

        public struct Inputs
        {
            public const string SELECT_ACCOUNT = "SelectAccount";

            public const string SELECT_FOR_CONTINUE = "selectForContinue";

            public const string CONTRACT_PROMPT = "ContractPrompt";

            public const string MAX_AMOUNT_DEBIT = "MaxAmountDebit";

            public const string DATE_RANGE = "DATE_RANGE";

            public const string DATE_INIT = "DateInit";

            public const string DATE_END = "DateEnd";
        }

        public struct Texts
        {
            public const string FAILED_REST_CARD = "Disculpa, pero no puedo obtener la información de tu tarjeta";

            public const string NOT_FOUND_OWNER_ACCOUNTS = "No encontré tus cuentas, por favor intenta más tarde.";

            public const string NOT_FOUND_BALANCE = "No encontré el saldo de tus cuentas, por favor intenta más tarde";

            public const string CANCEL = "{0}, dime en que más te puedo ayudar";

            public const string FAILED_POLICY = "Disculpa, ocurrió un error al solicitar el seguro de protección, por favor vuelve a intentar";

            public const string FAILED_ENABLE_PAYMENT_INTERNET = "Disculpa, ocurrió un error al solicitar la habilitación de pagos por internet, por favor vuelve a intentar";
        }

        private enum DateRanges
        {
            ONE_DAY,
            THREE_DAYS,
            ONE_WEEK,
            ONE_MONTH
        }

        private Dictionary<DateRanges, string> TextsDate = new Dictionary<DateRanges, string>()
        {
            { DateRanges.ONE_DAY, "1 día" },
            { DateRanges.THREE_DAYS, "3 días" },
            { DateRanges.ONE_WEEK, "7 días" },
            { DateRanges.ONE_MONTH, "30 días" }
        };

        //private Dictionary<string, string> DatesInit = new Dictionary<string, string>()
        //{
        //    { DateRanges.ONE_DAY, DateTime.Now.ToString("dd/MM/yyyy") },
        //    { DateRanges.THREE_DAYS, DateTime },
        //    { DateRanges.ONE_DAY, "" },
        //    { DateRanges.ONE_DAY, "" }
        //};

        private Dictionary<DateRanges, string> DatesEnd = new Dictionary<DateRanges, string>()
        {
            { DateRanges.ONE_DAY, DateTime.Now.AddDays(1).ToString("dd/MM/yyyy") },
            { DateRanges.THREE_DAYS, DateTime.Now.AddDays(3).ToString("dd/MM/yyyy") },
            { DateRanges.ONE_WEEK, DateTime.Now.AddDays(7).ToString("dd/MM/yyyy") },
            { DateRanges.ONE_MONTH, DateTime.Now.AddDays(30).ToString("dd/MM/yyyy") }
        };

        private static List<string> SYNONYMS_CONTRACT_YES = new List<string>()
        {
            "Si", "claro", "por supuesto", "de acuerdo", "si acepto", "acepto", "si"
        };

        private static List<string> SYNONYMS_CONTRACT_NOT = new List<string>()
        {
            "no", "No"
        };

        private const string CONTRACT_YES = "CONTRACT_YES";
        private const string CONTRACT_NOT = "CONTRACT_NOT";

        private static List<Choice> optionsContractAccepted = new List<Choice>()
        {
            new Choice()
            {
                Action = new CardAction(title: "Sí acepto", type: ActionTypes.PostBack, value: CONTRACT_YES),
                Synonyms = SYNONYMS_CONTRACT_YES,
                Value = CONTRACT_YES
            },
            new Choice()
            {
                Action = new CardAction(title: "No acepto", type: ActionTypes.PostBack, value: CONTRACT_NOT),
                Synonyms = SYNONYMS_CONTRACT_NOT,
                Value = CONTRACT_NOT
            }
        };

        public static EnableDisableInternetPaymentsContainer Instance { get; } = new EnableDisableInternetPaymentsContainer();

        public EnableDisableInternetPaymentsContainer() : base(Id)
        {
            var waterFallSteps = new List<WaterfallStep>();
            var waterFallSelectAccountSteps = new List<WaterfallStep>();
            var waterFallEnableInternetPaymentDataSteps = new List<WaterfallStep>();

            waterFallSteps.AddRange(UserProfileDocumentWaterfallSteps.Instance.WaterfallStepsOnlyDocumentNumber);

            waterFallSteps.AddRange(ClientProfileWaterfallSteps.Instance.WaterFalls);
            waterFallSteps.AddRange(SendVerificationOtpWaterfallSteps.Instance.Waterfalls);
            //waterFallSteps.Add(BeginSendVerificationOtp);
            waterFallSteps.Add(VerifySafeProtectionStep);
            waterFallSelectAccountSteps.Add(AcceptTermsAndConditionsStep);
            waterFallSelectAccountSteps.Add(ResponseAcceptTermsAndConditionsStep);
            waterFallSelectAccountSteps.Add(SelectNumberAccountStep);
            waterFallSelectAccountSteps.Add(RequestSafeProtection);

            waterFallEnableInternetPaymentDataSteps.Add(EnableInternetPaymentGetAmountStep);
            waterFallEnableInternetPaymentDataSteps.Add(ResponseEnableInternetPaymentGetAmountStep);
            waterFallEnableInternetPaymentDataSteps.Add(EnableInternetPaymentGetDateRangeStep);
            waterFallEnableInternetPaymentDataSteps.Add(ResponseEnableInternetPaymentGetDateRangeStep);
            waterFallEnableInternetPaymentDataSteps.Add(EnableInternetPaymentStep);

            this.Dialogs.Add(Id, waterFallSteps.ToArray());
            this.Dialogs.Add(SendVerificationOtpWaterfallSteps.Id, SendVerificationOtpWaterfallSteps.Instance.Waterfalls);
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Id, UserProfileDocumentWaterfallSteps.Instance.WaterfallStepsOnlyDocumentNumber);
            this.Dialogs.Add("SelectAccountWaterFallSteps", waterFallSelectAccountSteps.ToArray());
            this.Dialogs.Add("EnableInternetPaymentWaterFallSteps", waterFallEnableInternetPaymentDataSteps.ToArray());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_DOCUMENT, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.SELECT_FOR_CONTINUE, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish));
            this.Dialogs.Add(Inputs.SELECT_ACCOUNT, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish, ValidateSelectAccount));
            this.Dialogs.Add(SendVerificationOtpWaterfallSteps.Inputs.OTP_CODE, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(Inputs.MAX_AMOUNT_DEBIT, new Microsoft.Bot.Builder.Dialogs.TextPrompt(ValidateMaxAmountDebit));
            this.Dialogs.Add(Inputs.DATE_RANGE, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish));
            this.Dialogs.Add(Inputs.CONTRACT_PROMPT, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish));
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_CARD_ID, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
        }

        private async Task ValidateSelectAccount(ITurnContext context, ChoiceResult toValidate)
        {
            var conversationInfo = context.GetConversationState<ConversationInfo>();
            if (!conversationInfo.ProfileSession.Clients[0].Accounts.Any(a => a.Number == toValidate.Value.Value))
            {
                toValidate.Status = null;
                await context.SendActivity("Debes seleccionar o escribir una de sus cuentas personales, por favor intente nuevamente");
            }
        }

        private async Task ValidateMaxAmountDebit(ITurnContext context, TextResult toValidate)
        {
            var amount = context.Activity.Text;
            var amountDecimal = 0M;
            if (!decimal.TryParse(amount, out amountDecimal))
            {
                toValidate.Status = null;
                await context.SendActivity("El monto máximo no tiene el formato correcto ej: 123.45, por favor ingresa un valor válido");
            }
        }

        private async Task VerifySafeProtectionStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var response = await Connection.Process(new ConnectorRestPost<GetCardsByClientDto, BaseApiResponse<List<CardResult>>>(HttpClientFactory.Create(), configuration.GetValue<string>("Services:GetCardsByClient"), new GetCardsByClientDto()
            {
                CodeClient = long.Parse(conversationInfo.ProfileSession.Clients[0].Number)
            }));

            if (!response.IsOk)
            {
                await dc.Context.SendActivity(Texts.FAILED_REST_CARD);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
                return;
            }

            var responseGetCards = response as Response<BaseApiResponse<List<CardResult>>>;
            conversationInfo.EnableInternetPaymentData.Card = responseGetCards.Body.Body[0];
            if (!responseGetCards.Body.Body[0].HasSafeProtection)
            {
                await dc.Begin("SelectAccountWaterFallSteps");
            }
            else
            {
                await dc.Begin("EnableInternetPaymentWaterFallSteps");
            }

            /*Habilitar*/
            //await dc.Context.SendActivity("La tarjeta fue habilitada correctamente");
        }

        private async Task AcceptTermsAndConditionsStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            await dc.Context.SendActivity("Antes de continuar debes estar de acuerdo con los términos y condiciones que se describen en el pdf que te mando a continuación.");

            var conditions = MessageFactory.Attachment(new HeroCard(
                        title: "Tërminos y condiciones",
                        buttons: new List<CardAction>()
                        {
                            new CardAction(
                                title: "Ver",
                                type: ActionTypes.OpenUrl,
                                value: "http://carlitosbnbmigracion.azurewebsites.net/Content/documents/terminos_seguro_proteccion.pdf"
                                )
                        }
                        ).ToAttachment());

            await dc.Context.SendActivity(conditions);
            await dc.Prompt(Inputs.CONTRACT_PROMPT, "¿Aceptas los términos y condiciones?", new ChoicePromptOptions()
            {
                Choices = optionsContractAccepted,
                RetryPromptActivity = ChoiceFactory.SuggestedAction(optionsContractAccepted, "Por favor selecciona/escribe una de las opciones Si/No") as Activity
            });
        }

        private async Task ResponseAcceptTermsAndConditionsStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var selectedOption = (FoundChoice)args["Value"];
            if (selectedOption.Value != CONTRACT_YES)
            {
                await dc.Context.SendActivity("Lo siento, si no estás de acuerdo con los términos y condiciones del servicio no puedes solicitar el seguro de protección");
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
                return;
            }

            await next();
        }

        private async Task SelectNumberAccountStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();

            var clients = conversationInfo.ProfileSession.Clients;

            var options = new List<Choice>();

            foreach (var client in clients)
            {
                foreach (var account in client.Accounts)
                {
                    options.Add(new Choice()
                    {
                        Action = new CardAction(title: account.NumberObfuscate, type: ActionTypes.PostBack, value: account.Number),
                        Value = account.Number
                    });
                }
            }

            await dc.Prompt(Inputs.SELECT_ACCOUNT, "Selecciona una cuenta para realizar el débito del seguro de protección", new ChoicePromptOptions()
            {
                Choices = options,
                RetryPromptString = "Debes seleccionar o escribir una de sus cuentas personales, por favor intenta nuevamente"
            });
        }

        private async Task RequestSafeProtection(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var choice = (FoundChoice)args["Value"];
            conversationInfo.EnableInternetPaymentData.DebitAccount = new string(choice.Value.ToString().Where(c => char.IsDigit(c)).ToArray());
            //await dc.Context.SendActivity($"DebitAccount: {conversationInfo.EnableInternetPaymentData.DebitAccount}");
            //await dc.Context.SendActivity(NumberAccount.ToString());

            Response response;
            if (configuration.GetValue<bool>("Dev:UseHotCodeEnableInternetPayment"))
            {
                response = Response.Ok(new BaseApiResponse<CompleteRequestSafeProtectionResult>()
                {
                    Success = true,
                    Body = new CompleteRequestSafeProtectionResult()
                    {
                        IsOk = true,
                        PolicyUrl = "http://bnb.abrenet.com/api/th/1599833958/certificate"
                    }
                });
            }
            else
            {
                response = await Connection.Process(new ConnectorRestPost<CompleteRequestSafeProtectionDto, BaseApiResponse<CompleteRequestSafeProtectionResult>>(HttpClientFactory.Create(), configuration.GetValue<string>("Services:CompleteRequestSafeProtection"), new CompleteRequestSafeProtectionDto()
                {
                    CardNumber = 111111,
                    CodeClient = long.Parse(conversationInfo.ProfileSession.Clients[0].Number),
                    NumberAccount = long.Parse(conversationInfo.EnableInternetPaymentData.DebitAccount),
                    Document = long.Parse(conversationInfo.ProfileSession.DocumentNumber)
                }));
            }

            if (!response.IsOk)
            {
                //await ManagerScope.Process(dc.Context, LogInfo.Load(dc.Context.Activity.From.Id, response.Message));

                await dc.Context.SendActivity(Texts.FAILED_REST_CARD);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
                return;
            }

            var responseRequestSafeProtection = response as Response<BaseApiResponse<CompleteRequestSafeProtectionResult>>;
            if (!responseRequestSafeProtection.Body.Success || !responseRequestSafeProtection.Body.Body.IsOk)
            {
                //await ManagerScope.Process(dc.Context, LogInfo.Load(dc.Context.Activity.From.Id, $"{responseRequestSafeProtection?.Message} || {responseRequestSafeProtection?.Body?.Message} || {responseRequestSafeProtection?.Body?.Body?.Message}"));

                await dc.Context.SendActivity(Texts.FAILED_POLICY);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
                return;
            }

            await dc.Context.SendActivity("Puedes descargar la poliza del seguro en el siguiente botón");
            var contractAttachment = new Attachment();
            contractAttachment.ContentType = "application/pdf";
            contractAttachment.ContentUrl = responseRequestSafeProtection.Body.Body.PolicyUrl;
            var contractMessage = MessageFactory.Attachment(contractAttachment);
            await dc.Context.SendActivity(contractMessage);
            await dc.Begin("EnableInternetPaymentWaterFallSteps");
            //await dc.End();
        }

        private async Task EnableInternetPaymentGetAmountStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            await dc.Prompt(Inputs.MAX_AMOUNT_DEBIT, "Por favor ingresa el monto máximo (en dólares) para tus compras por internet");
        }

        private async Task ResponseEnableInternetPaymentGetAmountStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            conversationInfo.EnableInternetPaymentData.Amount = decimal.Parse(args["Value"].ToString());
            await next();
        }

        private async Task EnableInternetPaymentGetDateRangeStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var options = new List<Choice>();
            foreach (DateRanges dateRange in Enum.GetValues(typeof(DateRanges)))
            {
                if (TextsDate.ContainsKey(dateRange) && DatesEnd.ContainsKey(dateRange))
                {
                    options.Add(new Choice()
                    {
                        Action = new CardAction(title: TextsDate[dateRange], type: ActionTypes.PostBack, value: DatesEnd[dateRange]),
                        Value = DatesEnd[dateRange]
                    });
                }
            }

            await dc.Prompt(Inputs.DATE_RANGE, "Por favor selecciona el tiempo que deseas activar los pagos por internet", new ChoicePromptOptions()
            {
                Choices = options,
                RetryPromptString = "Debes seleccionar una de las opciones"
            });
        }

        private async Task ResponseEnableInternetPaymentGetDateRangeStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var dateRangeSelected = (FoundChoice)args["Value"];
            conversationInfo.EnableInternetPaymentData.DateInitial = DateTime.Now.ToString("dd/MM/yyyy");
            conversationInfo.EnableInternetPaymentData.DateEnd = dateRangeSelected.Value;
            await next();
        }

        //private async Task EnableInternetPaymentGetDateEndStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        //{
        //    await dc.Prompt(Inputs.DATE_END, "Por favor ingresa la fecha de fin para tus compras por internet ej: 14/09/2020");
        //}

        //private async Task ResponseEnableInternetPaymentGetDateEndStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        //{
        //    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
        //    conversationInfo.EnableInternetPaymentData.DateEnd = args["Value"].ToString();
        //    await next();
        //}

        private async Task EnableInternetPaymentStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);

            Response response;
            if (configuration.GetValue<bool>("Dev:UseHotCodeEnableInternetPayment"))
            {
                response = Response.Ok(new BaseApiResponse<CompleteEnableDisableInternetPaymentResult>()
                {
                    Success = true,
                    Body = new CompleteEnableDisableInternetPaymentResult()
                    {
                        IsOk = true
                    }
                });
            }
            else
            {
                response = await Connection.Process(new ConnectorRestPost<CompleteEnableDisableInternetPaymentDto, BaseApiResponse<CompleteEnableDisableInternetPaymentResult>>(HttpClientFactory.Create(), configuration.GetValue<string>("Services:CompleteEnableInternetPayment"), new CompleteEnableDisableInternetPaymentDto()
                {
                    CodeClient = long.Parse(conversationInfo.ProfileSession.Clients[0].Number),
                    //CardNumber = long.Parse(conversationInfo.EnableInternetPaymentData.Card.Number),
                    CardNumber = 111111,
                    MaxAmount = conversationInfo.EnableInternetPaymentData.Amount,
                    DateInitial = conversationInfo.EnableInternetPaymentData.DateInitial,
                    DateEnd = conversationInfo.EnableInternetPaymentData.DateEnd
                }));
            }

            if (!response.IsOk)
            {
                //await ManagerScope.Process(dc.Context, LogInfo.Load(dc.Context.Activity.From.Id, response.Message));

                await dc.Context.SendActivity(Texts.FAILED_REST_CARD);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
                return;
            }

            var responseRequestSafeProtection = response as Response<BaseApiResponse<CompleteEnableDisableInternetPaymentResult>>;
            if (!responseRequestSafeProtection.Body.Success || !responseRequestSafeProtection.Body.Body.IsOk)
            {
                //await ManagerScope.Process(dc.Context, LogInfo.Load(dc.Context.Activity.From.Id, $"{responseRequestSafeProtection?.Message} || {responseRequestSafeProtection?.Body?.Message} || {responseRequestSafeProtection?.Body?.Body?.Message}"));
                await dc.Context.SendActivity(Texts.FAILED_ENABLE_PAYMENT_INTERNET);
                await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
                await dc.End();
                return;
            }

            await dc.Context.SendActivity("Tu Tarjeta fue habilitada correctamente para realizar compras por internet");
            await dc.Context.SendActivity(string.Format(Texts.CANCEL, dc.Context.Activity.From.FirstName()));
            await dc.End();
            conversationInfo.IsCompletedTask = true;
        }

        private async Task TestStep(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            if (string.IsNullOrEmpty(conversationInfo.ProfileSession.DocumentNumber))
            {
                await dc.Context.SendActivity("El documento esta vacío");
                await dc.End();
                return;
            }

            await dc.Context.SendActivity(JsonConvert.SerializeObject(conversationInfo.ProfileSession));
            await dc.End();
        }
    }

    public class GetCardsByClientDto
    {
        public long CodeClient { get; set; }
    }

    public class GetCardsByClientResult
    {

    }

    public class CardResult
    {
        //[JsonProperty("Numero")]
        public string Number { get; set; }

        //[JsonProperty("NumeroOfuscado")]
        public string NumberObfuscated { get; set; }

        //[JsonProperty("EstaActiva")]
        public bool IsActive { get; set; }

        //[JsonProperty("TieneSeguroProteccion")]
        public bool HasSafeProtection { get; set; }
    }

    public class CompleteRequestSafeProtectionDto
    {
        public long CardNumber { get; set; }

        public long CodeClient { get; set; }

        public long Document { get; set; }

        public long NumberAccount { get; set; }
    }

    public class CompleteRequestSafeProtectionResult
    {
        public bool IsOk { get; set; }

        public string Message { get; set; }

        public string PolicyUrl { get; set; }

        public string Policy { get; set; }
    }

    public class CompleteEnableDisableInternetPaymentDto
    {
        public long CardNumber { get; set; }

        public decimal MaxAmount { get; set; }

        public bool Enable { get; set; }

        public string DateInitial { get; set; }

        public string DateEnd { get; set; }

        public string User { get; set; }

        public string Password { get; set; }

        public long CodeUser { get; set; }

        public long CodeClient { get; set; }

        public string Origin { get; set; }
    }

    public class CompleteEnableDisableInternetPaymentResult
    {
        public bool IsOk { get; set; }

        public string Message { get; set; }
    }
}
