﻿namespace CarlitosBnb.Dialogs.AgenciesAndAtm.Agencies
{
    using BusinessLogic.Modules.AgencyModule;
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;

    public class AgencyDialogContainer : DialogContainer
    {
        public const string Id = "Agency";

        public static AgencyDialogContainer Instance { get; } = new AgencyDialogContainer();

        private struct Texts
        {
            public const string NOT_FOUND_COORDINATES = "No encontré coordenadas, por favor intenta nuevamente";
        }

        public AgencyDialogContainer() : base(Id)
        {
            this.Dialogs.Add(Id, new WaterfallStep[]
            {
                RequestGetLocationWaterfallStep.Instance.Waterfall,
                async(dc, args, next) =>
                {
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
                    var message = dc.Context.Activity.AsMessageActivity();
                    var place = message?.Entities?.Where(t => t.Type == "Place").Select(t => t.GetAs<Place>()).FirstOrDefault();

                    if(place == null)
                    {
                        await dc.Context.SendActivity(Texts.NOT_FOUND_COORDINATES);
                        //conversationInfo.Clear();
                        await dc.End();
                        return;
                    }

                    var geoLocation = (place.Geo as JObject)?.ToObject<GeoCoordinates>();

                    if(geoLocation == null)
                    {
                        await dc.Context.SendActivity(Texts.NOT_FOUND_COORDINATES);
                        //conversationInfo.Clear();
                        await dc.End();
                        return;
                    }

                    //var serviceProvider = dc.Context.Services.Get<IServiceProvider>(CarlitosBnbBot.SERVICE_PROVIDER_ID);

                    //var result = await ManagerScope.Process(dc.Context, new AgencyDto()
                    //{
                    //    Latitude = geoLocation.Latitude.ToString().Replace(',', '.'),
                    //    Longitude = geoLocation.Longitude.ToString().Replace(',', '.')
                    //});

                    var result = await Connection.Process(new ConnectorRestPost<LocationRequest, LocationAgencyResponse>(
                        HttpClientFactory.Create(),
                        configuration.GetValue<string>("Services:GetAgenciesNearbyLocation"), new LocationRequest(){
                         Latitude = geoLocation.Latitude.HasValue ? geoLocation.Latitude.Value: 0,
                         Longitude = geoLocation.Longitude.HasValue ? geoLocation.Longitude.Value: 0
                    }));

                    if(!result.IsOk)
                    {
                        await dc.Context.SendActivity("Ups, no encontré ninguna agencia a tu alrededor, intentelo desde otra ubicación por favor.");
                        //conversationInfo.Clear();
                        await dc.End();
                        return;
                    }

                    var agencies = result as Response<LocationAgencyResponse>;

                    //var agencyService = serviceProvider.GetService<IAgencyService>();
                    //var agencies = await agencyService.GetAgenciesNearby(new AgencyDto()
                    //{
                    //    Latitude = geoLocation.Latitude.ToString().Replace(',', '.'),
                    //    Longitude = geoLocation.Longitude.ToString().Replace(',', '.')
                    //});

                    //agencyService.Dispose();

                    if(agencies.Body.Agencies.Count <= 0)
                    {
                        await dc.Context.SendActivity("Ups, no encontré ninguna agencia a tu alrededor, intentelo desde otra ubicación por favor.");
                        //await dc.Context.SendActivity("O busca en la lista de todas nuestras agencias :)");
                        //conversationInfo.Clear();
                        await dc.End();
                        return;
                    }

                    var attachments = new List<Attachment>();
                    foreach(var agency in agencies.Body.Agencies)
                    {
                        attachments.Add(new HeroCard(){
                            Title = agency.Description,
                            Subtitle = $"Horario: { agency.AttentionSchedule }",
                            Images = new List<CardImage>()
                            {
                                new CardImage()
                                {
                                    Url = agency.ImageUrl
                                }
                            },
                            Buttons = new List<CardAction>()
                            {
                                new CardAction()
                                {
                                    Title = "Ver mapa",
                                    Type = ActionTypes.OpenUrl,
                                    Value = $"http://www.google.com/maps/place/{agency.Latitude},{agency.Longitude}"
                                }
                            }
                        }.ToAttachment());
                    }

                    await dc.Context.SendActivity(MessageFactory.Carousel(attachments));
                    await dc.End();
                    conversationInfo.IsCompletedTask = true;
                }
            });
        }

        //private DbGeography
    }

    //public sealed class FacebookMessage
    //{
    //    //public FacebookMessage(string text, IList<FacebookQuickReply> quickReplies = null);

    //    [JsonProperty(PropertyName = "quick_replies")]
    //    public IList<FacebookQuickReply> QuickReplies { get; set; }
    //    [JsonProperty(PropertyName = "text")]
    //    public string Text { get; set; }
    //}

    //public sealed class FacebookQuickReply
    //{
    //    //public FacebookQuickReply(string contentType, string title, string payload, string image = null);

    //    [JsonProperty(PropertyName = "content_type")]
    //    public string ContentType { get; set; }
    //    [JsonProperty(PropertyName = "image_url")]
    //    public string Image { get; set; }
    //    [JsonProperty(PropertyName = "payload")]
    //    public string Payload { get; set; }
    //    [JsonProperty(PropertyName = "title")]
    //    public string Title { get; set; }

    //    public sealed class ContentTypes
    //    {
    //        public const string Location = "location";
    //        public const string Text = "text";
    //    }
    //}
}
