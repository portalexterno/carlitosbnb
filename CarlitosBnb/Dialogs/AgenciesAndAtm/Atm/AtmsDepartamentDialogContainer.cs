﻿namespace CarlitosBnb.Dialogs.AgenciesAndAtm.Atm
{
    using BusinessLogic.Modules.AgencyModule;
    using CarlitosBnb.Middlewares.DialogFlow;
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Google.Cloud.Dialogflow.V2;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using System.Collections.Generic;
    using System.Net.Http;

    public class AtmsDepartamentDialogContainer : DialogContainer
    {
        public const string Id = "AtmsByDepartament";

        private const string DIALOGFLOW_DEPARTAMENT_ID = "Departamentos";

        public static AtmsDepartamentDialogContainer Instance { get; } = new AtmsDepartamentDialogContainer();

        public AtmsDepartamentDialogContainer() : base(Id)
        {
            this.Dialogs.Add(Id, new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
                    var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
                    var queryResult = dc.Context.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);
                    var departament = queryResult.Parameters.Fields[DIALOGFLOW_DEPARTAMENT_ID].StringValue;

                    //var serviceProvider = dc.Context.Services.Get<IServiceProvider>(CarlitosBnbBot.SERVICE_PROVIDER_ID);

                    //var result = await ManagerScope.Process(dc.Context, new AtmByDepartamentDto()
                    //{
                    //    Departament = departament
                    //});

                    var result = await Connection.Process(new ConnectorRestPost<LocationByDepartamentRequest, LocationAtmResponse>(
                        HttpClientFactory.Create(),
                        configuration.GetValue<string>("Services:GetAtmsByDepartament"), new LocationByDepartamentRequest(){
                         DepartmentId = int.Parse(departament)
                    }));

                    if(!result.IsOk)
                    {
                        await dc.Context.SendActivity("Ups, no encontré ninguna agencia a tu alrededor, intentelo desde otra ubicación por favor.");
                        //conversationInfo.Clear();
                        await dc.End();
                        return;
                    }

                    var agencies = result as Response<LocationAtmResponse>;

                    //var agencyService = serviceProvider.GetService<IAgencyService>();

                    //var agencies = await agencyService.GetAtmsByDepartament(new AgenciesByDepartamentDto()
                    //{
                    //    Departament = departament
                    //});

                    //agencyService.Dispose();

                    if(agencies.Body.Atms.Count <= 0)
                    {
                        await dc.Context.SendActivity("Ups, no encontré ningun atm a tu alrededor, intentelo desde otra ubicación por favor.");
                        //await dc.Context.SendActivity("O busca en la lista de todas nuestras agencias :)");
                        //conversationInfo.Clear();
                        await dc.End();
                        return;
                    }

                    var attachments = new List<Attachment>();
                    foreach(var agency in agencies.Body.Atms)
                    {
                        attachments.Add(new HeroCard(){
                            Title = agency.Description,
                            Subtitle = $"Horario: { agency.AttentionSchedule }",
                            Images = new List<CardImage>()
                            {
                                new CardImage()
                                {
                                    Url = agency.ImageUrl
                                }
                            },
                            Buttons = new List<CardAction>()
                            {
                                new CardAction()
                                {
                                    Title = "Ver mapa",
                                    Type = ActionTypes.OpenUrl,
                                    Value = $"http://www.google.com/maps/place/{agency.Latitude},{agency.Longitude}"
                                }
                            }
                        }.ToAttachment());
                    }

                    await dc.Context.SendActivity(MessageFactory.Carousel(attachments));
                    await dc.End();

                    conversationInfo.IsCompletedTask = true;
                }
            });
        }
    }
}
