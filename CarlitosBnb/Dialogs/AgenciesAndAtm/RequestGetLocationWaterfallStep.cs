﻿namespace CarlitosBnb.Dialogs.AgenciesAndAtm
{
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class RequestGetLocationWaterfallStep
    {
        private const string channelId = "facebook";

        public WaterfallStep Waterfall => SendRequest;

        public static RequestGetLocationWaterfallStep Instance { get; } = new RequestGetLocationWaterfallStep();

        private struct Texts
        {
            public const string SHARE_LOCATION = "Comparte tu ubicación por favor.";

            public const string NO_CHANNEL_FACEBOOK = "Lo siento esta función sólo está disponible en facebook messenger";
        }

        private RequestGetLocationWaterfallStep()
        {

        }

        private async Task SendRequest(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            if (dc.Context.Activity.ChannelId != channelId)
            {
                await dc.Context.SendActivity(Texts.NO_CHANNEL_FACEBOOK);
                //conversationInfo.Clear();
                await dc.End();
                return;
            }

            //var message = Activity.CreateMessageActivity();
            //message.ChannelData = new FacebookMessage(
            //    text: Texts.SHARE_LOCATION,
            //    quickReplies: new List<FacebookQuickReply>
            //    {
            //                new FacebookQuickReply(
            //                    contentType: FacebookQuickReply.ContentTypes.Location,
            //                    title: default(string),
            //                    payload: default(string)
            //                    )
            //    }
            //    );

            dc.Context.GetConversationState<ConversationInfo>().SkipDialogFlow();
            await dc.Context.SendActivity("");
        }
    }
}
