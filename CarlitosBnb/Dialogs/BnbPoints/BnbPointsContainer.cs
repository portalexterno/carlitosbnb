﻿using BusinessLogic.Modules;
using CarlitosBnb.Core.Factories;
using CarlitosBnb.Dialogs.Common;
using CarlitosBnb.SeedWorks;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Recognizers.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.Dialogs.BnbPoints
{
    public class BnbPointsContainer : DialogContainer
    {
        public const string Id = "BnbPoints";
        public const int DOCUMENT_TYPE_CI = 1;
        public const int CHANNEL_CARLITOS_BNB = 6;

        public static BnbPointsContainer Instance { get; } = new BnbPointsContainer();

        public BnbPointsContainer() : base(Id)
        {
            var waterFallSteps = new List<WaterfallStep>();
            waterFallSteps.AddRange(UserProfileDocumentWaterfallSteps.Instance.WaterfallStepsFull);
            //waterFallSteps.AddRange(ClientProfileWaterfallSteps.Instance.WaterFalls);
            //waterFallSteps.AddRange(SendVerificationOtpWaterfallSteps.Instance.Waterfalls);
            waterFallSteps.Add(this.GetTotalBnbPoints);
            this.Dialogs.Add(Id, waterFallSteps.ToArray());
            this.Dialogs.Add(SendVerificationOtpWaterfallSteps.Id, SendVerificationOtpWaterfallSteps.Instance.Waterfalls);
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Id, UserProfileDocumentWaterfallSteps.Instance.WaterfallStepsOnlyDocumentNumber);
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_DOCUMENT, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.SELECT_FOR_CONTINUE, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish));
            this.Dialogs.Add(SendVerificationOtpWaterfallSteps.Inputs.OTP_CODE, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_DOCUMENT_EXTENTION, new Microsoft.Bot.Builder.Dialogs.ChoicePrompt(Culture.Spanish));
            this.Dialogs.Add(UserProfileDocumentWaterfallSteps.Inputs.INPUT_CARD_ID, new Microsoft.Bot.Builder.Dialogs.TextPrompt());
        }

        private async Task GetTotalBnbPoints(DialogContext dc, IDictionary<string, object> args, SkipStepFunction next)
        {
            var conversationInfo = dc.Context.GetConversationState<ConversationInfo>();
            var configuration = dc.Context.Services.Get<IConfiguration>(CarlitosBnbBot.CONFIGURATION_ID);
            var httpClient = HttpClientFactory.Create();
            
            var urlApiCustomerData = configuration.GetValue<string>("Services:BnbPointsCustomerData");

            var responseApiCustomerData = await httpClient.Post<CustomerDataDto, BaseApiResponse<CustomerDataResponse>>(urlApiCustomerData, new CustomerDataDto()
            {
                DocumentNumber = conversationInfo.ProfileSession.DocumentNumberLong,
                DocumentExtention = conversationInfo.ProfileSession.DocumentExtension,
                DocumentType = DOCUMENT_TYPE_CI
            });

            if (!responseApiCustomerData.IsOk)
            {
                await dc.Context.SendActivity("Lo siento no pude obtener tus datos, por favor vuelve a intentar");
                await dc.Context.SendActivity(string.Format("{0}, dime en que más te puedo ayudar", dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            var responseCustomerData = responseApiCustomerData as Result<BaseApiResponse<CustomerDataResponse>>;
            if (responseCustomerData == null || !responseCustomerData.Body.Success)
            {
                await dc.Context.SendActivity("Lo siento no pude obtener tus datos, por favor vuelve a intentar.");
                await dc.Context.SendActivity(string.Format("{0}, dime en que más te puedo ayudar", dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            var urlApiBalancePoint = configuration.GetValue<string>("Services:BnbPointsBalancePoint");
            var responseApiBalancePoint = await httpClient.Post<BalancePointDto, BaseApiResponse<BalancePointResponse>>(urlApiBalancePoint, new BalancePointDto()
            {
                LoyaltyId = responseCustomerData.Body.Body.Data[0].AtcAccount,
                ChannelId = CHANNEL_CARLITOS_BNB
            });

            if (!responseApiBalancePoint.IsOk)
            {
                await dc.Context.SendActivity("Lo siento no pude obtener tus Puntos Bnb, por favor vuelve a intentar");
                await dc.Context.SendActivity(string.Format("{0}, dime en que más te puedo ayudar", dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            var responseBalancePoint = responseApiBalancePoint as Result<BaseApiResponse<BalancePointResponse>>;
            if (responseBalancePoint == null || !responseBalancePoint.Body.Success)
            {
                await dc.Context.SendActivity("Lo siento no pude obtener tus Puntos Bnb, por favor vuelve a intentar");
                await dc.Context.SendActivity(string.Format("{0}, dime en que más te puedo ayudar", dc.Context.Activity.From.FirstName()));
                dc.EndAll();
                return;
            }

            //await dc.Context.SendActivity($"Tus puntos BNB son: {responseBalancePoint.Body.Body.Data.TotalPoint}");

            var messages = new List<IMessageActivity>();

            var creator = new CardCreator();
            creator.UserName = dc.Context.Activity.From.FirstName();
            messages.Add(creator.Create(new BusinessLogic.Modules.CardModule.CardInfo()
            {
                Title = $"Puntos BNB",
                SubTitle = $"Saldo: { responseBalancePoint.Body.Body.Data.TotalPoint.ToString("N", new CultureInfo("en-US")) }",
                Buttons = new List<BusinessLogic.Modules.CardModule.CardActionInfo>()
                                {
                                    new BusinessLogic.Modules.CardModule.CardActionInfo()
                                    {
                                        Title = "Ver Movimientos",
                                        Type = ActionTypes.PostBack,
                                        Value = $"movimientos { responseCustomerData.Body.Body.Data[0].AtcAccount } { 3 }"
                                    }
                                },
                Type = "HERO_CARD"
            }));

            await dc.Context.SendActivities(messages.ToArray());


            await dc.Context.SendActivity($"{dc.Context.Activity.From.FirstName()}, dime en que más te puedo ayudar");
            //dc.EndAll();
            await dc.End();
            conversationInfo.IsCompletedTask = true;
        }

        public class CustomerDataDto
        {
            public CustomerDataDto()
            {
                this.Complement = string.Empty;
            }

            public long DocumentNumber { get; set; }

            public string DocumentExtention { get; set; }

            public string Complement { get; set; }

            public int DocumentType { get; set; }
        }

        public class CustomerDataResponse
        {
            public List<DataResponse> Data { get; set; }

            public class DataResponse
            {
                public long AtcAccount { get; set; }
            }
        }

        public class BalancePointDto
        {
            public long LoyaltyId { get; set; }

            public int ChannelId { get; set; }
        }

        public class BalancePointResponse
        {
            public DataResponse Data { get; set; }

            public class DataResponse
            {
                public decimal TotalPoint { get; set; }
            }
        }
    }
}
