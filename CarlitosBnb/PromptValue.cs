﻿namespace CarlitosBnb
{
    using Newtonsoft.Json;

    public class PromptValue
    {
        private PromptValue(string value, FlagsDialogFlow flagsDialogFlow)
        {
            this.Value = value;
            this.FlagsDialogFlow = flagsDialogFlow;
        }

        public FlagsDialogFlow FlagsDialogFlow { get; set; }

        public string Value { get; set; }

        public static string Create(string value, bool skipDialogFlow = false, bool isNotUnknownIntent = false)
        {
            var promptValue = new PromptValue(value, FlagsDialogFlow.Create(skipDialogFlow, isNotUnknownIntent));
            return JsonConvert.SerializeObject(promptValue);
        }
    }

    public class FlagsDialogFlow
    {
        private FlagsDialogFlow(bool skipDialogFlow, bool isNotUnknownIntent)
        {
            this.SkipDialogFlow = skipDialogFlow;
            this.IsNotUnknownIntent = isNotUnknownIntent;
        }

        public bool SkipDialogFlow { get; set; }

        public bool IsNotUnknownIntent { get; set; }

        public static FlagsDialogFlow Create(bool skipDialogFlow, bool isNotUnknownIntent)
        {
            return new FlagsDialogFlow(skipDialogFlow, isNotUnknownIntent);
        }
    }
}
