﻿using Microsoft.Bot.Schema;

namespace CarlitosBnb.SeedWorks
{
    public class FbActivity : Activity
    {
    }

    public class ChannelData
    {
        public Message Message { get; set; }
    }

    public class Message
    {
        public string Mid { get; set; }

        public int Seq { get; set; }

        public QuickReply QuickReply { get; set; }

        public string Text { get; set; }

        public bool IsEcho { get; set; }
    }

    public class QuickReply
    {
        public string Payload { get; set; }
    }
}
