﻿using BusinessLogic.Modules;
using CarlitosBnb.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CarlitosBnb.SeedWorks
{
    public static class HttpClientEx
    {
        private const string MEDIA_TYPE = "application/json";

        public static async Task<Result> Post<BODY, RESULT>(this HttpClient httpClient, string url, BODY body, Dictionary<string, string> headers = null)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, MEDIA_TYPE);

            if (headers != null)
                foreach (var header in headers)
                {
                    content.Headers.Add(header.Key, header.Value);
                }

            var response = await httpClient.PostAsync(url, content);

            var responseContent = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                var resultObject = JsonConvert.DeserializeObject<RESULT>(responseContent);
                return Result.Ok(resultObject);
            }

            return Result.Fail($"ReasonPhrase: {response.ReasonPhrase} Detail: {responseContent} ");
        }

        public static async Task<Result> Post<BODY>(this HttpClient httpClient, string url, BODY body, Dictionary<string, string> headers = null)
        {
            return await httpClient.Post<BODY, Result>(url, body, headers);
        }

        public static async Task<RESULT> PostV2<RESULT>(this HttpClient httpClient, string url, object body = null, string userMessageError = null, Dictionary<string, string> headers = null)
        {
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, MEDIA_TYPE);

            if (headers != null)
                foreach (var header in headers)
                {
                    content.Headers.Add(header.Key, header.Value);
                }

            HttpResponseMessage response = null;
            try
            {
                response = await httpClient.PostAsync(url, content);
            }
            catch (Exception e)
            {
                throw new ApiServicesException(httpClient.BaseAddress.AbsoluteUri, url, response?.ReasonPhrase, null, body, e, userMessageError);
            }


            var responseContent = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                //throw new AppException($"BaseUrl: {httpClient.BaseAddress} Url: {url} ReasonPhrase: {response.ReasonPhrase} Detail: {responseContent} ");
                throw new ApiServicesException(httpClient.BaseAddress.AbsoluteUri, url, response.ReasonPhrase, responseContent, body, userMessageError);
            }

            RESULT resultObject;
            try
            {
                resultObject = JsonConvert.DeserializeObject<RESULT>(responseContent);
            }
            catch (Exception e)
            {
                //throw new Exception($"BaseUrl: {httpClient.BaseAddress} Url: {url}", e);
                throw new ApiServicesException(httpClient.BaseAddress.AbsoluteUri, url, response.ReasonPhrase, responseContent, body, e, userMessageError);
            }

            return resultObject;
        }
    }
}
