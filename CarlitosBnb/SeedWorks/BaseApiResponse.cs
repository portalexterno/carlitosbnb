﻿namespace CarlitosBnb.SeedWorks
{
    public class BaseApiResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public object Exception { get; set; }
    }

    public class BaseApiResponse<T> : BaseApiResponse
    {
        public T Body { get; set; }
    }
}
