﻿namespace CarlitosBnb
{
    using System.Collections.Generic;

    public interface ISelectorIntent<T>
    {
        T GetAction(string intent);
    }

    public class SelectorIntent<T> : ISelectorIntent<T>
    {
        private Dictionary<string, T> actions { get; set; }

        public T GetAction(string intent)
        {
            return this.actions.GetValueOrDefault(intent);
        }
    }
}
