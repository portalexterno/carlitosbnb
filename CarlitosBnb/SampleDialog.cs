﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarlitosBnb
{
    public class SampleDialog : DialogContainer
    {
        public const string Id = "sample";

        public static SampleDialog Instance { get; } = new SampleDialog();

        private SampleDialog() : base(Id)
        {
            //this.Dialogs.Add(Id, new WaterfallStep[]{
            //    async(dc, args, next) =>
            //    {
            //        await dc.Context.SendActivity("Hola");

            //        this.Dialogs.Add("init", new WaterfallStep[]
            //        {
            //            async(dc1, args1, next1) =>
            //            {
            //                await dc1.Context.SendActivity("Otro hola");
            //            }
            //        });
            //    }
            //});
            this.Dialogs = new DialogSet();
            this.Dialogs.Add(Id, new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    var state = dc.Context.GetConversationState<Dictionary<string, object>>();
                    var conversation = ConversationFlow.SampleFromJson();

                    var waterfallSteps = new WaterfallStep[conversation.Blocks[0].Elements.Count];

                    for (var i = 0; i < conversation.Blocks[0].Elements.Count; i++)
                    {
                        waterfallSteps[i] = async(dc1, args1, next1) =>
                        {
                            var stateI = dc1.Context.GetConversationState<Dictionary<string, object>>();
                            //var conversationI = state["conversation"] as ConversationFlow;
                            var conversationI = ConversationFlow.SampleFromJson();

                            state["index"] = state.ContainsKey("index") ? (int)state["index"] + 1 : 0;
                            var index = (int)state["index"];
                            var element = conversationI.Blocks[0].Elements[index];
                            await dc1.Context.SendActivity((element as TextElement).Text);
                            if(conversation.Blocks[0].Elements.Count - 1 <= index)
                            {
                                state["index"] = -1;
                                var d = state["dialogStack"] as List<DialogInstance>;

                                await dc1.End();
                                await dc.End();
                            }
                        };
                    }

                    Waterfall w;
                    if(this.Dialogs.Find("conversation") == null)
                    {
                        w = this.Dialogs.Add("conversation", waterfallSteps);
                    }
                    else
                    {
                        w = new Waterfall(waterfallSteps);
                    }

                    await dc.Begin("conversation");
                }
            });
        }
    }

    public class MyMiddleware : IMiddleware
    {
        public async Task OnTurn(ITurnContext context, MiddlewareSet.NextDelegate next)
        {
            var state = ConversationState<ConversationFlow>.Get(context);
            var s = context.GetConversationState<ConversationFlow>();

            s = ConversationFlow.Sample();

            state = ConversationFlow.Sample();

            await next();
        }
    }
}