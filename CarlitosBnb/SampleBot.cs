﻿namespace CarlitosBnb
{
    using Microsoft.Bot;
    using Microsoft.Bot.Builder;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Schema;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class SampleBot : IBot
    {
        private DialogSet dialogs;

        public SampleBot()
        {
            this.dialogs = new DialogSet();

            this.dialogs.Add("document", new TextPrompt());
            this.dialogs.Add("init", new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    await dc.Context.SendActivity("Hola");
                    await dc.Continue();
                },
                async(dc, args, next) =>
                {
                    await dc.Prompt("document", "Ingresa tu ci");
                },
                async(dc, args, next) =>
                {
                    var document = args["Text"] as string;
                    await dc.Context.SendActivity($"Tu ci es: {document}");
                    await dc.End();
                }
            });

            //this.dialogs = new DialogSet();
            //var waterfallStep = new WaterfallStep[2];

            //this.dialogs.Add("")
        }

        public async Task OnTurn(ITurnContext turnContext)
        {
            if (turnContext.Activity.Type == ActivityTypes.Message)
            {
                try
                {
                    if (turnContext.Activity.Type == ActivityTypes.Message)
                    {
                        var state = ConversationState<Dictionary<string, object>>.Get(turnContext);
                        var dialogContainer = this.dialogs.CreateContext(turnContext, state);

                        await dialogContainer.Continue();
                        if (!turnContext.Responded)
                        {
                            await dialogContainer.Begin("init");
                        }
                    }
                }
                catch (Exception e)
                {
                    await turnContext.SendActivity($"Exception: {e.Message}");
                }
            }
        }


        private IMessageActivity getCarousel()
        {
            return MessageFactory.Carousel(new List<Attachment>()
            {
                this.getSimpleHeroCard(),
                this.getSimpleHeroCard(),
                this.getHeroCard()
            }, "TEXT_CAROUSEL");
        }

        private Attachment getSimpleHeroCard()
        {
            return new HeroCard("Title", "Subtitle", "Text", new List<CardImage>()
            {
                new CardImage(@"https://picsum.photos/200/300/?random", "Alt")
            }).ToAttachment();
        }

        private Attachment getHeroCard()
        {
            return new HeroCard("Title", "SUbtitle", "Text",
                new List<CardImage>
                {
                    new CardImage(@"https://picsum.photos/200/300/?random", "Alt", new CardAction(type: ActionTypes.ImBack, title: "Image", value: "Tocaste la imagen")),
                    new CardImage(@"https://picsum.photos/200/300/?random", "Alt"),
                    new CardImage(@"https://picsum.photos/200/300/?random", "Alt")
                },
                new List<CardAction>
                {
                    new CardAction(type: ActionTypes.ImBack, title: "Title button", value: "ACTION"),
                    new CardAction(type: ActionTypes.ImBack, title: "Title button 2", value: "ACTION 2"),
                    new CardAction(type: ActionTypes.ImBack, title: "Title button 3", value: "ACTION 3"),
                    new CardAction(type: ActionTypes.ImBack, title: "Title button 2", value: "ACTION 2"),
                    new CardAction(type: ActionTypes.OpenUrl, title: "Title button 3", value: "ACTION 3")
                }
                ).ToAttachment();
        }
    }
}
