﻿using CarlitosBnb.SeedWorks;
using CarlitosBnb.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.ApiServices
{
    public class ClientService : BaseService, IClientService
    {
        private readonly ClientServices options;

        public ClientService(HttpClient httpClient, IOptions<ClientServices> options) : base(httpClient)
        {
            this.options = options.Value;
        }

        public async Task<List<AccountBalanceModel>> Balance()
        {
            var response = await this.httpClient.PostV2<BaseApiResponse<List<AccountBalanceModel>>>(this.options.ClientBalance, new { });
            return response.Body;
        }

        public async Task<List<AccountMovementModel>> Movements(string account, int type)
        {
            var response = await this.httpClient.PostV2<BaseApiResponse<List<AccountMovementModel>>>(this.options.ClientMovements, new { account, type });
            return response.Body;
        }
    }
}