﻿using CarlitosBnb.SeedWorks;
using CarlitosBnb.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.ApiServices
{
    public class AuthenticationService : BaseService, IAuthenticationService
    {
        private readonly AuthenticationServices authenticationServicesSettings;

        public AuthenticationService(HttpClient httpClient, IOptions<AuthenticationServices> options) : base(httpClient)
        {
            this.authenticationServicesSettings = options.Value;
        }

        public async Task<GetTokenResponse> GetToken(string channelAccountId, string conversationId)
        {
            var response = await this.httpClient.PostV2<BaseApiResponse<GetTokenResponse>>(this.authenticationServicesSettings.GetTokenApplication, new
            {
                this.authenticationServicesSettings.ApplicationKey,
                this.authenticationServicesSettings.ApplicationSecret,
                ChannelAccountId = channelAccountId,
                ConversationId = conversationId
            });

            return response.Body;
        }

        public async Task<RequestUserTokenResponse> RequestUserToken(string documentNumber)
        {
            var response = await this.httpClient.PostV2<BaseApiResponse<RequestUserTokenResponse>>(this.authenticationServicesSettings.RequestTokenUser, new { documentNumber });
            return response.Body;
        }

        public async Task AcceptContract()
        {
            await this.httpClient.PostV2<BaseApiResponse>(this.authenticationServicesSettings.AcceptContract, new { });
        }

        public async Task<GetUserTokenResponse> GetUserToken(string document, int otpCode)
        {
            var response = await this.httpClient.PostV2<BaseApiResponse<GetUserTokenResponse>>(this.authenticationServicesSettings.GetTokenUser, new { document, otpCode });
            return response.Body;
        }
    }


}