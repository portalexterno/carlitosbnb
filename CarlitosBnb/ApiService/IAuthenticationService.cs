﻿using CarlitosBnb.SeedWorks;
using System;
using System.Threading.Tasks;

namespace CarlitosBnb.ApiServices
{
    public interface IBaseService
    {
        void SetAuthToken(string authToken);

        void SetAuthUserToken(string authUserToken);
    }

    public interface IAuthenticationService : IBaseService
    {
        Task AcceptContract();
        Task<GetTokenResponse> GetToken(string channelAccountId, string conversationId);
        Task<GetUserTokenResponse> GetUserToken(string document, int otpCode);
        Task<RequestUserTokenResponse> RequestUserToken(string documentNumber);
    }

    public class GetTokenResponse
    {
        public string JwtToken { get; set; }

        public bool IsContractAccepted { get; set; }
    }

    public class RequestUserTokenResponse
    {
        public string Email { get; set; }

        public string Phone { get; set; }
    }

    public class GetUserTokenResponse
    {
        public string Token { get; set; }

        public DateTime ExpirationDate { get; set; }
    }
}