﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarlitosBnb.ApiServices
{
    public interface IClientService : IBaseService
    {
        Task<List<AccountBalanceModel>> Balance();
        Task<List<AccountMovementModel>> Movements(string account, int type);
    }

    public class AccountBalanceModel
    {
        [JsonProperty("type")]
        public int Type { get; set; }

        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("currencyDescription")]
        public string CurrencyDescription { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }

    public class AccountMovementModel
    {
        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("movementType")]
        public string MovementType { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}