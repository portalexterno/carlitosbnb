﻿using System.Net.Http;

namespace CarlitosBnb.ApiServices
{
    public abstract class BaseService : IBaseService
    {
        protected readonly HttpClient httpClient;

        public BaseService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public void SetAuthToken(string authToken)
        {
            this.httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authToken);
        }

        public void SetAuthUserToken(string authUserToken)
        {
            this.httpClient.DefaultRequestHeaders.Add("Auth-user", authUserToken);
        }
    }


}