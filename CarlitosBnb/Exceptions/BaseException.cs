﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarlitosBnb.Exceptions
{
    public abstract class BaseException : Exception
    {
        public BaseException() : base() { }

        public BaseException(string message, string userMessageError) : base(message)
        {
            this.UserMessageError = userMessageError;
        }

        public BaseException(string message, Exception exception, string userMessageError) : base(message, exception)
        {
            this.UserMessageError = userMessageError;
        }

        public string UserMessageError { get; }
    }
}
