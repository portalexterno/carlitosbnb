﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarlitosBnb.Exceptions
{
    public class ApiServicesException : BaseException
    {
        public ApiServicesException(string baseUrl, string url, string reasonPhrase, string contentResponse, object request, string userMessageError = null)
            : base($"BaseUrl: {baseUrl} Url: {url} Reason: {reasonPhrase} Content: {contentResponse} Request: {JsonConvert.SerializeObject(request)}", userMessageError)
        {
            SetRequest(request);
        }

        public ApiServicesException(string baseUrl, string url, string reasonPhrase, string contentResponse, object request, Exception e, string userMessageError = null)
            : base($"BaseUrl: {baseUrl} Url: {url} Reason: {reasonPhrase} Content: {contentResponse} Request: {JsonConvert.SerializeObject(request)}", e, userMessageError)
        {
            BaseUrl = baseUrl;
            Url = url;
            ReasonPhrase = reasonPhrase;
            ContentResponse = contentResponse;
            SetRequest(request);
        }

        public string BaseUrl { get; }
        public string Url { get; }
        public string ReasonPhrase { get; }
        public string ContentResponse { get; }

        public string Request { get; set; }

        private void SetRequest(object request)
        {
            Request = JsonConvert.SerializeObject(request);
        }
    }
}
