﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CarlitosBnb
{
    public class ConversationFlow
    {
        public List<Block> Blocks { get; set; }

        public static ConversationFlow SampleFromJson()
        {
            var json = File.ReadAllText("conversation.json");
            var obj = JsonConvert.DeserializeObject<ConversationFlow>(json);
            return obj;
        }

        public static ConversationFlow Sample()
        {
            return new ConversationFlow()
            {
                Blocks = new List<Block>()
                {
                    new Block()
                    {
                        Elements = new List<TextElement>()
                        {
                            new TextElement()
                            {
                                Id = "1",
                                Text = "Ok! por favor",
                                UseVariables = false,
                                Order = 1
                            },
                            new TextElement()
                            {
                                Id = "2",
                                //Prompt = "Ingresa tu documento de identidad",
                                //Key = "document",
                                Text = "Hola",
                                Order = 2
                            },
                            new TextElement()
                            {
                                Id = "3",
                                Text = "document",
                                UseVariables = true,
                                Order = 3
                            }
                        },
                        Id = "1"
                    }
                }
                //Dialogs = new List<IElement>()
                //{
                //    new TextElement()
                //    {
                //        Id = "",
                //        Text = "Ok! por favor"
                //    },
                //    new PromptElement()
                //    {
                //        Id = "",
                //        Prompt = "Ingresa tu documento de identidad",
                //        KeyValue = "document"
                //    }
                //}
            };
        }
    }

    public class Block
    {
        public string Id { get; set; }

        //[JsonConverter(typeof(InterfaceConverter<IElement, TextElement>))]
        public List<TextElement> Elements { get; set; }
    }

    public class InterfaceConverter<TInterface, TConcrete> : CustomCreationConverter<TInterface>
    where TConcrete : TInterface, new()
    {
        public override TInterface Create(Type objectType)
        {
            return new TConcrete();
        }
    }

    public class TypeElements
    {
        public const string JSON = "JSON";
        public const string TEXT = "TEXT";
        public const string PROMPT = "PROMPT";
    }

    public interface IElement
    {
        string Type { get; }

        int Order { get; }
    }

    public abstract class Element : IElement
    {
        public abstract string Type { get; }

        public int Order { get; set; }
    }

    public class JsonApiElement : Element
    {
        public string Url { get; set; }

        public List<Dictionary<string, string>> Parameters { get; set; }

        public override string Type => TypeElements.JSON;
    }

    public class TextElement : Element
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Text")]
        public string Text { get; set; }

        [JsonProperty("UseVariables")]
        public bool UseVariables { get; set; }

        public override string Type => TypeElements.TEXT;
    }

    public class PromptElement : Element
    {
        public string Id { get; set; }

        public string Prompt { get; set; }

        public string Key { get; set; }

        public override string Type => TypeElements.PROMPT;
    }

    public class FactoryDialogs { }
}
