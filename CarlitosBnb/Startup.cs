﻿namespace CarlitosBnb
{
    using AutoMapper;
    using BusinessLogic.Modules;
    using BusinessLogic.Modules.AgencyModule;
    using BusinessLogic.Modules.ApplicationModule;
    using BusinessLogic.Modules.CardModule;
    using BusinessLogic.Modules.CarouselModule;
    using BusinessLogic.Modules.UnknownIntentModule;
    using BusinessLogic.Modules.UserModule;
    using CarlitosBnb.ApiServices;
    using CarlitosBnb.Exceptions;
    using CarlitosBnb.Middlewares.ContentType;
    using CarlitosBnb.Middlewares.DialogFlow;
    using CarlitosBnb.Middlewares.QueryBI;
    using CarlitosBnb.Middlewares.Welcome;
    using CarlitosBnb.Settings;
    using Database;
    using Database.Core;
    using Database.Core.Repositories;
    using Database.Persistence;
    using Database.Persistence.Repositories;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Bot.Builder.BotFramework;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Integration.AspNet.Core;
    using Microsoft.Bot.Builder.TraceExtensions;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using System;

    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IHostingEnvironment env, ILogger<Startup> logger)
        {
            _logger = logger;

            this.env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public const string AUTHENTICATION_SERVICES = nameof(AUTHENTICATION_SERVICES);
        public const string CLIENT_SERVICES = nameof(CLIENT_SERVICES);

        public IConfiguration Configuration { get; }
        private readonly ILogger<Startup> _logger;
        private readonly IHostingEnvironment env;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CarlitosBnbContext>(o =>
            {
                o.UseSqlServer(this.Configuration.GetConnectionString("CarlitosBnb"));
            });

            var sectionAuthenticationServices = Configuration.GetSection("Services:BnbChatBotServices:AuthenticationServices");
            var authenticationServices = new AuthenticationServices();
            sectionAuthenticationServices.Bind(authenticationServices);
            services.Configure<AuthenticationServices>(sectionAuthenticationServices);

            var sectionClientServices = Configuration.GetSection("Services:BnbChatBotServices:ClientServices");
            var clientServices = new ClientServices();
            sectionClientServices.Bind(clientServices);
            services.Configure<ClientServices>(sectionClientServices);

            services.AddHttpClient("AuthenticationServices", c =>
            {
                c.BaseAddress = new Uri(new Uri(authenticationServices.BaseUrl), authenticationServices.BaseApp);
            });

            services.AddHttpClient("ClientServices", c =>
            {
                c.BaseAddress = new Uri(new Uri(clientServices.BaseUrl), clientServices.BaseApp);
            });

            services.AddHttpClient<IAuthenticationService, AuthenticationService>(c =>
            {
                c.BaseAddress = new Uri(new Uri(authenticationServices.BaseUrl), authenticationServices.BaseApp);

            });

            services.AddHttpClient<IClientService, ClientService>(c =>
            {
                c.BaseAddress = new Uri(new Uri(clientServices.BaseUrl), clientServices.BaseApp);
            });


            services.AddScoped<IUnitOfWork, UnitOfWork>();
            //services.AddTransient<IService<LogInfo>, LogErrorService>();
            services.AddTransient<IService<UnknownIntentInfo>, RegisterUnknownIntentService>();
            services.AddTransient<IService<GetUserByChannelIdDto>, GetUserByChannelIdService>();
            services.AddTransient<IService<GetCarouseldDto>, GetCarouselByIntentService>();
            services.AddTransient<IService<AgenciesByDepartamentDto>, GetAgenciesByDepartamentService>();
            services.AddTransient<IService<AgencyDto>, GetAgenciesNearbyService>();
            services.AddTransient<IService<AtmDto>, GetAtmsNearbyService>();
            services.AddTransient<IService<AtmByDepartamentDto>, GetAtmsByDepartamentService>();
            services.AddTransient<IService<UserInfo>, SaveUserService>();
            services.AddTransient<IService<UserAcceptContractDto>, UserAcceptContractService>();
            services.AddTransient<IService<GetCarouseldDto>, GetCarouselByIntentService>();
            services.AddTransient<IService<GetCardDto>, GetCardByIntentService>();
            services.AddTransient<IService<GetApplicationByNameDto>, GetApplicationByNameService>();
            services.AddTransient<IService<UpdateConversationIdDto>, UpdateConversationIdService>();

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ICardRepository, CardRepository>();
            services.AddTransient<ICarouselRepository, CarouselRepository>();
            services.AddTransient<IAgencyRepository, AgencyRepository>();
            services.AddTransient<ILogRepository, LogRepository>();
            services.AddTransient<IUnknownIntentionRepository, UnknownIntentionRepository>();
            services.AddTransient<IApplicationRepository, ApplicationRepository>();
            services.AddTransient<IApplicationUserRepository, ApplicationUserRepository>();

            services.AddTransient<INaturalLanguageUnderstanding, DialogFlowConnector>();

            services.AddAutoMapper();

            var provider = services.BuildServiceProvider();
            services.AddBot<CarlitosBnbBot>(options =>
                {
                    options.CredentialProvider = new ConfigurationCredentialProvider(Configuration);

                    // The CatchExceptionMiddleware provides a top-level exception handler for your bot. 
                    // Any exceptions thrown by other Middleware, or by your OnTurn method, will be 
                    // caught here. To facillitate debugging, the exception is sent out, via Trace, 
                    // to the emulator. Trace activities are NOT displayed to users, so in addition
                    // an "Ooops" message is sent. 
                    options.Middleware.Add(new CatchExceptionMiddleware<Exception>(async (context, exception) =>
                            {
                                _logger.LogError(string.Format("Middleware Custom Exception {0} - {1}", exception.Message, exception.StackTrace));

                                string detailException = string.Empty;
                                if (exception is BaseException)
                                {
                                    var ex = (BaseException)exception;

                                    if (!string.IsNullOrWhiteSpace(ex.UserMessageError))
                                    {
                                        await context.SendActivity(ex.UserMessageError);
                                    }
                                }

                                if (exception is ApiServicesException)
                                {
                                    var ex = (ApiServicesException)exception;
                                    detailException = ex.ToString();
                                }
                                else
                                {
                                    detailException = exception.ToString();
                                }

                                await context.TraceActivity("CarlitosBnbBot Exception", exception);

                                if (string.IsNullOrWhiteSpace(detailException))
                                {
                                    detailException = $"CarlitosBnbBot Empty Exception {exception.GetType()} Ex: {exception}";
                                }

                                //var logService = provider.GetService<IService<LogInfo>>();
                                //logService.Data = LogInfo.Load(context.Activity.From.Id, detailException, exception.StackTrace);
                                //await logService.Process();

                                await context.SendActivity("Perdón, no puedo completar tu solicitud en este momento. Intenta nuevamente en unos minutos.");
                            }));

                    // The Memory Storage used here is for local bot debugging only. When the bot
                    // is restarted, anything stored in memory will be gone. 
                    //IStorage dataStore = new MemoryStorage();
                    IStorage dataStoreFile = new FileStorage(System.IO.Path.GetTempPath());
                    // The File data store, shown here, is suitable for bots that run on 
                    // a single machine and need durable state across application restarts.                 
                    //IStorage dataStoreFile = new FileStorage(System.IO.Path.GetTempPath());

                    // For production bots use the Azure Table Store, Azure Blob, or 
                    // Azure CosmosDB storage provides, as seen below. To include any of 
                    // the Azure based storage providers, add the Microsoft.Bot.Builder.Azure 
                    // Nuget package to your solution. That package is found at:
                    //      https://www.nuget.org/packages/Microsoft.Bot.Builder.Azure/

                    //IStorage dataStore = new Microsoft.Bot.Builder.Azure.AzureTableStorage(this.Configuration.GetValue<string>("Azure:connectionString"), "carlitosmigration");
                    // IStorage dataStore = new Microsoft.Bot.Builder.Azure.AzureBlobStorage("AzureBlobConnectionString", "containerName");
                    var conversationState = new ConversationState<SessionData>(dataStoreFile);
                    var userState = new UserState<UserProfile>(dataStoreFile);
                    options.Middleware.Add(new ConversationState<ConversationInfo>(dataStoreFile));
                    //options.Middleware.Add(new UserState<UserInfo>(new DatabaseStorage(provider.GetRequiredService<IUserService>())));

                    //options.Middleware.Add(new ShowTypingMiddleware(delay: 0));

                    //options.Middleware.Add(new WelcomeMiddleware(this.Configuration, provider.GetRequiredService<IUserService>(), provider.GetRequiredService<ICardService>(), provider.GetRequiredService<ILogService>()));
                    //options.Middleware.Add(new WelcomeMiddleware(this.Configuration, provider));
                    options.Middleware.Add(new WelcomeV2Middleware(provider.GetService<IAuthenticationService>(), provider.GetService<IService<GetCardDto>>()));
                    options.Middleware.Add(new ContentTypeMiddlware());
                    options.Middleware.Add(
                        new DialogFlowRecognizerMiddleware(
                            new DialogFlowConnector(
                                this.Configuration.GetValue<string>("NLU:DialogFlow:ProjectId"),
                                this.Configuration.GetValue<string>("NLU:DialogFlow:FileJson"),
                                this.Configuration.GetValue<string>("NLU:DialogFlow:Language")
                                ), this.Configuration, provider));
                    options.Middleware.Add(new QueryBiMiddleware(this.Configuration, provider));
                    //options.Middleware.Add(new RetryValidatePromptMiddleware(this.Configuration));
                    options.EnableProactiveMessages = true;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles()
                .UseStaticFiles()
                .UseBotFramework();
        }
    }
}