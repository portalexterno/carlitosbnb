﻿namespace CarlitosBnb.Settings
{
    public class BnbChatBotServices
    {
        public string ApplicationKey { get; set; }

        public string ApplicationSecret { get; set; }

        public string BaseUrl { get; set; }

        public string GetTokenApplication { get; set; }

        public string RequestTokenUser { get; set; }

        public string GetTokenUser { get; set; }

        public string GetCreditCards { get; set; }

        public string CreditCardStatement { get; set; }

        public string ClientBalance { get; set; }

        public string ClientMovements { get; set; }
    }

    public class Services
    {
        public string BaseUrl { get; set; }

        public string BaseApp { get; set; }
    }

    public class AuthenticationServices : Services
    {
        public string ApplicationKey { get; set; }

        public string ApplicationSecret { get; set; }

        public string GetTokenApplication { get; set; }

        public string RequestTokenUser { get; set; }

        public string GetTokenUser { get; set; }

        public string AcceptContract { get; set; }
    }

    public class ClientServices : Services
    {
        public string GetCreditCards { get; set; }

        public string CreditCardStatement { get; set; }

        public string ClientBalance { get; set; }

        public string ClientMovements { get; set; }

        public string GetPeriods { get; set; }
    }
}
