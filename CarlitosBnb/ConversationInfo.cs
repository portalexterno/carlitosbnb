﻿namespace CarlitosBnb
{
    using System;
    using System.Collections.Generic;

    public class ConversationInfo : Dictionary<string, object>
    {
        public struct NameKeys
        {
            public const string COUNTER_ATTEMPTS_LOGIN = "CounterAttemptsLogin";

            public const string COUNTER_ATTEMPTS_OTP_CODE = "CounterAttemptsOtpCode";

            public const string IS_CONTRACT_ACCEPTED = "IsContractAccepted";

            public const string IS_CONTRACT_STEP = "IsContractStep";

            public const string IS_PROCESS_SAVE_USER = "IsProcessSaveUser";

            public const string LAST_SESSION = "LastSession";

            public const string CLIENTS = "Clients";

            public const string ACCOUNT = "Account";

            public const string DOCUMENT = "Document";

            public const string PROFILE_SESSION = "PROFILE_SESSION";

            public const string CURRENT_INTENT = "CURRENT_INTENT";

            public const string NAME_INTENT = "NAME_INTENT";

            public const string DIALOGFLOW_RESPONSE_TEXT = "DIALOGFLOW_RESPONSE_TEXT";

            public const string ENABLE_INTERNET_PAYMENT_DATA = "ENABLE_INTERNET_PAYMENT_DATA";

            public const string IS_COMPLETED_TASK = "IS_COMPLETED_TASK";

            public const string CREDIT_CARD_EXTRACT_INFO = "CREDIT_CARD_EXTRACT_INFO";

            public const string AUTH_TOKEN = "AUTH_TOKEN";

            public const string AUTH_USER_TOKEN = "AUTH_USER_TOKEN";
        }

        public ConversationInfo()
        {
            this.ExpectedResponse = false;
            this.IsSkipDialogFlow = false;
            this.IsImage = false;
            this.IsAudio = false;
            this.IsContractAccepted = false;
            this.IsContractStep = false;
            this.IsProcessSaveUser = false;
            this.CounterAttemptsLogin = 0;
            this.CounterAttemptsOtpCode = 0;
            this.IsTyping = false;
            this.ProfileSession = new ProfileSession();
            this.CurrentIntent = string.Empty;
            this.NameIntent = string.Empty;
            this.DialogflowTextResponse = string.Empty;
            this.EnableInternetPaymentData = new EnableInternetPaymentData();
            this.IsCompletedTask = false;
            this.LastSession = null;
            this.CreditCardExtractInfo = new CreditCardExtractInfo();
            this.AuthToken = string.Empty;
            this.AuthUserToken = string.Empty;
            this.Account = string.Empty;
        }

        public bool HasAuthToken
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.AuthToken);
            }
        }

        public bool HasAuthUserToken
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.AuthUserToken);
            }
        }

        public bool IsTyping;

        public string AuthToken
        {
            get
            {
                if (!this.ContainsKey(NameKeys.AUTH_TOKEN))
                {
                    return null;
                }

                return this[NameKeys.AUTH_TOKEN].ToString();
            }
            set
            {
                this[NameKeys.AUTH_TOKEN] = value;
            }
        }

        public string AuthUserToken
        {
            get
            {
                if (!this.ContainsKey(NameKeys.AUTH_USER_TOKEN))
                {
                    return null;
                }

                return this[NameKeys.AUTH_USER_TOKEN].ToString();
            }
            set
            {
                this[NameKeys.LAST_SESSION] = DateTime.Now;
                this[NameKeys.AUTH_USER_TOKEN] = value;
            }
        }

        public string Account
        {
            get
            {
                if (!this.ContainsKey(NameKeys.ACCOUNT))
                {
                    return null;
                }

                return this[NameKeys.ACCOUNT].ToString();
            }
            set
            {
                this[NameKeys.ACCOUNT] = value;
            }
        }


        public DateTime? LastSession
        {
            get
            {
                return (DateTime?)this[NameKeys.LAST_SESSION];
            }
            set
            {
                this[NameKeys.LAST_SESSION] = value;
            }
        }

        public bool IsProcessSaveUser
        {
            get
            {
                return (int)this[NameKeys.IS_PROCESS_SAVE_USER] == 1;
            }
            set
            {
                this[NameKeys.IS_PROCESS_SAVE_USER] = value ? 1 : 0;
            }
        }

        public bool IsContractStep
        {
            get
            {
                return (int)this[NameKeys.IS_CONTRACT_STEP] == 1;
            }
            set
            {
                this[NameKeys.IS_CONTRACT_STEP] = value ? 1 : 0;
            }
        }

        public int CounterAttemptsLogin
        {
            get
            {
                return int.Parse(this[NameKeys.COUNTER_ATTEMPTS_LOGIN].ToString());
            }
            set
            {
                this[NameKeys.COUNTER_ATTEMPTS_LOGIN] = value;
            }
        }

        public int CounterAttemptsOtpCode
        {
            get
            {
                return int.Parse(this[NameKeys.COUNTER_ATTEMPTS_OTP_CODE].ToString());                
            }
            set
            {
                this[NameKeys.COUNTER_ATTEMPTS_OTP_CODE] = value;
            }
        }

        public ProfileSession ProfileSession
        {
            get
            {
                if (!this.ContainsKey(NameKeys.PROFILE_SESSION))
                {
                    return null;
                }

                return (ProfileSession)this[NameKeys.PROFILE_SESSION];
            }
            set
            {
                this[NameKeys.PROFILE_SESSION] = value;
            }
        }

        public EnableInternetPaymentData EnableInternetPaymentData
        {
            get
            {
                if (!this.ContainsKey(NameKeys.ENABLE_INTERNET_PAYMENT_DATA))
                {
                    return null;
                }

                return (EnableInternetPaymentData)this[NameKeys.ENABLE_INTERNET_PAYMENT_DATA];
            }
            set
            {
                this[NameKeys.ENABLE_INTERNET_PAYMENT_DATA] = value;
            }
        }

        public string CurrentIntent
        {
            get
            {
                if (!this.ContainsKey(NameKeys.CURRENT_INTENT))
                {
                    return null;
                }

                return (string)this[NameKeys.CURRENT_INTENT];
            }
            set
            {
                this[NameKeys.CURRENT_INTENT] = value;
            }
        }

        public string NameIntent
        {
            get
            {
                if (!this.ContainsKey(NameKeys.NAME_INTENT))
                {
                    return null;
                }

                return (string)this[NameKeys.NAME_INTENT];
            }
            set
            {
                this[NameKeys.NAME_INTENT] = value;
            }
        }

        public string DialogflowTextResponse
        {
            get
            {
                if (!this.ContainsKey(NameKeys.DIALOGFLOW_RESPONSE_TEXT))
                {
                    return null;
                }

                return (string)this[NameKeys.DIALOGFLOW_RESPONSE_TEXT];
            }
            set
            {
                this[NameKeys.DIALOGFLOW_RESPONSE_TEXT] = value;
            }
        }

        public bool IsCompletedTask
        {
            get
            {
                if (!this.ContainsKey(NameKeys.IS_COMPLETED_TASK))
                {
                    return false;
                }

                return (bool)this[NameKeys.IS_COMPLETED_TASK];
            }
            set
            {
                this[NameKeys.IS_COMPLETED_TASK] = value;
            }
        }

        public bool IsContractAccepted
        {
            get
            {
                if (!this.ContainsKey(NameKeys.IS_CONTRACT_ACCEPTED))
                {
                    return false;
                }

                return (bool)this[NameKeys.IS_CONTRACT_ACCEPTED];
            }
            set
            {
                this[NameKeys.IS_CONTRACT_ACCEPTED] = value;
            }
        }

        public bool IsAudio { get; protected set; }

        public bool IsImage { get; protected set; }

        public bool ExpectedResponse { get; protected set; }

        public bool IsSkipDialogFlow { get; protected set; }

        public CreditCardExtractInfo CreditCardExtractInfo
        {
            get
            {
                if (!this.ContainsKey(NameKeys.CREDIT_CARD_EXTRACT_INFO))
                {
                    return null;
                }

                return (CreditCardExtractInfo)this[NameKeys.CREDIT_CARD_EXTRACT_INFO];
            }
            set
            {
                this[NameKeys.CREDIT_CARD_EXTRACT_INFO] = value;
            }
        }

        public void SkipDialogFlow()
        {
            this.IsSkipDialogFlow = true;
        }

        public void NotSkipDialogFlow()
        {
            this.IsSkipDialogFlow = false;
        }

        public void HasExpectedResponse()
        {
            this.ExpectedResponse = true;
        }

        public void HasUnExpectedResponse()
        {
            this.ExpectedResponse = false;
        }

        public void HasAudio()
        {
            this.IsAudio = true;
            this.IsImage = false;
        }

        public void HasImage()
        {
            this.IsAudio = false;
            this.IsImage = true;
        }

        public void HasMessage()
        {
            this.IsAudio = false;
            this.IsImage = false;
        }
    }
}