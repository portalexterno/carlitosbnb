﻿namespace CarlitosBnb.Core.Factories
{
    using BusinessLogic.Modules.CardModule;
    using Microsoft.Bot.Schema;
    using System.Linq;

    public class HeroCardFactory : IAttachmentFactory
    {
        public string UserName { get; set; }

        public Attachment Create(CardInfo card)
        {
            var heroCard = new HeroCard();
            if (!string.IsNullOrEmpty(card.Title))
                heroCard.Title = string.Format(card.Title, this.UserName);

            if (!string.IsNullOrEmpty(card.SubTitle))
                heroCard.Subtitle = string.Format(card.SubTitle, this.UserName);

            if (!string.IsNullOrEmpty(card.Text))
                heroCard.Text = string.Format(card.Text, this.UserName);

            if (card.Media != null)
            {
                heroCard.Images = new ListCardImageConstructor(card.Media).CardImages;
            }

            if (card.Buttons != null)
            {
                heroCard.Buttons = new ListCardActionConstructor(card.Buttons.OrderBy(c => c.Order).ToList()).CardActions;
            }

            return heroCard.ToAttachment();
        }
    }
}
