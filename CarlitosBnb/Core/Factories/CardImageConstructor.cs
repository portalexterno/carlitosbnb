﻿namespace CarlitosBnb.Core.Factories
{
    using BusinessLogic.Modules.CardModule;
    using Microsoft.Bot.Schema;

    public class CardImageConstructor
    {
        public CardImageConstructor(CardMediaInfo cardMedia)
        {
            this.cardImage = new CardImage(url: cardMedia.Url);
            if (cardMedia != null)
            {
                this.cardImage.Tap = new CardActionConstructor(cardMedia.CardAction).CardAction;
            }
        }

        private CardImage cardImage;
    }
}
