﻿namespace CarlitosBnb.Core.Factories
{
    using System.Collections.Generic;

    public class SelectorFactory
    {
        private static Dictionary<string, IAttachmentFactory> factories = new Dictionary<string, IAttachmentFactory>()
        {
            { "HERO_CARD", new HeroCardFactory() }
        };

        public static IAttachmentFactory Get(string type)
        {
            IAttachmentFactory attachmentFactory;
            if (!factories.TryGetValue(type, out attachmentFactory))
            {
                throw new KeyNotFoundException($"Los tipos de tarjetas disponibles son HERO_CARD, se intento con {type}");
            }

            return attachmentFactory;
        }
    }
}
