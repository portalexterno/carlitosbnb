﻿namespace CarlitosBnb.Core.Factories
{
    using BusinessLogic.Modules.CarouselModule;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Schema;
    using System.Collections.Generic;

    public class CarouselCreator : ICreator<CarouselInfo>
    {
        public string UserName { get; set; }

        public IMessageActivity Create(CarouselInfo carousel)
        {
            var attachments = new List<Attachment>();

            foreach (var card in carousel.Cards)
            {
                attachments.Add(SelectorFactory.Get(card.Type).Create(card));
            }

            var carouselActivity = MessageFactory.Carousel(attachments, carousel.Text);
            return carouselActivity;
        }
    }
}
