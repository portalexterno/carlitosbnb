﻿namespace CarlitosBnb.Core.Factories
{
    using Microsoft.Bot.Schema;

    interface ICreator<Seed>
    {
        IMessageActivity Create(Seed card);

        string UserName { get; set; }
    }
}
