﻿namespace CarlitosBnb.Core.Factories
{
    using BusinessLogic.Modules.CardModule;
    using Microsoft.Bot.Schema;
    using System.Collections.Generic;

    public class ListCardImageConstructor
    {
        public ListCardImageConstructor(List<CardMediaInfo> images)
        {
            this.CardImages = new List<CardImage>();
            foreach (var image in images)
            {
                this.CardImages.Add(new CardImage(url: image.Url, tap: image.CardAction != null ? new CardActionConstructor(image.CardAction).CardAction : null));
            }
        }

        public List<CardImage> CardImages { get; private set; }
    }
}
