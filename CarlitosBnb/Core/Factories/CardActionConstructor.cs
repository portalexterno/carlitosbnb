﻿namespace CarlitosBnb.Core.Factories
{
    using BusinessLogic.Modules.CardModule;
    using Microsoft.Bot.Schema;

    public class CardActionConstructor
    {
        public CardActionConstructor(CardActionInfo cardAction)
        {
            this.CardAction = new CardAction(type: cardAction.Type, title: cardAction.Title, value: cardAction.Value);
        }

        public CardAction CardAction { get; private set; }
    }
}
