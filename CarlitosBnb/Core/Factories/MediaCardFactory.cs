﻿namespace CarlitosBnb.Core.Factories
{
    using BusinessLogic.Modules.CardModule;
    using Microsoft.Bot.Schema;
    using System.Collections.Generic;
    using System.Linq;

    public class MediaCardFactory : IAttachmentFactory
    {
        public string UserName { get; set; }

        public Attachment Create(CardInfo card)
        {
            var mediaCard = new HeroCard();

            if (!string.IsNullOrEmpty(card.Title))
                mediaCard.Title = string.Format(card.Title, this.UserName);

            if (!string.IsNullOrEmpty(card.SubTitle))
                mediaCard.Subtitle = string.Format(card.SubTitle, this.UserName);

            if (!string.IsNullOrEmpty(card.Text))
                mediaCard.Text = string.Format(card.Text, this.UserName);

            if (card.Media != null)
            {
                //mediaCard.Media = new List<MediaUrl>();
                mediaCard.Images = new List<CardImage>();

                foreach (var url in card.Media)
                {
                    mediaCard.Images.Add(new CardImage() { Url = url.Url });
                }

                //mediaCard.Media.Add(new MediaUrl());
                //= new ListCardImageConstructor(card.Media).CardImages;
            }

            if (card.Buttons != null)
            {
                mediaCard.Buttons = new ListCardActionConstructor(card.Buttons.OrderBy(c => c.Order).ToList()).CardActions;
            }
            return mediaCard.ToAttachment();
        }
    }
}
