﻿namespace CarlitosBnb.Core.Factories
{
    using BusinessLogic.Modules.CardModule;
    using Microsoft.Bot.Schema;
    using System.Collections.Generic;

    public class ListCardActionConstructor
    {
        public ListCardActionConstructor(List<CardActionInfo> buttons)
        {
            this.CardActions = new List<CardAction>();
            foreach (var button in buttons)
            {
                this.CardActions.Add(new CardAction(type: button.Type, title: button.Title, value: button.Value));
            }
        }

        public List<CardAction> CardActions { get; private set; }
    }
}
