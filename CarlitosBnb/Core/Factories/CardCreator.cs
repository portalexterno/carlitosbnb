﻿namespace CarlitosBnb.Core.Factories
{
    using BusinessLogic.Modules.CardModule;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Schema;

    public class CardCreator : ICreator<CardInfo>
    {
        public string UserName { get; set; }

        public IMessageActivity Create(CardInfo card)
        {
            IAttachmentFactory attachmentFactory = SelectorFactory.Get(card.Type);
            attachmentFactory.UserName = this.UserName;
            return MessageFactory.Attachment(attachmentFactory.Create(card));
        }
    }
}
