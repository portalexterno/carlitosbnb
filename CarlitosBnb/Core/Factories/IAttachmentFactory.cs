﻿namespace CarlitosBnb.Core.Factories
{
    using BusinessLogic.Modules.CardModule;
    using Microsoft.Bot.Schema;

    public interface IAttachmentFactory
    {
        string UserName { get; set; }

        Attachment Create(CardInfo card);
    }
}
