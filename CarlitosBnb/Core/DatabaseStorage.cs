﻿using BusinessLogic.Modules.UserModule;
using Microsoft.Bot.Builder.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarlitosBnb.Core
{
    public class DatabaseStorage : IStorage
    {
        public DatabaseStorage(IUserService userService)
        {
            this.userService = userService;
        }

        private IUserService userService;

        public Task Delete(params string[] keys)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<KeyValuePair<string, object>>> Read(params string[] keys)
        {
            var result = await this.userService.GetByFacebookId(new GetUserByFacebookIdDto()
            {
                ChannelAccountId = keys[0]
            });

            List<KeyValuePair<string, object>> list = new List<KeyValuePair<string, object>>();
            list.Add(new KeyValuePair<string, object>(result.ChannelAccountId, result));
            return list;
        }

        public async Task Write(IEnumerable<KeyValuePair<string, object>> changes)
        {
            //await this.userService.SaveUser(new Database.Core.Entities.User()
            //{
                
            //});
        }
    }
}
