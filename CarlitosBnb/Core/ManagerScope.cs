﻿namespace CarlitosBnb.Core
{
    using BusinessLogic.Modules;
    //using BusinessLogic.Modules.LogModule;
    using Microsoft.Bot.Builder;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class ManagerScope
    {
        public ManagerScope(string channelAccountId)
        {
            this.channelAccountId = channelAccountId;
        }

        private string channelAccountId;

        private static ITypingActivity typing = Activity.CreateTypingActivity();

        private async Task<Result> process<Dto>(ITurnContext context, Dto data, IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                //var logService = scope.ServiceProvider.GetRequiredService<IService<LogInfo>>();
                try
                {
                    var service = scope.ServiceProvider.GetRequiredService<IService<Dto>>();
                    service.Data = data;
                    var result = await service.Process();
                    //if (!result.IsOk)
                    //{
                    //    logService.Data = LogInfo.Load(this.channelAccountId, result.Message, result.StackTrace);
                    //    await logService.Process();
                    //}

                    return result;
                }
                catch (Exception e)
                {
                    //logService.Data = LogInfo.Load(this.channelAccountId, e.Message, e.StackTrace);
                    //await logService.Process();

                    return Result.Fail("No se pudo guardar el usuario");
                }
            }
        }

        public static async Task<Result> Process<Dto>(ITurnContext context, Dto data, IServiceProvider serviceProvider = null)
        {
            var conversationInfo = context.GetConversationState<ConversationInfo>();
            //conversationInfo.IsTyping = true;

            if (!conversationInfo.ContainsKey("dialogStack") || conversationInfo["dialogStack"] == null || (conversationInfo["dialogStack"] as List<DialogInstance>).Count <= 0)
            {
                await context.SendActivity(typing);
            }

            return await new ManagerScope(context.Activity.From.Id).process(context, data, serviceProvider ?? context.Services.Get<IServiceProvider>(CarlitosBnbBot.SERVICE_PROVIDER_ID));
        }
    }
}
