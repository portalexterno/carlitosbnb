﻿namespace CarlitosBnb
{
    using Microsoft.Bot.Schema;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class Extensions
    {
        public static string FirstName(this ChannelAccount from)
        {
            return from.Name.Split(' ')[0];
        }

        public static bool HasAnyFormat(this IList<Attachment> attachments, params string[] formats)
        {
            return attachments.Any(a => formats.Contains(a.ContentType));
        }

        public static Attachment GetOneHasAnyFormats(this IList<Attachment> attachments, params string[] formats)
        {
            return attachments.FirstOrDefault(a => formats.Contains(a.ContentType));
        }
    }
}
