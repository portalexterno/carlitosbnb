﻿namespace CarlitosBnb.Middlewares
{
    using Microsoft.Bot.Builder;
    using System.Threading.Tasks;

    public class PostResponseMiddleware : IMiddleware
    {
        public async Task OnTurn(ITurnContext context, MiddlewareSet.NextDelegate next)
        {
            await next().ConfigureAwait(false);
            //if (context.Responded)
            await context.SendActivity($"{context.Activity.From.FirstName()}, dime en que más te puedo ayudar.");
        }
    }
}
