﻿using CarlitosBnb.SeedWorks;
using CarlitosBnb.Settings;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.Middlewares.Welcome
{
    public class LoginApplicationMiddleware : IMiddleware
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly BnbChatBotServices settingsBnbChatBotServices;
        private const string GENERIC_BOT = "Bot";

        private struct Texts
        {
            public const string GREETING = "BNB Tú Primero";

            public const string ERROR_SAVE_USER = "Perdón su usuario no se ha podido registrar debido a un error interno, por favor comuniquese con el administrador para notificar este problema";

            public const string ERROR_GET_USER = "Perdón hubo un problema al intentar obtener su usuario, si el problema persiste por favor comuniquese con el administrador para notificar este problema";
        }

        public LoginApplicationMiddleware(IHttpClientFactory httpClientFactory, IOptions<BnbChatBotServices> options)
        {
            this.httpClientFactory = httpClientFactory;
            this.settingsBnbChatBotServices = options.Value;
        }

        public async Task OnTurn(ITurnContext context, MiddlewareSet.NextDelegate next)
        {
            if (context.Activity.Type == ActivityTypes.ConversationUpdate)
            {
                var conversation = ConversationState<ConversationInfo>.Get(context);

                if (conversation.HasAuthToken)
                {
                    conversation.IsContractAccepted = true;
                    return;
                }

                var requestGetApplicationToken = new RequestGetApplicationToken()
                {
                    ApplicationKey = settingsBnbChatBotServices.ApplicationKey,
                    ApplicationSecret = settingsBnbChatBotServices.ApplicationSecret,
                    ChannelAccountId = context.Activity.From.Id,
                    ConversationId = context.Activity.Conversation.Id
                };

                var httpClientBnbChatBotServices = this.httpClientFactory.CreateClient("BnbChatBotServices");
                var responseApplicationToken = await httpClientBnbChatBotServices.PostV2<ResponseGetApplicationToken>(
                    settingsBnbChatBotServices.GetTokenApplication, requestGetApplicationToken);
                conversation.AuthToken = responseApplicationToken.JwtToken;
                conversation.IsContractAccepted = responseApplicationToken.IsContractAccepted;
            }

            await context.SendActivity(Texts.GREETING);
            await next();
        }
    }

    public class RequestGetApplicationToken
    {
        public string ApplicationKey { get; set; }

        public string ApplicationSecret { get; set; }

        public string ChannelAccountId { get; set; }

        public string ConversationId { get; set; }
    }

    public class ResponseGetApplicationToken
    {
        public string JwtToken { get; set; }

        public bool IsContractAccepted { get; set; }
    }
}
