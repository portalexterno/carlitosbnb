﻿namespace CarlitosBnb.Middlewares.Welcome
{
    using Newtonsoft.Json;

    public class FacebookUserResponse
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("profile_pic")]
        public string ProfilePic { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
