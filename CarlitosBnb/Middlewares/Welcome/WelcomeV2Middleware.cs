﻿using BusinessLogic.Modules;
using BusinessLogic.Modules.CardModule;
using CarlitosBnb.ApiServices;
using CarlitosBnb.Dialogs;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CarlitosBnb.Middlewares.Welcome
{
    public class WelcomeV2Middleware : IMiddleware
    {
        private readonly IAuthenticationService authenticationService;
        private readonly IService<GetCardDto> serviceGetCard;

        public WelcomeV2Middleware(IAuthenticationService authenticationService, IService<GetCardDto> serviceGetCard)
        {
            this.authenticationService = authenticationService;
            this.serviceGetCard = serviceGetCard;
        }

        private struct Texts
        {
            public const string GREETING = "BNB Tú Primero";

            public const string ERROR_SAVE_USER = "Perdón su usuario no se ha podido registrar debido a un error interno, por favor comuniquese con el administrador para notificar este problema";

            public const string ERROR_GET_USER = "Perdón hubo un problema al intentar obtener su usuario, si el problema persiste por favor comuniquese con el administrador para notificar este problema";
        }

        public async Task OnTurn(ITurnContext context, MiddlewareSet.NextDelegate next)
        {

            //if (context.Activity.Type == ActivityTypes.ConversationUpdate)
            //{
            //    await context.SendActivity(Texts.GREETING);
            //}

            if (context.Activity.Type == ActivityTypes.Message)
            {
                var conversation = ConversationState<ConversationInfo>.Get(context);
                if (string.IsNullOrWhiteSpace(conversation.AuthToken) || !conversation.IsContractAccepted)
                {
                    var responseGetToken = await this.authenticationService.GetToken(context.Activity.From.Id, context.Activity.Conversation.Id);
                    conversation.AuthToken = responseGetToken.JwtToken;
                    conversation.IsContractAccepted = responseGetToken.IsContractAccepted;
                }

                if (conversation.IsContractAccepted)
                {
                    await next();
                    return;
                }

                var dialogs = new DialogSet();
                dialogs.Add(ContractDialogContainer.Id, ContractDialogContainer.Instance);
                var dc = dialogs.CreateContext(context, conversation);
                dc.Context.Services.Add(this.authenticationService);
                dc.Context.Services.Add(this.serviceGetCard);

                await dc.Continue();

                if (!dc.Context.Responded)
                {
                    await dc.Begin(ContractDialogContainer.Id, conversation);
                }
            }

        }
    }
}