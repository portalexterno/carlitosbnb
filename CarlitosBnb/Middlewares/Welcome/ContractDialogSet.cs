﻿namespace CarlitosBnb.Middlewares.Welcome
{
    using BusinessLogic.Modules.CardModule;
    using BusinessLogic.Modules.UserModule;
    using CarlitosBnb.Core.Factories;
    using CarlitosBnb.Middlewares.DialogFlow;
    using Google.Cloud.Dialogflow.V2;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Builder.Prompts.Choices;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Recognizers.Text;
    using System;
    using System.Collections.Generic;

    public class ContractDialogSet : DialogSet
    {
        public const string Id = "ContractDialog";

        private const string CONTRACT_CHOICE_PROMPT = "ChoiceOptionsContract";

        private const string CONTRACT_YES = "CONTRACT_YES";
        private const string CONTRACT_NOT = "CONTRACT_NOT";

        private static List<string> SYNONYMS_CONTRACT_YES = new List<string>()
        {
            "Si", "claro", "por supuesto", "de acuerdo", "si acepto"
        };

        private static List<string> SYNONYMS_CONTRACT_NOT = new List<string>()
        {
            "no"
        };

        private List<Choice> optionsContractAccepted = new List<Choice>()
        {
            new Choice()
            {
                Action = new CardAction(title: "Sí acepto", type: ActionTypes.PostBack, value: CONTRACT_YES),
                Synonyms = SYNONYMS_CONTRACT_YES,
                Value = CONTRACT_YES
            },
            new Choice()
            {
                Action = new CardAction(title: "No acepto", type: ActionTypes.PostBack, value: CONTRACT_NOT),
                Synonyms = SYNONYMS_CONTRACT_NOT,
                Value = CONTRACT_NOT
            }
        };

        public ContractDialogSet(IUserService userService, ICardService cardService)
        {
            this.Add(CONTRACT_CHOICE_PROMPT, new ChoicePrompt(culture: Culture.Spanish));

            this.Add(Id, new WaterfallStep[]
            {
                async(dc, args, next) =>
                {
                    var card = MessageFactory.Attachment(new HeroCard(
                        title: $"¡Hola! {dc.Context.Activity.From.FirstName() }",
                        subtitle: "Soy Carlitos. El chatbot del Banco Nacional de Bolivia.",
                        images: new List<CardImage>() { new CardImage($"http://bnbchatbotpruebas.azurewebsites.net/Content/images/CarlitosBotIcon.png") }
                        ).ToAttachment());

                    await dc.Context.SendActivity(card);

                    await dc.Context.SendActivity("- Antes de Comenzar debes estar de acuerdo con los términos y condiciones que se describen en el pdf que te mando a continuación.");

                    var conditions = MessageFactory.Attachment(new HeroCard(
                        title: "Tërminos y condiciones",
                        buttons: new List<CardAction>()
                        {
                            new CardAction(
                                title: "Ver",
                                type: ActionTypes.OpenUrl,
                                value: "http://bnbchatbotpruebas.azurewebsites.net/Content/pdf/Terminosycondiciones.pdf"
                                )
                        }
                        ).ToAttachment());

                    await dc.Context.SendActivity(conditions);

                    await dc.Prompt(CONTRACT_CHOICE_PROMPT, "¿Aceptas los términos y condiciones?", new ChoicePromptOptions()
                    {
                        Choices = optionsContractAccepted,
                        RetryPromptActivity = ChoiceFactory.SuggestedAction(optionsContractAccepted, "Por favor seleccione una de las opciones") as Activity
                    });
                },
                async(dc, args, next) =>
                {
                    var selectedOption = (FoundChoice)args["Value"];
                    if(selectedOption.Value == CONTRACT_YES)
                    {
                        await userService.AcceptContract(new GetUserByFacebookIdDto()
                        {
                            ChannelAccountId = dc.Context.Activity.From.Id
                        });

                        await dc.Context.SendActivity("¡Muy bien! Aceptaste los términos y condiciones");
                        await dc.Context.SendActivity("¡Bienvenido! Estas son las opciones que tengo para ti");
                        var serviceProvider = dc.Context.Services.Get<IServiceProvider>(CarlitosBnbBot.SERVICE_PROVIDER_ID);
                        CardInfo card = new CardInfo();
                        
                        //var result = dc.Context.Services.Get<QueryResult>(DialogFlowRecognizerMiddleware.DialogFlowRecognizerResultKey);
                        card = await cardService.GetByIntent(new GetCardDto(){
                            Intent = "b8fe65ab-ebee-44c3-a715-6bce21e7e7c4"
                        });

                        var creator = new CardCreator();
                        creator.UserName = dc.Context.Activity.From.FirstName();
                        var message = creator.Create(card);
                        await dc.Context.SendActivity(message);
                        await dc.End();
                    }
                    else
                    {
                        await dc.Context.SendActivity("Lo siento, si no estás de acuerdo con los términos y condiciones del servicio no puedes seguir interactuando commigo");
                    }

                    
                }
            });
        }
    }
}
