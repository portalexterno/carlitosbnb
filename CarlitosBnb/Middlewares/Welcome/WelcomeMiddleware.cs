﻿namespace CarlitosBnb.Middlewares.Welcome
{
    using BusinessLogic.Modules;
    //using BusinessLogic.Modules.LogModule;
    using BusinessLogic.Modules.UserModule;
    using CarlitosBnb.Core;
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Microsoft.Bot.Builder;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class WelcomeMiddleware : IMiddleware
    {
        private IServiceProvider provider;

        private IConfiguration configuration;

        private const string GENERIC_BOT = "Bot";

        //private static bool isProcessContract;

        public WelcomeMiddleware(IConfiguration configuration, IServiceProvider provider)
        {
            this.provider = provider;
            this.configuration = configuration;
        }

        private struct Texts
        {
            public const string GREETING = "BNB Tú Primero";

            public const string ERROR_SAVE_USER = "Perdón su usuario no se ha podido registrar debido a un error interno, por favor comuniquese con el administrador para notificar este problema";

            public const string ERROR_GET_USER = "Perdón hubo un problema al intentar obtener su usuario, si el problema persiste por favor comuniquese con el administrador para notificar este problema";
        }

        public async Task OnTurn(ITurnContext context, MiddlewareSet.NextDelegate next)
        {
            if (context.Activity.Type == ActivityTypes.ConversationUpdate)
            {
                var newUserName = context.Activity.MembersAdded.FirstOrDefault()?.Name;

                if (string.IsNullOrWhiteSpace(newUserName) || newUserName == GENERIC_BOT)
                {
                    return;
                }

                var conversation = ConversationState<ConversationInfo>.Get(context);

                var result = await ManagerScope.Process(context, new GetUserByChannelIdDto()
                {
                    ChannelAccountId = context.Activity.From.Id
                }, provider);

                if (!result.IsOk)
                {
                    //await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, result.Message, result.StackTrace), provider);
                    await context.SendActivity(Texts.ERROR_GET_USER);
                    return;
                }

                var resultUser = result as Result<UserInfo>;
                if (resultUser.Body == null)
                {
                    var newUserInfo = new UserInfo()
                    {
                        ChannelAccountId = context.Activity.From.Id,
                        ServiceUrl = context.Activity.ServiceUrl,
                        FullName = context.Activity.From.Name,
                        ConversationId = context.Activity.Conversation.Id
                    };

                    var urlfacebook = $"https://graph.facebook.com/{context.Activity.From.Id}?fields=first_name,last_name,profile_pic&access_token={this.configuration.GetValue<string>("FaceboookToken")}";
                    var response = await Connection.Process(new ConnectorRestGetString<FacebookUserResponse>(HttpClientFactory.Create(), urlfacebook));
                    if (!response.IsOk)
                    {
                        //await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, response.Message, "WelcomeMIddlware.OnTurn__Connection.Process(new ConnectorRestGetString<FacebookUserResponse>(urlfacebook))"), provider);
                    }
                    else
                    {
                        var facebookUserData = response as Response<FacebookUserResponse>;
                        newUserInfo.FirstName = facebookUserData.Body.FirstName;
                        newUserInfo.LastName = facebookUserData.Body.LastName;
                        newUserInfo.Gender = facebookUserData.Body.Gender;
                        newUserInfo.Picture = facebookUserData.Body.ProfilePic;
                    }

                    var resultSaveUser = await ManagerScope.Process(context, newUserInfo, provider);
                    if (!resultSaveUser.IsOk)
                    {
                        //await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, resultSaveUser.Message, resultSaveUser.StackTrace), provider);
                        await context.SendActivity(Texts.ERROR_SAVE_USER);
                    }

                    conversation.IsContractAccepted = false;
                }
                else
                {
                    if (resultUser.Body.ConversationId != context.Activity.Conversation.Id)
                    {
                        var resultUpdateConversationId = await ManagerScope.Process(context, new UpdateConversationIdDto()
                        {
                            ApplicationName = this.configuration.GetValue<string>("AppName"),
                            ConversationId = context.Activity.Conversation.Id,
                            ChannelAccountId = context.Activity.From.Id
                        }, provider);
                    }

                    conversation.IsContractAccepted = resultUser.Body.IsContractAccepted;
                }

                await context.SendActivity(Texts.GREETING);
            }
            else if (context.Activity.Type == ActivityTypes.Message)
            {
                var conversation = ConversationState<ConversationInfo>.Get(context);

                if (conversation.IsContractAccepted)
                {
                    await next().ConfigureAwait(false);
                    return;
                }

                var result = await ManagerScope.Process(context, new GetUserByChannelIdDto()
                {
                    ChannelAccountId = context.Activity.From.Id
                }, provider);

                if (!result.IsOk)
                {
                    //await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, result.Message, result.StackTrace), provider);
                    await context.SendActivity(Texts.ERROR_GET_USER);
                    return;
                }

                var resultUser = result as Result<UserInfo>;

                if (resultUser.Body == null)
                {
                    var newUserInfo = new UserInfo()
                    {
                        ChannelAccountId = context.Activity.From.Id,
                        ServiceUrl = context.Activity.ServiceUrl,
                        FullName = context.Activity.From.Name,
                        ConversationId = context.Activity.Conversation.Id
                    };

                    var urlfacebook = $"https://graph.facebook.com/{context.Activity.From.Id}?fields=first_name,last_name,profile_pic&access_token={this.configuration.GetValue<string>("FaceboookToken")}";
                    var response = await Connection.Process(new ConnectorRestGetString<FacebookUserResponse>(HttpClientFactory.Create(), urlfacebook));
                    if (!response.IsOk)
                    {
                        //await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, response.Message, "WelcomeMIddlware.OnTurn__Connection.Process(new ConnectorRestGetString<FacebookUserResponse>(urlfacebook))"), provider);
                    }
                    else
                    {
                        var facebookUserData = response as Response<FacebookUserResponse>;
                        newUserInfo.FirstName = facebookUserData.Body.FirstName;
                        newUserInfo.LastName = facebookUserData.Body.LastName;
                        newUserInfo.Gender = facebookUserData.Body.Gender;
                        newUserInfo.Picture = facebookUserData.Body.ProfilePic;
                    }

                    var resultSaveUser = await ManagerScope.Process(context, newUserInfo, provider);
                    if (!resultSaveUser.IsOk)
                    {
                        //await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, resultSaveUser.Message, resultSaveUser.StackTrace), provider);
                        await context.SendActivity(Texts.ERROR_SAVE_USER);
                    }

                    conversation.IsContractAccepted = false;
                }
                else
                {
                    if (resultUser.Body.ConversationId != context.Activity.Conversation.Id)
                    {
                        var resultUpdateConversationId = await ManagerScope.Process(context, new UpdateConversationIdDto()
                        {
                            ApplicationName = this.configuration.GetValue<string>("AppName"),
                            ConversationId = context.Activity.Conversation.Id,
                            ChannelAccountId = context.Activity.From.Id
                        }, provider);
                    }

                    conversation.IsContractAccepted = resultUser.Body.IsContractAccepted;
                }

                await next().ConfigureAwait(false);
            }
        }
    }
}
