﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace CarlitosBnb.Middlewares
{
    public class RetryValidatePromptMiddleware : IMiddleware
    {
        private readonly IConfiguration configuration;

        public RetryValidatePromptMiddleware(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task OnTurn(ITurnContext context, MiddlewareSet.NextDelegate next)
        {
            var conversationInfo = ConversationState<ConversationInfo>.Get(context);
            if (conversationInfo.CounterAttemptsOtpCode >= configuration.GetValue<int>("MaxTryCounterOtpCode"))
            {
                //var dc = new Microsoft.Bot.Builder.Dialogs.DialogContext()
                await next();
            }

            conversationInfo.CounterAttemptsOtpCode++;
            await context.SendActivity("Incrementando otpcodeattempts");
        }
    }
}
