﻿namespace CarlitosBnb.Middlewares.DialogFlow
{
    using Google.Cloud.Dialogflow.V2;
    using Google.Protobuf;

    class StreamDetectIntentRequestBuilder : BaseDetectIntentRequestBuilder
    {
        private readonly byte[] stream;
        private readonly AudioEncoding audioEncoding;
        private readonly int sampleRateHertz;

        public StreamDetectIntentRequestBuilder(byte[] stream, AudioEncoding audioEncoding, int sampleRateHertz)
        {
            this.stream = stream;
            this.audioEncoding = audioEncoding;
            this.sampleRateHertz = sampleRateHertz;
        }

        protected override void filDetectIntentRequest(DetectIntentRequest detector, string languageCode)
        {
            detector.InputAudio = ByteString.CopyFrom(this.stream);
            detector.QueryInput = new QueryInput()
            {
                AudioConfig = new InputAudioConfig()
                {
                    AudioEncoding = this.audioEncoding,
                    LanguageCode = languageCode,
                    //SampleRateHertz = this.sampleRateHertz
                }
            };
        }
    }
}
