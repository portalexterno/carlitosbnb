﻿namespace CarlitosBnb.Middlewares.DialogFlow
{
    using AudioConverter;
    //using BusinessLogic.Modules.LogModule;
    using BusinessLogic.Modules.UnknownIntentModule;
    using CarlitosBnb.Core;
    using global::Core.Connectors;
    using global::Core.Connectors.Core;
    using Google.Cloud.Dialogflow.V2;
    using Microsoft.Bot.Builder;
    using Microsoft.Bot.Builder.Core.Extensions;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Schema;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    //using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    public class DialogFlowRecognizerMiddleware : IMiddleware
    {
        public DialogFlowRecognizerMiddleware(INaturalLanguageUnderstanding nlu, IConfiguration configuration, IServiceProvider provider)
        {
            this.nlu = nlu;
            this.configuration = configuration;
            this.provider = provider;
        }

        public const string DialogFlowRecognizerResultKey = "DialogFlowRecognizerResult";

        //public const string PATTERN_DOCUMENT_AND_COMPLEMENT = @"^([0-9]{5,9})(-([a-zA-Z0-9]{1,4}))$";

        private INaturalLanguageUnderstanding nlu;

        private IConfiguration configuration;

        private IServiceProvider provider;


        private readonly string[] formats = { @"video/mp4", @"audio/aac", @"audio/vnd.dlna.adts" };

        public async Task OnTurn(ITurnContext context, MiddlewareSet.NextDelegate next)
        {
            BotAssert.ContextNotNull(context);
            IDetectIntentRequestBuilder detectIntent;

            var messagePlace = context.Activity.AsMessageActivity()?.Entities?.FirstOrDefault(t => t.Type == "Place");

            //await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, JsonConvert.SerializeObject(context.Activity), "DialogFlowRecognizerMiddleware_OnTurn"), provider);
            //if (context.Activity.Type == ActivityTypes.Message && context.Responded && messagePlace == null)
            if (context.Activity.Type == ActivityTypes.Message && messagePlace == null)
            {
                var conversation = ConversationState<ConversationInfo>.Get(context);

                if (conversation.IsAudio)
                {
                    var attachment = context.Activity.Attachments.GetOneHasAnyFormats(this.formats);

                    var response = await Connection.Process(new ConnetorRestGetStream(HttpClientFactory.Create(), attachment.ContentUrl)) as Response<Stream>;
                    Stream convertAudio = null;
                    var convertState = AudioConverter.Convert(response.Body, "mp4", "flac", configuration.GetValue<string>("PathToFfmpeg"), out convertAudio);

                    if (!convertState.IsOk)
                    {
                        //await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, convertState.Message, "DialogFlowRecognizerMiddleware_OnTurn__AudioConverter.Convert"), provider);
                        await context.SendActivity("Perdón pero no pude entender el audio, por favor intenta nuevamente, si el problema persiste contacta al administrador también podría usar el teclado hasta que solucione este problema");
                        return;
                    }


                    detectIntent = new StreamDetectIntentRequestBuilder(convertAudio.ToByteArray(),
                        AudioEncoding.Flac,
                        this.configuration.GetValue<int>("NLU:DialogFlow:SampleRateHertz"));
                }
                else
                {
                    detectIntent = new TextDetectIntentRequestBuilder(string.IsNullOrWhiteSpace(context.Activity.AsMessageActivity().Text) ? context.Activity.Value.ToString() : context.Activity.AsMessageActivity().Text);
                }

                var queryResult = await nlu.DetectIntentAsync(detectIntent);

                //if (Regex.IsMatch(queryResult.QueryText, PATTERN_DOCUMENT_AND_COMPLEMENT))
                //{
                //    queryResult.Action = "";
                //    queryResult.AllRequiredParamsPresent = false;
                //    queryResult.DiagnosticInfo = null;
                //    queryResult.Intent = null;
                //    queryResult.IntentDetectionConfidence = 0;
                //    queryResult.Parameters = null;
                //}

                if (queryResult.Intent != null)
                {
                    conversation.CurrentIntent = queryResult.Intent.IntentName.IntentId;
                    conversation.NameIntent = queryResult.Intent.DisplayName;
                    conversation.DialogflowTextResponse = queryResult.FulfillmentText;
                }

                //if (!conversation.ExpectedResponse && queryResult.Intent == null)

                if ((!conversation.ContainsKey("dialogStack") || (conversation["dialogStack"] == null) || (conversation["dialogStack"] as List<DialogInstance>).Count <= 0) && queryResult.Intent == null)
                {
                    await ManagerScope.Process(context, new UnknownIntentInfo()
                    {
                        Sentence = queryResult.QueryText,
                        ChannelAccountId = context.Activity.From.Id
                    }, provider);
                }

                //conversation.HasUnExpectedResponse();
                context.Activity.Text = queryResult.QueryText;
                context.Services.Add(DialogFlowRecognizerResultKey, queryResult);
                //}

                //conversation.NotSkipDialogFlow();
            }

            await next().ConfigureAwait(false);
        }
    }
}
