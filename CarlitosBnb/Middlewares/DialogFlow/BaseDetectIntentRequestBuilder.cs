﻿namespace CarlitosBnb.Middlewares.DialogFlow
{
    using Google.Cloud.Dialogflow.V2;
    using System;

    abstract class BaseDetectIntentRequestBuilder : IDetectIntentRequestBuilder
    {
        public DetectIntentRequest Build(string projectId, string languageCode)
        {
            var detector = new DetectIntentRequest();
            detector.SessionAsSessionName = new SessionName(projectId, Guid.NewGuid().ToString());
            this.filDetectIntentRequest(detector, languageCode);
            return detector;
        }

        protected abstract void filDetectIntentRequest(DetectIntentRequest detector, string languageCode);
    }
}
