﻿namespace CarlitosBnb.Middlewares.DialogFlow
{
    using Google.Cloud.Dialogflow.V2;

    class TextDetectIntentRequestBuilder : BaseDetectIntentRequestBuilder
    {
        private readonly string text;

        public TextDetectIntentRequestBuilder(string text)
        {
            this.text = text;
        }

        protected override void filDetectIntentRequest(DetectIntentRequest detector, string languageCode)
        {
            detector.QueryInput = new QueryInput()
            {
                Text = new TextInput()
                {
                    LanguageCode = languageCode,
                    Text = text
                }
            };
        }
    }
}
