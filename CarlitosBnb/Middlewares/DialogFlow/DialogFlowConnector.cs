﻿namespace CarlitosBnb.Middlewares.DialogFlow
{
    using Google.Apis.Auth.OAuth2;
    using Google.Apis.Services;
    using Google.Cloud.Dialogflow.V2;
    using Grpc.Auth;
    using Grpc.Core;
    using System.Threading.Tasks;

    public interface INaturalLanguageUnderstanding
    {
        Task<QueryResult> DetectIntentAsync(IDetectIntentRequestBuilder detectIntentRequestBuilder);

        QueryResult DetectIntent(IDetectIntentRequestBuilder detectIntentRequestBuilder);
    }

    public class DialogFlowConnector : INaturalLanguageUnderstanding
    {
        private Channel channel;
        private SessionsClient client;
        private readonly string projectId;
        private readonly string languageCode;

        public DialogFlowConnector(string projectId, string file, string languageCode)
        {
            var service = new BaseClientService.Initializer();
            var credential = GoogleCredential.FromFile(file);
            this.channel = new Channel(SessionsClient.DefaultEndpoint.Host, credential.ToChannelCredentials());
            this.client = SessionsClient.Create(channel);
            this.projectId = projectId;
            this.languageCode = languageCode;
        }

        private async Task<QueryResult> Connect(IDetectIntentRequestBuilder detectIntentRequestBuilder)
        {
            var response = await this.client.DetectIntentAsync(detectIntentRequestBuilder.Build(this.projectId, this.languageCode));
            return response.QueryResult;
        }

        //public static Task<QueryResult> DetectIntentText(string text, string projectId, string file, string languageCode)
        //{
        //    return new DialogFlowConnector(projectId, file, languageCode).Connect(new TextDetectIntentRequestBuilder(text));
        //}

        //public static Task<QueryResult> DetectIntentStream(byte[] stream, string projectId, string file, string languageCode)
        //{
        //    return new DialogFlowConnector(projectId, file, languageCode).Connect(new StreamDetectIntentRequestBuilder(stream, AudioEncoding.Flac, 16000));
        //}

        public Task<QueryResult> DetectIntentAsync(IDetectIntentRequestBuilder detectIntentRequestBuilder)
        {
            return this.Connect(detectIntentRequestBuilder);
        }

        public QueryResult DetectIntent(IDetectIntentRequestBuilder detectIntentRequestBuilder)
        {
            return this.Connect(detectIntentRequestBuilder).Result;
        }
    }

    //public class DialogFlowConnector : INaturalLanguageUnderstanding
    //{
    //    private const string ACTION_ERROR = "Error";

    //    //public async Task<QueryResult> DetectIntent(ICreatorDetectIntentRequest creatorDetectIntentRequest)
    //    public QueryResult DetectIntent(ICreatorDetectIntentRequest creatorDetectIntentRequest)
    //    {
    //        try
    //        {
    //            //var client = await SessionsClient.CreateAsync();
    //            //var request = creatorDetectIntentRequest.Create;
    //            //var response = await client.DetectIntentAsync(request);

    //            var client = SessionsClient.Create();
    //            var request = creatorDetectIntentRequest.Create;
    //            var response = client.DetectIntent(request);                
    //            return response.QueryResult;
    //        }
    //        catch (Exception e)
    //        {
    //            return new QueryResult()
    //            {
    //                Action = ACTION_ERROR,
    //                FulfillmentText = e.Message
    //            };
    //        }
    //    }

    //    public Task<QueryResult> DetectIntentAsync(ICreatorDetectIntentRequest creatorDetectIntentRequest)
    //    {
    //        throw new NotImplementedException();
    //    }

    //}
}
