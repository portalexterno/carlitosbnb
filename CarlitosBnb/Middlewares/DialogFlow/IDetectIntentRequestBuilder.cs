﻿namespace CarlitosBnb.Middlewares.DialogFlow
{
    using Google.Cloud.Dialogflow.V2;

    public interface IDetectIntentRequestBuilder
    {
        DetectIntentRequest Build(string projectId, string languageCode);
    }
}
