﻿//using BusinessLogic.Modules.LogModule;
using CarlitosBnb.Core;
using CarlitosBnb.SeedWorks;
using Core.Connectors;
using Core.Connectors.Core;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarlitosBnb.Middlewares.QueryBI
{
    public class QueryBiMiddleware : IMiddleware
    {
        private readonly string QueryGeneralUrl;
        private readonly string QuerySpecificUrl;
        private readonly IConfiguration configuration;
        private readonly IServiceProvider serviceProvider;

        public QueryBiMiddleware(IConfiguration configuration, IServiceProvider serviceProvider)
        {
            this.QueryGeneralUrl = configuration.GetValue<string>("Services:RegisterQueryGeneral");
            this.QuerySpecificUrl = configuration.GetValue<string>("Services:RegisterQuerySpecific");
            this.configuration = configuration;
            this.serviceProvider = serviceProvider;
        }

        public async Task OnTurn(ITurnContext context, MiddlewareSet.NextDelegate next)
        {
            var conversation = ConversationState<ConversationInfo>.Get(context);
            if (context.Activity.Type == ActivityTypes.Message)
            {
                if (context.Responded && !configuration.GetValue<bool>("Services:NotUseRegisterQueryBi"))
                {
                    var request = new QueryGeneralDto()
                    {
                        User = context.Activity.From.Id,
                        IntentName = conversation.NameIntent,
                        NameUser = context.Activity.From.Name
                    };

                    var responseQueryGeneral = await Connection.Process(new ConnectorRestPost<QueryGeneralDto, BaseApiResponse>(HttpClientFactory.Create(), this.QueryGeneralUrl, request));

                    //if (!responseQueryGeneral.IsOk)
                    //{
                        //await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, $"{nameof(QueryBiMiddleware)} Url: {this.QueryGeneralUrl} Message: {responseQueryGeneral.Message} Request: {JsonConvert.SerializeObject(request)}"), this.serviceProvider);
                    //}

                    var responseBaseApi = responseQueryGeneral as Response<BaseApiResponse>;

                    if (responseBaseApi != null && !responseBaseApi.Body.Success)
                    {
                        //await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, $"{nameof(QueryBiMiddleware)} Url: {this.QueryGeneralUrl} Message: {responseBaseApi.Message} Request: {JsonConvert.SerializeObject(request)}"), this.serviceProvider);
                    }
                }

                conversation.IsCompletedTask = false;

                await next();

                if (conversation.IsCompletedTask)
                {
                    if (!configuration.GetValue<bool>("Dev:NotUseRegisterQueryBi"))
                    {
                        var request = new QuerySpecificDto()
                        {
                            User = context.Activity.From.Id,
                            IntentName = conversation.NameIntent,
                            Document = conversation.ProfileSession.DocumentNumberLong,
                            Action = conversation.ProfileSession?.Action,
                            NameUser = context.Activity.From.Name
                        };

                        var responseQuerySpecific = await Connection.Process(new ConnectorRestPost<QuerySpecificDto, BaseApiResponse>(HttpClientFactory.Create(), this.QuerySpecificUrl, request));

                        //if (!responseQuerySpecific.IsOk)
                        //{
                        //    await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, $"{nameof(QueryBiMiddleware)} Url: {this.QuerySpecificUrl} Message: {responseQuerySpecific.Message} Request: {JsonConvert.SerializeObject(request)}"), this.serviceProvider);
                        //}

                        var responseBaseApiSpecific = responseQuerySpecific as Response<BaseApiResponse>;

                        //if (!responseBaseApiSpecific.Body.Success)
                        //{
                        //    await ManagerScope.Process(context, LogInfo.Load(context.Activity.From.Id, $"{nameof(QueryBiMiddleware)} Url: {this.QuerySpecificUrl} Message: {responseQuerySpecific.Message} Request: {JsonConvert.SerializeObject(request)}"), this.serviceProvider);
                        //}
                    }
                }
            }
        }
    }

    public class QueryGeneralDto
    {
        public string User { get; set; }

        public string IntentName { get; set; }

        public string NameUser { get; set; }
    }

    public class QuerySpecificDto
    {
        public string User { get; set; }

        public long Document { get; set; }

        public string IntentName { get; set; }

        public string Action { get; set; }

        public string NameUser { get; set; }
    }
}
