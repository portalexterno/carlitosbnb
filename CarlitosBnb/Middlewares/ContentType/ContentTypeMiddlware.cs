﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Core.Extensions;
using Microsoft.Bot.Schema;
using System.Threading.Tasks;

namespace CarlitosBnb.Middlewares.ContentType
{
    public class ContentTypeMiddlware : IMiddleware
    {
        public static readonly string[] formatsAudio = { @"video/mp4", @"audio/aac", @"audio/vnd.dlna.adts" };

        public static readonly string[] formatsImage = { @"image/png", @"image/jpeg", @"image/gif" };

        public async Task OnTurn(ITurnContext context, MiddlewareSet.NextDelegate next)
        {
            if (context.Activity.Type == ActivityTypes.Message)
            {
                var conversation = ConversationState<ConversationInfo>.Get(context);
                if (context.Activity.Attachments != null && context.Activity.Attachments.Count > 0 && context.Activity.Attachments.HasAnyFormat(formatsImage))
                {
                    conversation.HasImage();
                    await context.SendActivity("👍");
                    return;
                }

                if (context.Activity.Attachments != null && context.Activity.Attachments.Count > 0 && context.Activity.Attachments.HasAnyFormat(formatsAudio))
                {
                    conversation.HasAudio();
                }
                else if (context.Activity.Attachments != null && context.Activity.Attachments.Count > 0)
                {
                    await context.SendActivity("Que archivo tan raro, aún no se como interpretarlo");
                    return;
                }
                else
                {
                    conversation.HasMessage();
                }
            }

            await next().ConfigureAwait(false);
        }
    }
}
