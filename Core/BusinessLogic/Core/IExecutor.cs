﻿namespace Core.BusinessLogic.Core
{
    using System.Threading.Tasks;

    public interface IExecutor<Entity>
    {
        Task<Entity> Execute(Entity entity);
    }
}
