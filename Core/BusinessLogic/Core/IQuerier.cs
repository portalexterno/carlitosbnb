﻿namespace Core.BusinessLogic.Core
{
    using System.Threading.Tasks;

    public interface IQuerier<Parameter, Result>
    {
        Task<Result> Query(Parameter parameter);
    }
}
