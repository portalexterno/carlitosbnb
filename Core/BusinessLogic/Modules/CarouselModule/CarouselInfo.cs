﻿namespace BusinessLogic.Modules.CarouselModule
{
    using BusinessLogic.Modules.CardModule;
    using System.Collections.Generic;

    public class CarouselInfo
    {
        public string Name { get; set; }

        public string Text { get; set; }

        public string Intent { get; set; }

        public List<CardInfo> Cards { get; set; }
    }
}