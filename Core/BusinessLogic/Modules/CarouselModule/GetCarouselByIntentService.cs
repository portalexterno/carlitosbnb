﻿namespace BusinessLogic.Modules.CarouselModule
{
    using AutoMapper;
    using Database.Core;
    using System.Threading.Tasks;

    public class GetCarouselByIntentService : QuerierService<GetCarouseldDto, CarouselInfo>
    {
        public GetCarouselByIntentService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        protected override async Task<Result> querier(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var carousel = await this.unitOfWork.Carousel.GetByIntent(this.Data.Intent);
            return Result<CarouselInfo>.Ok(this.mapper.Map<CarouselInfo>(carousel));
        }
    }
}
