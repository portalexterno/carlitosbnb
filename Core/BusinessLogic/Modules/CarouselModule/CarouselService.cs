﻿namespace BusinessLogic.Modules.CarouselModule
{
    using System;
    using System.Threading.Tasks;
    using AutoMapper;
    using Database.Core;
    using Database.Core.Entities;

    public class CarouselService : Service, ICarouselService
    {
        public CarouselService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        public IUnitOfWork GetContext => this.unitOfWork;
        public Task CreateUser(CarouselInfo carousel)
        {
            throw new NotImplementedException();
        }

        public async Task<CarouselInfo> GetByIntent(GetCarouseldDto dto)
        {
            var carousel = await this.unitOfWork.Carousel.GetByIntent(dto.Intent);
            return this.mapper.Map<Carousel, CarouselInfo>(carousel);
        }
    }
}
