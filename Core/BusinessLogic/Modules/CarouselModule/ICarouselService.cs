﻿namespace BusinessLogic.Modules.CarouselModule
{
    using System;
    using System.Threading.Tasks;

    public interface ICarouselService : IService
    {
        Task<CarouselInfo> GetByIntent(GetCarouseldDto dto);

        Task CreateUser(CarouselInfo carousel);
    }
}
