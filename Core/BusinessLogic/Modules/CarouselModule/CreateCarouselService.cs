﻿namespace BusinessLogic.Modules.CarouselModule
{
    using AutoMapper;
    using Database.Core;
    using Database.Core.Entities;
    using System.Threading.Tasks;

    public class CreateCarouselService : ExecutorService<CarouselInfo>
    {
        public CreateCarouselService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        protected override async Task execute(IUnitOfWork unitOfWork, IMapper mapper)
        {
            await unitOfWork.Carousel.AddAsync(mapper.Map<Carousel>(this.Data));
        }
    }
}
