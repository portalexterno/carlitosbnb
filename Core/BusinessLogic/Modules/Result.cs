﻿namespace BusinessLogic.Modules
{
    public class Result
    {
        protected Result()
        {
            this.IsOk = false;
            this.Message = string.Empty;
        }

        protected Result(bool isOk, string message = null, string stackTrace = null)
        {
            this.IsOk = isOk;
            this.Message = message;
            this.StackTrace = stackTrace;
        }

        public bool IsOk { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public static Result Ok()
        {
            return new Result(true);
        }

        public static Result Fail(string message, string stackTrace = null)
        {
            return new Result(false, message, stackTrace);
        }

        public static Result Ok<BODY>(BODY body)
        {
            return new Result<BODY>()
            {
                IsOk = true,
                Message = null,
                Body = body
            };
        }
    }

    public class Result<BODY> : Result
    //where BODY : class
    {
        public Result()
        {
        }

        public BODY Body { get; set; }
    }
}
