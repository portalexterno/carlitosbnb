﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BusinessLogic.Modules.AgencyModule
{
    public class LocationAtmResponse
    {
        [JsonProperty("atm")]
        public IReadOnlyList<Atm> Atms { get; set; }
    }

    public class Atm
    {
        public string Description { get; set; }

        public string AttentionSchedule { get; set; }

        public decimal Longitude { get; set; }

        public decimal Latitude { get; set; }

        public string ImageUrl { get; set; }
    }
}
