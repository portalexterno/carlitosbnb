﻿namespace BusinessLogic.Modules.AgencyModule
{
    using AutoMapper;
    using Database.Core;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class GetAgenciesByDepartamentService : QuerierService<AgenciesByDepartamentDto, List<AgencyInfo>>
    {
        public GetAgenciesByDepartamentService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        protected override async Task<Result> querier(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var agencies = await unitOfWork.Agencies.GetAgenciesByDepartament(this.Data.Departament);

            return Result<List<AgencyInfo>>.Ok(mapper.Map<List<AgencyInfo>>(agencies));
        }
    }
}
