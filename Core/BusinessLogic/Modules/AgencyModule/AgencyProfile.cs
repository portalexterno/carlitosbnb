﻿namespace BusinessLogic.Modules.AgencyModule
{
    using AutoMapper;

    public class AgencyProfile : Profile
    {
        public AgencyProfile()
        {
            CreateMap<Agency, AgencyInfo>();
        }
    }
}
