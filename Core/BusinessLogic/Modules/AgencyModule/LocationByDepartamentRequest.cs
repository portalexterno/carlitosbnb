﻿namespace BusinessLogic.Modules.AgencyModule
{
    public class LocationByDepartamentRequest
    {
        public int DepartmentId { get; set; }
    }
}
