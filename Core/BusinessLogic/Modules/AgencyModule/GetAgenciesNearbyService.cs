﻿namespace BusinessLogic.Modules.AgencyModule
{
    using AutoMapper;
    using Database.Core;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class GetAgenciesNearbyService : QuerierService<AgencyDto, List<AgencyInfo>>
    {
        public GetAgenciesNearbyService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        protected override async Task<Result> querier(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var agencies = await this.unitOfWork.Agencies.GetAgenciesNearby(this.Data.Latitude, this.Data.Longitude);

            if (agencies.Count() <= 0)
            {
                return Result.Fail("No se hallaron elementos");
            }

            return Result<List<AgencyInfo>>.Ok(this.mapper.Map<List<AgencyInfo>>(agencies));
        }
    }
}
