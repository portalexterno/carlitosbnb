﻿namespace BusinessLogic.Modules.AgencyModule
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using Database.Core;
    using Microsoft.EntityFrameworkCore;

    public class AgencyService : Service, IAgencyService
    {
        public AgencyService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        public IUnitOfWork GetContext => this.unitOfWork;

        public async Task<List<AgencyInfo>> GetAgenciesByDepartament(AgenciesByDepartamentDto dto)
        {
            var r = await this.unitOfWork.Agencies.GetAgenciesByDepartament(dto.Departament);
            return this.mapper.Map<List<AgencyInfo>>(r);
        }

        public async Task<List<AgencyInfo>> GetAgenciesNearby(AgencyDto dto)
        {
            var r = await this.unitOfWork.Agencies.GetAgenciesNearby(dto.Latitude, dto.Longitude);
            return this.mapper.Map<List<AgencyInfo>>(r);
        }

        public async Task<List<AgencyInfo>> GetAtmsByDepartament(AgenciesByDepartamentDto dto)
        {
            var r = await this.unitOfWork.Agencies.GetAtmsByDepartament(dto.Departament);
            return this.mapper.Map<List<AgencyInfo>>(r);
        }

        public async Task<List<AgencyInfo>> GetAtmsNearby(AgencyDto dto)
        {
            var r = await this.unitOfWork.Agencies.GetAtmsNearby(dto.Latitude, dto.Longitude);
            return this.mapper.Map<List<AgencyInfo>>(r);
        }
    }
}
