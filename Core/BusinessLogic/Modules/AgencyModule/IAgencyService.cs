﻿namespace BusinessLogic.Modules.AgencyModule
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAgencyService : IService    
    {
        Task<List<AgencyInfo>> GetAgenciesNearby(AgencyDto dto);

        Task<List<AgencyInfo>> GetAgenciesByDepartament(AgenciesByDepartamentDto dto);

        Task<List<AgencyInfo>> GetAtmsNearby(AgencyDto dto);

        Task<List<AgencyInfo>> GetAtmsByDepartament(AgenciesByDepartamentDto dto);
    }
}
