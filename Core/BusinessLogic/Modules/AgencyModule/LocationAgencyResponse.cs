﻿namespace BusinessLogic.Modules.AgencyModule
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class LocationAgencyResponse
    {
        [JsonProperty("agency")]
        public IReadOnlyList<Agency> Agencies { get; set; }
    }

    public class Agency
    {
        public string Description { get; set; }

        public string AttentionSchedule { get; set; }

        public decimal Longitude { get; set; }

        public decimal Latitude { get; set; }

        public string ImageUrl { get; set; }
    }
}
