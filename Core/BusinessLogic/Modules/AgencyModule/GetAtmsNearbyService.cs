﻿namespace BusinessLogic.Modules.AgencyModule
{
    using AutoMapper;
    using Database.Core;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class GetAtmsNearbyService : QuerierService<AtmDto, List<AgencyInfo>>
    {
        public GetAtmsNearbyService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        protected override async Task<Result> querier(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var atms = await this.unitOfWork.Agencies.GetAtmsNearby(this.Data.Latitude, this.Data.Longitude);

            if (atms.Count() <= 0)
            {
                return Result.Fail("No se hallaron elementos");
            }

            return Result<List<AgencyInfo>>.Ok(this.mapper.Map<List<AgencyInfo>>(atms));
        }
    }
}
