﻿namespace BusinessLogic.Modules.AgencyModule
{
    public class LocationRequest
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
