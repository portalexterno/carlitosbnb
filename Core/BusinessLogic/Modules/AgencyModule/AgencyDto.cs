﻿namespace BusinessLogic.Modules.AgencyModule
{
    public class AgencyDto
    {
        public string Latitude { get; set; }

        public string Longitude { get; set; }
    }

    public class AtmDto : AgencyDto { }
}
