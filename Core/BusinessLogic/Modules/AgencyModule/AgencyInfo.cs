﻿namespace BusinessLogic.Modules.AgencyModule
{
    public class AgencyInfo
    {
        public string Description { get; set; }

        public string Schedule { get; set; }

        public decimal Longitude { get; set; }

        public decimal Latitude { get; set; }

        public string UrlImage { get; set; }
    }
}
