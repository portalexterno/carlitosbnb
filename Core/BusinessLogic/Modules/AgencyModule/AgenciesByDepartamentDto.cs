﻿namespace BusinessLogic.Modules.AgencyModule
{
    public class AgenciesByDepartamentDto
    {
        public string Departament { get; set; }
    }

    public class AtmByDepartamentDto : AgenciesByDepartamentDto
    {
    }
}
