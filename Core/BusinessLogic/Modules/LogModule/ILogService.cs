﻿namespace BusinessLogic.Modules.LogModule
{
    using System;
    using System.Threading.Tasks;

    public interface ILogService : IService
    {
        Task Error(LogInfo logInfo);

        Task Info(LogInfo logInfo);

        Task Warn(LogInfo logInfo);
    }
}
