﻿namespace BusinessLogic.Modules.LogModule
{
    using AutoMapper;
    using Database.Core;
    using Database.Core.Entities;
    using System.Threading.Tasks;

    //public abstract class LogService : ExecutorService<LogInfo>
    //{
    //public struct LogTypes
    //{
    //    public const string ERROR = "ERROR";

    //    public const string INFORMATION = "INFO";

    //    public const string WARNING = "WARN";
    //}

    //public LogService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    //{
    //}

    //protected override async Task execute(IUnitOfWork unitOfWork, IMapper mapper)
    //{
    //var log = new Log();
    //log.Message = this.Data.Message;
    //log.StackTrace = this.Data.StackTrace;
    //log.Type = this.Type;

    //log.ChannelAccountId = this.Data.ChannelId;
    //await this.unitOfWork.Logs.AddAsync(log);
    //}

    //protected abstract string Type { get; }
    //}

    //public class LogErrorService : LogService
    //{
    //    public LogErrorService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    //    {
    //    }

    //    protected override string Type => LogTypes.ERROR;
    //}

    //public class LogInfoService : LogService
    //{
    //    public LogInfoService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    //    {
    //    }

    //    protected override string Type => LogTypes.INFORMATION;
    //}

    //public class LogWarnService : LogService
    //{
    //    public LogWarnService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    //    {
    //    }

    //    protected override string Type => LogTypes.WARNING;
    //}
}
