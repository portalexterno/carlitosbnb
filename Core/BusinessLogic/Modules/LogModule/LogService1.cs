﻿namespace BusinessLogic.Modules.LogModule
{
    using System.Threading.Tasks;
    using AutoMapper;
    using Database.Core;
    using Database.Core.Entities;

    public class LogService1 : Service, ILogService
    {
        public struct LogTypes
        {
            public const string ERROR = "ERROR";

            public const string INFORMATION = "INFO";

            public const string WARNING = "WARN";
        }

        public IUnitOfWork GetContext => this.unitOfWork;
        public LogService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        public async Task Error(LogInfo logInfo)
        {
            var log = new Log();
            log.Message = logInfo.Message;
            log.StackTrace = logInfo.StackTrace;
            log.Type = LogTypes.ERROR;
            //log.User = await this.unitOfWork.Users.GetByFacebookId(logInfo.FacebookId);
            log.ChannelAccountId = logInfo.ChannelId;
            await this.unitOfWork.Logs.AddAsync(log);
            await this.unitOfWork.CompleteAsync();
        }

        public async Task Info(LogInfo logInfo)
        {
            var log = new Log();
            log.Message = logInfo.Message;
            log.StackTrace = logInfo.StackTrace;
            log.Type = LogTypes.INFORMATION;
            //log.User = await this.unitOfWork.Users.GetByFacebookId(logInfo.ChannelId);
            log.ChannelAccountId = logInfo.ChannelId;
            await this.unitOfWork.Logs.AddAsync(log);
            await this.unitOfWork.CompleteAsync();
        }

        public async Task Warn(LogInfo logInfo)
        {
            var log = new Log();
            log.Message = logInfo.Message;
            log.StackTrace = logInfo.StackTrace;
            log.Type = LogTypes.WARNING;
            //log.User = await this.unitOfWork.Users.GetByFacebookId(logInfo.ChannelId);
            log.ChannelAccountId = logInfo.ChannelId;
            await this.unitOfWork.Logs.AddAsync(log);
            await this.unitOfWork.CompleteAsync();
        }
    }
}
