﻿namespace BusinessLogic.Modules.ApplicationModule
{
    public class GetApplicationByNameDto
    {
        public string Name { get; set; }
    }
}
