﻿namespace BusinessLogic.Modules.ApplicationModule
{
    using AutoMapper;
    using Database.Core;
    using System.Threading.Tasks;

    public class GetApplicationByNameService : QuerierService<GetApplicationByNameDto, ApplicationInfo>
    {
        public GetApplicationByNameService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        protected override async Task<Result> querier(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var application = await unitOfWork.Applications.GetApplicationByName(this.Data.Name);
            return Result<ApplicationInfo>.Ok(mapper.Map<ApplicationInfo>(application));
        }
    }
}
