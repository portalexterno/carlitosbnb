﻿namespace BusinessLogic.Modules.UserModule
{
    public class UpdateConversationIdDto
    {
        public string ApplicationName { get; set; }

        public string ChannelAccountId { get; set; }

        public string ConversationId { get; set; }
    }
}
