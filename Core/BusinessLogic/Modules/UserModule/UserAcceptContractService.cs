﻿namespace BusinessLogic.Modules.UserModule
{
    using AutoMapper;
    using Database.Core;
    using System.Threading.Tasks;

    public class UserAcceptContractService : ExecutorService<UserAcceptContractDto>
    {
        public UserAcceptContractService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        { }

        protected override async Task execute(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var user = await unitOfWork.Users.GetByChannelId(this.Data.ChannelAccountId);
            user.IsContractAccepted = true;
            unitOfWork.Users.Update(user);
        }
    }
}
