﻿namespace BusinessLogic.Modules.UserModule
{
    using AutoMapper;
    using Core.BusinessLogic.Core;
    using Database.Core.Repositories;
    using System.Threading.Tasks;

    public class GetUserByFacebookIdQuerier : IQuerier<GetUserByFacebookIdDto, UserInfo>
    {
        private GetUserByFacebookIdQuerier(IUserRepository users, IMapper mapper)
        {
            this.users = users;
            this.mapper = mapper;
        }

        private readonly IUserRepository users;

        private readonly IMapper mapper;

        public async Task<UserInfo> Query(GetUserByFacebookIdDto dto)
        {
            var user = await this.users.GetByFacebookId(dto.ChannelAccountId);
            return this.mapper.Map<UserInfo>(user);
        }

        public static async Task<UserInfo> Query(GetUserByFacebookIdDto dto, IUserRepository users, IMapper mapper)
        {
            return await new GetUserByFacebookIdQuerier(users, mapper).Query(dto);
        }
    }
}
