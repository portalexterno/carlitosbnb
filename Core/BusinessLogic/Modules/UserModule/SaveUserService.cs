﻿namespace BusinessLogic.Modules.UserModule
{
    using AutoMapper;
    using Database.Core;
    using Database.Core.Entities;
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class SaveUserService : ExecutorService<UserInfo>
    {
        private IConfiguration configuration;

        public SaveUserService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration) : base(unitOfWork, mapper)
        {
            this.configuration = configuration;
        }

        protected override async Task execute(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var name = this.configuration.GetValue<string>("AppName");
            var application = await this.unitOfWork.Applications.GetApplicationByName(name);
            if (application == null)
            {
                throw new IndexOutOfRangeException($"No se encontró ninguna aplicación registrada con el nombre {name}");
            }

            var newUser = mapper.Map<User>(this.Data);
            newUser.ApplicationUser = new List<ApplicationUser>()
                {
                    new ApplicationUser()
                    {
                        ApplicationId = application.Id,
                        ChannelAccountId = this.Data.ChannelAccountId,
                        ConversationId = this.Data.ConversationId
                    }
                };

            await this.unitOfWork.Users.AddAsync(newUser);
        }
    }
}
