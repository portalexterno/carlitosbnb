﻿namespace BusinessLogic.Modules.UserModule
{
    public class UserInfo
    {
        public string ChannelAccountId { get; set; }

        public string ConversationId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Gender { get; set; }

        public string Birthday { get; set; }

        public string Picture { get; set; }

        public string Email { get; set; }

        public string HomeTown { get; set; }

        public string ServiceUrl { get; set; }

        public bool IsContractAccepted { get; set; }
    }

    public class ApplicationUserInfo
    {
        public int ApplicationId { get; set; }

        //public Application Application { get; set; }

        public int UserId { get; set; }

        //public User User { get; set; }

        public string ChannelId { get; set; }
    }
}
