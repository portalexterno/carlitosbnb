﻿namespace BusinessLogic.Modules.UserModule
{
    using AutoMapper;
    using Database.Core;
    using System.Threading.Tasks;

    public class UpdateConversationIdService : ExecutorService<UpdateConversationIdDto>
    {
        public UpdateConversationIdService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {

        }

        protected override async Task execute(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var applicationUser = await unitOfWork.ApplicationUsers.GetByChannelAccountId(this.Data.ChannelAccountId);
            applicationUser.ConversationId = this.Data.ConversationId;
            unitOfWork.ApplicationUsers.Update(applicationUser);
        }
    }
}
