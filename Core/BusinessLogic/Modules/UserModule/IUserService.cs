﻿namespace BusinessLogic.Modules.UserModule
{
    using System;
    using System.Threading.Tasks;

    public interface IUserService : IService
    {
        Task<UserInfo> GetByFacebookId(GetUserByFacebookIdDto facebookId);

        Task SaveUser(UserInfo user);

        Task AcceptContract(GetUserByFacebookIdDto facebookId);
    }
}
