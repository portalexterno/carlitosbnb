﻿namespace BusinessLogic.Modules.UserModule
{
    public class GetUserByChannelIdDto
    {
        public string ChannelAccountId { get; set; }
    }

    public class UserAcceptContractDto
    {
        public string ChannelAccountId { get; set; }
    }
}
