﻿namespace BusinessLogic.Modules.UserModule
{
    using AutoMapper;
    using Database.Core;
    using System.Linq;
    using System.Threading.Tasks;

    public class GetUserByChannelIdService : QuerierService<GetUserByChannelIdDto, UserInfo>
    {
        public GetUserByChannelIdService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        { }

        protected override async Task<Result> querier(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var user = await unitOfWork.Users.GetByChannelId(this.Data.ChannelAccountId);
            var userInfo = mapper.Map<UserInfo>(user);
            if (user != null)
                userInfo.ConversationId = user.ApplicationUser.FirstOrDefault(ap => ap.ChannelAccountId == this.Data.ChannelAccountId).ConversationId;

            var u = Result<UserInfo>.Ok(userInfo);

            return u;
        }
    }
}
