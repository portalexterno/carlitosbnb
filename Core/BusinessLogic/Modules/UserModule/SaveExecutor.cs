﻿namespace BusinessLogic.Modules.UserModule
{
    using Core.BusinessLogic.Core;
    using Database.Core.Entities;
    using Database.Core.Repositories;
    using System.Threading.Tasks;

    public class SaveExecutor : IExecutor<User>
    {
        public SaveExecutor(IUserRepository users)
        {
            this.users = users;
        }

        private readonly IUserRepository users;

        public async Task<User> Execute(User entity)
        {
            await this.users.AddAsync(entity);
            return entity;
        }
    }
}
