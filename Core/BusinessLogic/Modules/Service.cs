﻿namespace BusinessLogic.Modules
{
    using AutoMapper;
    using Database.Core;
    using System;
    using System.Threading.Tasks;

    public abstract class Service<Dto> : IService<Dto>
    {
        protected readonly IMapper mapper;

        protected readonly IUnitOfWork unitOfWork;

        public Service(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public Dto Data { get; set; }

        public async Task<Result> Process()
        {
            try
            {
                var p = await this.process(this.unitOfWork, this.mapper);
                return p;
            }
            catch (Exception e)
            {
                return Result.Fail(e.Message, e.StackTrace);
            }
        }

        protected abstract Task<Result> process(IUnitOfWork unitOfWork, IMapper mapper);
    }

    public abstract class ExecutorService<Dto> : Service<Dto>
    {
        public ExecutorService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        { }

        protected override async Task<Result> process(IUnitOfWork unitOfWork, IMapper mapper)
        {
            await this.execute(unitOfWork, mapper);
            var changes = await unitOfWork.CompleteAsync();
            if (changes <= 0)
            {
                return Result.Fail("No se afecto ningún registro", this.GetType().ToString());
            }

            return Result.Ok();
        }

        protected abstract Task execute(IUnitOfWork unitOfWork, IMapper mapper);
    }

    public abstract class QuerierService<Dto, Body> : Service<Dto>
        where Body : class
    {
        public QuerierService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        { }

        protected override async Task<Result> process(IUnitOfWork unitOfWork, IMapper mapper)
        {
            return await this.querier(unitOfWork, mapper);
        }

        protected abstract Task<Result> querier(IUnitOfWork unitOfWork, IMapper mapper);
    }
}
