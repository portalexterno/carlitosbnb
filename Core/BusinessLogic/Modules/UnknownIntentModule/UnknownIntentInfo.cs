﻿namespace BusinessLogic.Modules.UnknownIntentModule
{
    public class UnknownIntentInfo
    {
        public string Sentence { get; set; }

        public string ChannelAccountId { get; set; }
    }
}