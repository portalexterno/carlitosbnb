﻿namespace BusinessLogic.Modules.UnknownIntentModule
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using AutoMapper;
    using Database.Core;

    public class UnknownIntentService : Service, IUnknownIntentService
    {
        public UnknownIntentService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        //Eliminar
        public IUnitOfWork GetContext => this.unitOfWork;
        public async Task Register(UnknownIntentInfo unknownIntent)
        {
            await this.unitOfWork.UnknownIntentions.AddAsync(new Database.Core.Entities.UnknownIntention()
            {
                Sentence = unknownIntent.Sentence,
                User = await this.unitOfWork.Users.GetByFacebookId(unknownIntent.ChannelAccountId)
            });
        }
    }
}
