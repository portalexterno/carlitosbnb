﻿namespace BusinessLogic.Modules.UnknownIntentModule
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;

    public interface IUnknownIntentService : IService
    {
        Task Register(UnknownIntentInfo unknownIntent);
    }
}
