﻿namespace BusinessLogic.Modules.UnknownIntentModule
{
    using AutoMapper;
    using Database.Core;
    using System.Threading.Tasks;

    public class RegisterUnknownIntentService : ExecutorService<UnknownIntentInfo>
    {
        public RegisterUnknownIntentService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        protected override async Task execute(IUnitOfWork unitOfWork, IMapper mapper)
        {
            await unitOfWork.UnknownIntentions.AddAsync(new Database.Core.Entities.UnknownIntention()
            {
                Sentence = this.Data.Sentence,
                User = await this.unitOfWork.Users.GetByChannelId(this.Data.ChannelAccountId)
            });
        }
    }
}
