﻿namespace BusinessLogic.Modules
{
    using System;
    using System.Threading.Tasks;

    public interface IService<Dto>
    {
        Dto Data { get; set; }

        Task<Result> Process();
    }
}
