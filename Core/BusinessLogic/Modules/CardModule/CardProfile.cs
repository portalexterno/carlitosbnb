﻿namespace BusinessLogic.Modules.CardModule
{
    using AutoMapper;
    using Database.Core.Entities;

    public class CardProfile : Profile
    {
        public CardProfile()
        {
            AllowNullDestinationValues = true;
            CreateMap<Card, CardInfo>().ReverseMap();
            CreateMap<CardAction, CardActionInfo>().ReverseMap();
            CreateMap<CardMedia, CardMediaInfo>().ReverseMap();
        }
    }
}
