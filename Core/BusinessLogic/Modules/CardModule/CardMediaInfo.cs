﻿namespace BusinessLogic.Modules.CardModule
{
    public class CardMediaInfo
    {
        public string Url { get; set; }

        public CardActionInfo CardAction { get; set; }
    }
}
