﻿namespace BusinessLogic.Modules.CardModule
{
    using System.Threading.Tasks;
    using AutoMapper;
    using Database.Core;
    using Database.Core.Entities;

    public class CardService : Service, ICardService
    {
        public CardService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        public IUnitOfWork GetContext => this.unitOfWork;
        public async Task CreateCard(CardInfo cardInfo)
        {
            var card = this.mapper.Map<Card>(cardInfo);
            await this.unitOfWork.Cards.AddAsync(card);
            await this.unitOfWork.CompleteAsync();
        }

        public async Task<CardInfo> GetByIntent(GetCardDto dto)
        {
            var card = await this.unitOfWork.Cards.GetByIntent(dto.Intent);
            return this.mapper.Map<CardInfo>(card);
        }
    }
}
