﻿namespace BusinessLogic.Modules.CardModule
{
    using AutoMapper;
    using Database.Core;
    using System.Threading.Tasks;

    public class GetCardByIntentService : QuerierService<GetCardDto, CardInfo>
    {
        public GetCardByIntentService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        protected override async Task<Result> querier(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var card = await this.unitOfWork.Cards.GetByIntent(this.Data.Intent);
            return Result.Ok(this.mapper.Map<CardInfo>(card));
        }
    }
}
