﻿namespace BusinessLogic.Modules.CardModule
{
    using System;
    using System.Threading.Tasks;

    public interface ICardService : IService
    {
        Task<CardInfo> GetByIntent(GetCardDto dto);

        Task CreateCard(CardInfo card);
    }
}
