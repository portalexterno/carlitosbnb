﻿namespace BusinessLogic.Modules.CardModule
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class CardInfo
    {
        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("SubTitle")]
        public string SubTitle { get; set; }

        [JsonProperty("Text")]
        public string Text { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("Intent")]
        public string Intent { get; set; }

        [JsonProperty("Media")]
        public List<CardMediaInfo> Media { get; set; }

        [JsonProperty("Buttons")]
        public List<CardActionInfo> Buttons { get; set; }
    }
}
