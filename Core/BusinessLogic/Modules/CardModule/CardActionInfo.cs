﻿namespace BusinessLogic.Modules.CardModule
{
    public class CardActionInfo
    {
        public string Type { get; set; }

        public string Title { get; set; }

        public string Value { get; set; }

        public int Order { get; set; }
    }
}
