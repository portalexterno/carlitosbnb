﻿namespace BusinessLogic.Modules.CardModule
{
    using AutoMapper;
    using Database.Core;
    using Database.Core.Entities;
    using System.Threading.Tasks;

    public class CreateCardService : ExecutorService<CardInfo>
    {
        public CreateCardService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        protected override async Task execute(IUnitOfWork unitOfWork, IMapper mapper)
        {
            var card = this.mapper.Map<Card>(this.Data);
            await this.unitOfWork.Cards.AddAsync(card);
        }
    }
}
