﻿namespace Core.Connectors
{
    using Connectors.Core;
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;

    public abstract class ConnectorRest<RESULT> : IConnector
        where RESULT : class
    {
        public ConnectorRest(HttpClient httpClient, string url, int timeOutMinutes = 0)
        {
            this.client = httpClient;
            this.url = url;
            this.timeOutMinutes = timeOutMinutes;
        }

        private HttpClient client;

        private int timeOutMinutes;

        protected string url;

        public void Close()
        {
            this.client.Dispose();
        }

        public Response Open()
        {
            try
            {
                //this.client = new HttpClient(new HttpClientHandler()
                //{
                //    UseProxy = false
                //});

                if (this.timeOutMinutes > 0)
                    client.Timeout = TimeSpan.FromMinutes(this.timeOutMinutes);
            }
            catch (Exception e)
            {
                return Response.Fail(e.Message);
            }

            return Response.Ok();
        }

        public async Task<Response> Process()
        {
            //this.client.BaseAddress = new Uri("https://graph.facebook.com/");
            var httpResponse = await this.getResponse(this.client, this.url);

            var stateCall = httpResponse.IsSuccessStatusCode;

            if (stateCall)
            {
                var json = await this.readContent(httpResponse.Content);
                return Response<RESULT>.Ok(json);
            }

            var jsonError = await httpResponse.Content.ReadAsStringAsync();
            return Response.Fail($"{httpResponse.ReasonPhrase}. Detail error: {jsonError}");
        }

        protected abstract Task<HttpResponseMessage> getResponse(HttpClient client, string url);

        protected abstract Task<RESULT> readContent(HttpContent content);
    }
}
