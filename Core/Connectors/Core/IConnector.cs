﻿namespace Core.Connectors.Core
{
    using System.Threading.Tasks;

    public interface IConnector
    {
        Response Open();

        void Close();

        Task<Response> Process();
    }
}
