﻿namespace Core.Connectors.Core
{
    using System;
    using System.Threading.Tasks;

    public class Connection
    //<RESULT>
    //: IConnection
    //where RESULT : class
    {
        private Connection(IConnector connector)
        {
            this.connector = connector;
        }

        IConnector connector;

        //private Response<RESULT> response;

        private async Task<Response> process()
        {
            Response response = Response.Ok();
            var open = this.connector.Open();
            if (open.IsOk)
            {
                try
                {
                    response = await this.connector.Process();
                }
                catch (Exception e)
                {
                    response = Response.Fail(e.Message);
                }
            }
            else
            {
                response = Response.Fail(open.Message);
            }

            this.connector.Close();

            return response;
        }

        public static async Task<Response> Process(IConnector connector)
        {
            var connection = new Connection(connector);
            return await connection.process();
        }
    }
}
