﻿namespace Core.Connectors.Core
{
    using System.Threading.Tasks;

    public interface IConnection
    {
        Task<Response> Process();
    }
}
