﻿namespace Core.Connectors.Core
{
    public class Response
    {
        protected Response()
        {
            this.IsOk = false;
            this.Message = null;
        }

        public bool IsOk { get; protected set; }

        public string Message { get; protected set; }

        public static Response Ok()
        {
            return new Response()
            {
                IsOk = true
            };
        }

        public static Response Ok<BODY>(BODY body)
        {
            return new Response<BODY>()
            {
                IsOk = true,
                Body = body
            };
        }

        public static Response Fail(string message)
        {
            return new Response()
            {
                IsOk = false,
                Message = message
            };
        }
    }

    public class Response<BODY> : Response
    {
        public BODY Body { get; set; }


    }
}
