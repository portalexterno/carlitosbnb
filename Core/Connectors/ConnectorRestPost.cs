﻿namespace Core.Connectors
{
    using Newtonsoft.Json;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    public class ConnectorRestPost<REQUEST, RESULT> : ConnectorRest<RESULT>
        where RESULT : class
    {
        public ConnectorRestPost(HttpClient httpclient, string url, REQUEST request) : base(httpclient, url)
        {
            this.request = request;
        }

        private const string MEDIA_TYPE = "application/json";

        private REQUEST request;

        protected override async Task<HttpResponseMessage> getResponse(HttpClient client, string url)
        {
            var httpContent = new StringContent(JsonConvert.SerializeObject(this.request, Formatting.Indented), Encoding.UTF8, MEDIA_TYPE);
            return await client.PostAsync(url, httpContent);
        }

        protected override async Task<RESULT> readContent(HttpContent content)
        {
            var json = await content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<RESULT>(json);
            return result;
        }
    }
}
