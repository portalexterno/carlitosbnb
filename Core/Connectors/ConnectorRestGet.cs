﻿namespace Core.Connectors
{
    using Newtonsoft.Json;
    using System.IO;
    using System.Net.Http;
    using System.Threading.Tasks;

    public abstract class ConnectorRestGet<RESULT> : ConnectorRest<RESULT>
        where RESULT : class
    {
        public ConnectorRestGet(HttpClient httpclient, string url, int timeOutMinutes = 0) : base(httpclient, url, timeOutMinutes)
        {
        }

        protected override async Task<HttpResponseMessage> getResponse(HttpClient client, string url)
        {
            return await client.GetAsync(url);
        }
    }

    public class ConnectorRestGetString<RESULT> : ConnectorRestGet<RESULT>
        where RESULT : class
    {
        public ConnectorRestGetString(HttpClient httpclient, string url, int timeOutMinutes = 0) : base(httpclient, url, timeOutMinutes)
        {
        }

        protected override async Task<RESULT> readContent(HttpContent content)
        {
            var json = await content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<RESULT>(json);
            return result;
        }
    }

    public class ConnetorRestGetStream : ConnectorRestGet<Stream>
    {
        public ConnetorRestGetStream(HttpClient httpclient, string url, int timeOutMinutes = 0) : base(httpclient, url, timeOutMinutes)
        {
        }

        protected override async Task<Stream> readContent(HttpContent content)
        {
            return await content.ReadAsStreamAsync();
        }
    }
}
