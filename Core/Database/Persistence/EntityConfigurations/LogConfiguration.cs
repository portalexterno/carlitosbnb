﻿namespace Database.Persistence.EntityConfigurations
{
    using Database.Core.Entities;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class LogConfiguration : Configuration<Log>
    {
        protected override void configure(EntityTypeBuilder<Log> builder)
        {
            builder.Property(p => p.Type).HasMaxLength(10).IsRequired();
        }
    }
}
