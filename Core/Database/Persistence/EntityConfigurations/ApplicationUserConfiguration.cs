﻿namespace Database.Persistence.EntityConfigurations
{
    using Database.Core.Entities;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ApplicationUserConfiguration : Configuration<ApplicationUser>
    {
        protected override void configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.HasKey(p => new { p.ApplicationId, p.UserId });

            builder.Property(p => p.ChannelAccountId).IsRequired();
            builder.Property(p => p.ConversationId).IsRequired();

            builder
                .HasOne(p => p.Application)
                .WithMany(p => p.ApplicationUsers)
                .HasForeignKey(p => p.ApplicationId);

            builder
                .HasOne(p => p.User)
                .WithMany(p => p.ApplicationUser)
                .HasForeignKey(p => p.UserId);
        }
    }
}
