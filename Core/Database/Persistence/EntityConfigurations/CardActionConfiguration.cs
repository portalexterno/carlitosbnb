﻿namespace Database.Persistence.EntityConfigurations
{
    using Database.Core.Entities;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class CardActionConfiguration : Configuration<CardAction>
    {
        protected override void configure(EntityTypeBuilder<CardAction> builder)
        {
            builder.Property(p => p.Title).HasMaxLength(30);
            builder.Property(p => p.Type).HasMaxLength(30);
            builder.Property(p => p.Value).HasMaxLength(200);

            //builder
            //    .HasOne(p => p.Card)
            //    .WithMany(p => p.Buttons)
            //    .HasForeignKey(p => p.CardId);

            builder
                .HasOne(p => p.CardMedia)
                .WithOne(p => p.CardAction)
                .HasForeignKey<CardMedia>(p => p.CardActionId);

        }
    }
}
