﻿namespace Database.Persistence.EntityConfigurations
{
    using Database.Core.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public abstract class Configuration<Entity> : IEntityTypeConfiguration<Entity>
        where Entity : class
    {
        public void Configure(EntityTypeBuilder<Entity> builder)
        {
            if (typeof(IIdentity).IsAssignableFrom(typeof(Entity)))
            {
                //var builderIdentity = builder as EntityTypeBuilder<IIdentity>;
                builder.HasKey(p => ((IIdentity)p).Id);
                builder.Property(p => ((IIdentity)p).Id).ValueGeneratedOnAdd();
            }

            if (typeof(ICreatedAt).IsAssignableFrom(typeof(Entity)))
            {
                //var builderCreatedAt = builder as EntityTypeBuilder<ICreatedAt>;
                builder.Property(p => ((ICreatedAt)p).CreatedAt).IsRequired();
            }

            if (typeof(Entity) is IModifiedAt)
            {
                //var builderModifiedAt = builder as EntityTypeBuilder<IModifiedAt>;
            }

            if (typeof(ISoftDelete).IsAssignableFrom(typeof(Entity)))
            {
                builder.HasQueryFilter(p => ((ISoftDelete)p).DeletedAt == null);
            }

            this.configure(builder);
        }

        protected abstract void configure(EntityTypeBuilder<Entity> builder);
    }
}
