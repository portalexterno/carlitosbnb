﻿namespace Database.Persistence.EntityConfigurations
{
    using Database.Core.Entities;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class CardConfiguration : Configuration<Card>
    {
        protected override void configure(EntityTypeBuilder<Card> builder)
        {
            builder.Property(p => p.Type).HasMaxLength(12).IsRequired();
            builder.Property(p => p.Title).HasMaxLength(80);
            builder.Property(p => p.SubTitle).HasMaxLength(80);
            builder.Property(p => p.Text).HasMaxLength(500);
            builder.Property(p => p.Intent).HasMaxLength(100).IsRequired();
            builder.Property(p => p.Name).HasMaxLength(100);

            builder.HasMany(p => p.Buttons).WithOne(p => p.Card).IsRequired();
            builder.HasMany(p => p.Media).WithOne(p => p.Card).IsRequired();
        }
    }
}
