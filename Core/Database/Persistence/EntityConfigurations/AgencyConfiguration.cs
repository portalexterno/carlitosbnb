﻿namespace Database.Persistence.EntityConfigurations
{
    using Database.Core.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class AgencyConfiguration : Configuration<Agency>
    {
        protected override void configure(EntityTypeBuilder<Agency> builder)
        {
            builder.HasKey(p => p.Code);
            builder.Property(p => p.Code).ValueGeneratedNever();

            builder.Property(p => p.Address).HasMaxLength(150);
            builder.Property(p => p.City).HasMaxLength(19);
            builder.Property(p => p.Departament).HasMaxLength(10);
            builder.Property(p => p.Description).HasMaxLength(50);
            builder.Property(p => p.Fax).HasMaxLength(14);
            builder.Property(p => p.IsActive).IsRequired();
            builder.Property(p => p.Latitude).HasColumnType("decimal(12,9)");
            builder.Property(p => p.Longitude).HasColumnType("decimal(12,9)");
            builder.Property(p => p.Phone1).HasMaxLength(20);
            builder.Property(p => p.Phone2).HasMaxLength(13);
            builder.Property(p => p.PhoneInternal1).HasMaxLength(20);
            builder.Property(p => p.PhoneInternal2).HasMaxLength(13);
            builder.Property(p => p.Promotion).HasMaxLength(3);
            builder.Property(p => p.Schedule).HasMaxLength(81);
            builder.Property(p => p.Ticketing).HasMaxLength(29);
            builder.Property(p => p.TypeAgency);
            builder.Property(p => p.UrlImage).HasMaxLength(250);
        }
    }
}
