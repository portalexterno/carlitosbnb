﻿namespace Database.Persistence.EntityConfigurations
{
    using Database.Core.Entities;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class UserConfiguration : Configuration<User>
    {
        protected override void configure(EntityTypeBuilder<User> builder)
        {
            builder.HasIndex(i => i.Email).IsUnique();
            //builder.HasIndex(i => new { i.FirstName, i.MiddleName, i.LastName }).IsUnique();
            //builder.HasIndex(i => i.FullName).IsUnique();
            builder.HasIndex(i => i.UserName).IsUnique();

            builder.Property(p => p.BirthDay);
            builder.Property(p => p.Email).HasMaxLength(50);
            builder.Property(p => p.FirstName).HasMaxLength(100);
            builder.Property(p => p.FullName).IsRequired().HasMaxLength(300);
            builder.Property(p => p.Gender).HasMaxLength(8);
            builder.Property(p => p.HomeTown).HasMaxLength(50);
            builder.Property(p => p.LastName).HasMaxLength(100);
            builder.Property(p => p.Link).HasMaxLength(300);
            builder.Property(p => p.MiddleName).HasMaxLength(100);
            builder.Property(p => p.Picture).HasMaxLength(200);
            builder.Property(p => p.ServiceUrl).IsRequired();
            builder.Property(p => p.UserName).HasMaxLength(50);

            builder.HasMany(p => p.UnknownIntentions).WithOne(p => p.User).IsRequired();
        }
    }
}