﻿using Database.Core.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Database.Persistence.EntityConfigurations
{
    public class CardMediaConfiguration : Configuration<CardMedia>
    {
        protected override void configure(EntityTypeBuilder<CardMedia> builder)
        {
            builder.Property(p => p.Url).HasMaxLength(250).IsRequired();

            //builder
            //    .HasOne(p => p.Button)
            //    .WithOne(i => i.CardMedia)
            //    .HasForeignKey<CardMedia>(b => b.ButtonId);
        }
    }
}
