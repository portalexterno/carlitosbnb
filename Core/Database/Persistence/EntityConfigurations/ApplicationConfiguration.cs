﻿namespace Database.Persistence.EntityConfigurations
{
    using Database.Core.Entities;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ApplicationConfiguration : Configuration<Application>
    {
        protected override void configure(EntityTypeBuilder<Application> builder)
        {
            builder.HasIndex(p => p.Name).IsUnique();

            builder.Property(p => p.Name).HasMaxLength(20).IsRequired();
            builder.Property(p => p.FacebookId).HasMaxLength(20).IsRequired();
            builder.Property(p => p.Token).HasMaxLength(200).IsRequired();

        }
    }
}
