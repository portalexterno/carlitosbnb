﻿namespace Database.Persistence.EntityConfigurations
{
    using Database.Core.Entities;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class UnknownIntentionConfiguration : Configuration<UnknownIntention>
    {
        protected override void configure(EntityTypeBuilder<UnknownIntention> builder)
        {

        }
    }
}
