﻿namespace Database.Persistence.EntityConfigurations
{
    using Database.Core.Entities;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class CarouselConfiguration : Configuration<Carousel>
    {
        protected override void configure(EntityTypeBuilder<Carousel> builder)
        {
            builder.Property(p => p.Intent).HasMaxLength(100).IsRequired();
            builder.Property(p => p.Text).HasMaxLength(100);
            builder.Property(p => p.Name).HasMaxLength(30);

            builder.HasMany(p => p.Cards).WithOne(p => p.Carousel);
        }
    }
}
