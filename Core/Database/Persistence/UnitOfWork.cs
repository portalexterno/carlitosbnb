﻿namespace Database.Persistence
{
    using Database.Core;
    using Database.Core.Repositories;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Storage;
    using System.Threading.Tasks;

    public class UnitOfWork : IUnitOfWork
    {
        public DbContext Context { get; }

        private bool isTransaction;

        private IDbContextTransaction transaction;

        public UnitOfWork(
            CarlitosBnbContext context,
            IUserRepository users,
            ICardRepository cards,
            ICarouselRepository carousel,
            IAgencyRepository agencies,
            ILogRepository logs,
            IUnknownIntentionRepository unknownIntentions,
            IApplicationRepository applications,
            IApplicationUserRepository applicationUsers,
            bool isTransaction = false)
        {
            this.Context = context;
            this.Users = users;
            this.Cards = cards;
            this.Carousel = carousel;
            this.Agencies = agencies;
            this.Logs = logs;
            this.UnknownIntentions = unknownIntentions;
            this.Applications = applications;
            this.ApplicationUsers = applicationUsers;

            this.isTransaction = isTransaction;
            if (this.isTransaction)
            {
                this.transaction = this.Context.Database.BeginTransaction();
            }
        }

        public IUserRepository Users { get; }

        public ICardRepository Cards { get; }

        public ICarouselRepository Carousel { get; }

        public IAgencyRepository Agencies { get; }

        public ILogRepository Logs { get; }

        public IUnknownIntentionRepository UnknownIntentions { get; }

        public IApplicationRepository Applications { get; }

        public IApplicationUserRepository ApplicationUsers { get; }

        public async Task<int> CompleteAsync()
        {
            return await this.Context.SaveChangesAsync();
        }
    }
}
