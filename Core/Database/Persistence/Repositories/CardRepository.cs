﻿namespace Database.Persistence.Repositories
{
    using Database.Core.Entities;
    using Database.Core.Repositories;
    using Microsoft.EntityFrameworkCore;
    using System.Threading.Tasks;

    public class CardRepository : Repository<Card>, ICardRepository
    {
        public CardRepository(CarlitosBnbContext context) : base(context)
        {
        }

        public async Task<Card> GetByIntent(string intent)
        {
            var q = await this.dbSet.Include(c => c.Media).Include(b => b.Buttons).SingleOrDefaultAsync(u => u.Intent == intent);
            return q;
        }
    }
}
