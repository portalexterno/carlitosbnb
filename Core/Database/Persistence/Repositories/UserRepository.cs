﻿namespace Database.Persistence.Repositories
{
    using Database.Core.Entities;
    using Database.Core.Repositories;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(CarlitosBnbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<User>> GetBetweenAges(int startAge, int endAge)
        {
            var q = await this.Get(u => this.getAge(u.BirthDay.Value) >= startAge && this.getAge(u.BirthDay.Value) <= endAge);
            return q;
        }

        public async Task<IEnumerable<User>> GetByAge(int age)
        {
            var q = await this.Get(u => this.getAge(u.BirthDay.Value) == age);
            return q;
        }

        public async Task<User> GetByChannelId(string facebookId)
        {
            var q = await this.dbSet.Include(a => a.ApplicationUser).FirstOrDefaultAsync(u => u.ApplicationUser.Select(a => a.ChannelAccountId).Contains(facebookId));
            return q;
        }

        public async Task<IEnumerable<User>> GetByGender(string gender)
        {
            var q = await this.Get(u => u.Gender == gender);
            return q;
        }

        public async Task<IEnumerable<User>> GetByPartialName(string partialName)
        {
            var q = await this.Get(u => u.FullName.Contains(partialName));
            return q;
        }

        public async Task<User> GetByUserName(string userName)
        {
            var q = await this.dbSet.FirstOrDefaultAsync(u => u.UserName == userName);
            return q;
        }

        private int getAge(DateTime birthDay)
        {
            var today = DateTime.Today;
            var age = today.Year - birthDay.Year;
            return birthDay > today.AddYears(-age) ? age-- : age;
        }
    }
}