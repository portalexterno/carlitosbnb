﻿namespace Database.Persistence.Repositories
{
    using Database.Core.Entities;
    using Database.Core.Repositories;

    public class UnknownIntentionRepository : Repository<UnknownIntention>, IUnknownIntentionRepository
    {
        public UnknownIntentionRepository(CarlitosBnbContext context) : base(context)
        {
        }
    }
}
