﻿namespace Database.Persistence.Repositories
{
    using Database.Core.Entities;
    using Database.Core.Repositories;
    using Microsoft.EntityFrameworkCore;
    using System.Threading.Tasks;

    public class CarouselRepository : Repository<Carousel>, ICarouselRepository
    {
        public CarouselRepository(CarlitosBnbContext context) : base(context)
        {
        }

        public async Task<Carousel> GetByIntent(string intent)
        {
            var q = await this.dbSet.SingleOrDefaultAsync(u => u.Intent == intent);
            return q;
        }
    }
}
