﻿using Database.Core.Entities;
using Database.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Database.Persistence.Repositories
{
    public class Repository<Entity> : IRepository<Entity>
        where Entity : class
    {
        internal DbSet<Entity> dbSet;

        private DbContext context;

        public Repository(CarlitosBnbContext context)
        {
            this.context = context;
            this.dbSet = this.context.Set<Entity>();
        }

        protected async Task<IEnumerable<Entity>> Get(
            Expression<Func<Entity, bool>> filter = null,
            Func<IQueryable<Entity>, IOrderedQueryable<Entity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<Entity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        public async Task AddAsync(Entity entity)
        {
            try
            {
                if (entity is ICreatedAt)
                {
                    var entityCreatedAt = entity as ICreatedAt;
                    entityCreatedAt.CreatedAt = DateTime.Now;
                }

                if (entity is ISoftDelete)
                {
                    var entitySoftDelete = entity as ISoftDelete;
                    entitySoftDelete.DeletedAt = null;
                }

                await this.dbSet.AddAsync(entity);
            }
            catch (Exception e)
            {
                var c = e;
            }
        }

        public void Update(Entity entity)
        {
            if (entity is IModifiedAt)
            {
                var entityModified = entity as IModifiedAt;
                entityModified.ModifiedAt = DateTime.Now;
            }

            this.context.Update<Entity>(entity);
        }

        public void Disable(Entity entity)
        {
            if (entity is ISoftDelete)
            {
                var entitySoftDelete = entity as ISoftDelete;
                entitySoftDelete.DeletedAt = DateTime.Now;
            }
        }

        public async Task<Entity> GetByIdAsync<Type>(Type id)
        {
            return await this.dbSet.FindAsync(id);
        }

        public async Task<IEnumerable<Entity>> GetAllAsync()
        {
            return await this.dbSet.ToListAsync();
        }
    }
}
