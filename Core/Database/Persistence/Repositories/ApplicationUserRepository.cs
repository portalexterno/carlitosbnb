﻿namespace Database.Persistence.Repositories
{
    using Database.Core.Entities;
    using Database.Core.Repositories;
    using Microsoft.EntityFrameworkCore;
    using System.Threading.Tasks;

    public class ApplicationUserRepository : Repository<ApplicationUser>, IApplicationUserRepository
    {
        public ApplicationUserRepository(CarlitosBnbContext context) : base(context)
        {
        }

        public async Task<ApplicationUser> GetByChannelAccountId(string channelAccountId)
        {
            return await this.dbSet.FirstOrDefaultAsync(c => c.ChannelAccountId == channelAccountId);
        }
    }
}
