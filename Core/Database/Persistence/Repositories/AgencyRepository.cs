﻿namespace Database.Persistence.Repositories
{
    using Database.Core.Entities;
    using Database.Core.Repositories;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class AgencyRepository : Repository<Agency>, IAgencyRepository
    {
        public AgencyRepository(CarlitosBnbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Agency>> GetAgenciesByDepartament(string departament)
        {
            return await this.dbSet.FromSql($"[chatbot].[GetGeoLocationDepartament] @p0, @p1", parameters: new[] { "1", departament }).ToListAsync();
        }

        public async Task<IEnumerable<Agency>> GetAgenciesNearby(string latitude, string longitude)
        {
            return await this.dbSet.FromSql($"[chatbot].[GetGeoLocation] @p0, @p1, @p2", parameters: new[] { latitude, longitude, "1" }).ToListAsync();
        }

        public async Task<IEnumerable<Agency>> GetAtmsByDepartament(string departament)
        {
            return await this.dbSet.FromSql($"[chatbot].[GetGeoLocationDepartament] @p0, @p1", parameters: new[] { "2", departament }).ToListAsync();
        }

        public async Task<IEnumerable<Agency>> GetAtmsNearby(string latitude, string longitude)
        {
            return await this.dbSet.FromSql($"[chatbot].[GetGeoLocation] @p0, @p1, @p2", parameters: new[] { latitude, longitude, "2" }).ToListAsync();
        }
    }
}
