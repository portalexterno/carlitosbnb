﻿namespace Database.Persistence.Repositories
{
    using Database.Core.Entities;
    using Database.Core.Repositories;

    public class LogRepository : Repository<Log>, ILogRepository
    {
        public LogRepository(CarlitosBnbContext context) : base(context)
        {
        }
    }
}
