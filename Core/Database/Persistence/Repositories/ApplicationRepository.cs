﻿namespace Database.Persistence.Repositories
{
    using Database.Core.Entities;
    using Database.Core.Repositories;
    using Microsoft.EntityFrameworkCore;
    using System.Threading.Tasks;

    public class ApplicationRepository : Repository<Application>, IApplicationRepository
    {
        public ApplicationRepository(CarlitosBnbContext context) : base(context)
        {
        }

        public async Task<Application> GetApplicationByName(string name)
        {
            return await this.dbSet.Include(a => a.ApplicationUsers).FirstOrDefaultAsync(u => u.Name == name);
        }
    }
}
