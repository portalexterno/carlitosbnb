﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Agency_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Agencies",
                schema: "chatbot",
                columns: table => new
                {
                    Code = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 50, nullable: true),
                    Address = table.Column<string>(maxLength: 150, nullable: true),
                    Departament = table.Column<string>(maxLength: 10, nullable: true),
                    City = table.Column<string>(maxLength: 19, nullable: true),
                    Schedule = table.Column<string>(maxLength: 81, nullable: true),
                    Ticketing = table.Column<string>(maxLength: 29, nullable: true),
                    Promotion = table.Column<string>(maxLength: 3, nullable: true),
                    Phone1 = table.Column<string>(maxLength: 20, nullable: true),
                    Phone2 = table.Column<string>(maxLength: 13, nullable: true),
                    PhoneInternal1 = table.Column<string>(maxLength: 20, nullable: true),
                    PhoneInternal2 = table.Column<string>(maxLength: 13, nullable: true),
                    Fax = table.Column<string>(maxLength: 14, nullable: true),
                    Longitude = table.Column<decimal>(type: "decimal(12,9)", nullable: false),
                    Latitude = table.Column<decimal>(type: "decimal(12,9)", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TypeAgency = table.Column<int>(nullable: false),
                    UrlImage = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Agencies", x => x.Code);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Agencies",
                schema: "chatbot");
        }
    }
}
