﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Log_Table_User_Relation_Delete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Logs_Users_UserId",
                schema: "chatbot",
                table: "Logs");

            migrationBuilder.DropIndex(
                name: "IX_Logs_UserId",
                schema: "chatbot",
                table: "Logs");

            migrationBuilder.DropColumn(
                name: "UserId",
                schema: "chatbot",
                table: "Logs");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                schema: "chatbot",
                table: "Logs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Logs_UserId",
                schema: "chatbot",
                table: "Logs",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Logs_Users_UserId",
                schema: "chatbot",
                table: "Logs",
                column: "UserId",
                principalSchema: "chatbot",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
