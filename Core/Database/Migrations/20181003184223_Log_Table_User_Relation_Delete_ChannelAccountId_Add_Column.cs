﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Log_Table_User_Relation_Delete_ChannelAccountId_Add_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Logs_Users_UserId",
                schema: "chatbot",
                table: "Logs");

            migrationBuilder.RenameColumn(
                name: "ChannelId",
                schema: "chatbot",
                table: "ApplicationUsers",
                newName: "ChannelAccountId");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                schema: "chatbot",
                table: "Logs",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "ChannelAccountId",
                schema: "chatbot",
                table: "Logs",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Logs_Users_UserId",
                schema: "chatbot",
                table: "Logs",
                column: "UserId",
                principalSchema: "chatbot",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Logs_Users_UserId",
                schema: "chatbot",
                table: "Logs");

            migrationBuilder.DropColumn(
                name: "ChannelAccountId",
                schema: "chatbot",
                table: "Logs");

            migrationBuilder.RenameColumn(
                name: "ChannelAccountId",
                schema: "chatbot",
                table: "ApplicationUsers",
                newName: "ChannelId");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                schema: "chatbot",
                table: "Logs",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Logs_Users_UserId",
                schema: "chatbot",
                table: "Logs",
                column: "UserId",
                principalSchema: "chatbot",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
