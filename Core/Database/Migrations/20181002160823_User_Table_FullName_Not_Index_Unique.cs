﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class User_Table_FullName_Not_Index_Unique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Users_FullName",
                schema: "chatbot",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "FacebookApplicationId",
                schema: "chatbot",
                table: "Applications",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FacebookApplicationId",
                schema: "chatbot",
                table: "Applications");

            migrationBuilder.CreateIndex(
                name: "IX_Users_FullName",
                schema: "chatbot",
                table: "Users",
                column: "FullName",
                unique: true);
        }
    }
}
