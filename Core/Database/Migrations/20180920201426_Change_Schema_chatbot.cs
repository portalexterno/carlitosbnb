﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Change_Schema_chatbot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "chatbot");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "Users",
                newSchema: "chatbot");

            migrationBuilder.RenameTable(
                name: "Carousel",
                newName: "Carousel",
                newSchema: "chatbot");

            migrationBuilder.RenameTable(
                name: "CardsAction",
                newName: "CardsAction",
                newSchema: "chatbot");

            migrationBuilder.RenameTable(
                name: "Cards",
                newName: "Cards",
                newSchema: "chatbot");

            migrationBuilder.RenameTable(
                name: "CardMedia",
                newName: "CardMedia",
                newSchema: "chatbot");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "Users",
                schema: "chatbot",
                newName: "Users");

            migrationBuilder.RenameTable(
                name: "Carousel",
                schema: "chatbot",
                newName: "Carousel");

            migrationBuilder.RenameTable(
                name: "CardsAction",
                schema: "chatbot",
                newName: "CardsAction");

            migrationBuilder.RenameTable(
                name: "Cards",
                schema: "chatbot",
                newName: "Cards");

            migrationBuilder.RenameTable(
                name: "CardMedia",
                schema: "chatbot",
                newName: "CardMedia");
        }
    }
}
