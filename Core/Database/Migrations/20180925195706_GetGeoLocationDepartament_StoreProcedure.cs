﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class GetGeoLocationDepartament_StoreProcedure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = @"
                /****** Object:  StoredProcedure [chatbot].[GetGeoLocationDepartament]    Script Date: 25/9/2018 15:54:25 ******/
                SET ANSI_NULLS ON
                GO
                SET QUOTED_IDENTIFIER ON
                GO
                CREATE PROCEDURE [chatbot].[GetGeoLocationDepartament] 
	                @type int, 
	                @departament varchar(50)
                AS
                BEGIN
	                SELECT Type, Description, Address, Schedule, Latitude, Longitude, UrlImage
	                FROM [chatbot].[agencies]
	                WHERE Type = @type AND UPPER(Departament) = @departament
                END
            ";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sp = "DROP PROCEDURE [chatbot].[GetGeoLocationDepartament]";
            migrationBuilder.Sql(sp);
        }
    }
}
