﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class ApplicationUsers_Table_ChannelAccountId_Column_Maxlength_Max : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ChannelAccountId",
                schema: "chatbot",
                table: "ApplicationUsers",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ChannelAccountId",
                schema: "chatbot",
                table: "ApplicationUsers",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
