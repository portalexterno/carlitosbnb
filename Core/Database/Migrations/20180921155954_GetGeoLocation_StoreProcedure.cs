﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class GetGeoLocation_StoreProcedure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sp = @"
                /****** Object:  StoredProcedure [chatbot].[GetGeoLocation]    Script Date: 21/9/2018 12:21:46 ******/
                SET ANSI_NULLS ON
                GO

                SET QUOTED_IDENTIFIER ON
                GO

                -- =============================================
                -- Author:		<Author,,Name>
                -- Create date: <Create Date,,>
                -- Description:	<Description,,>
                -- =============================================
                CREATE PROCEDURE [chatbot].[GetGeoLocation]
	                -- Add the parameters for the stored procedure here
	                @latitude decimal(12,9) =-16.4987352, 
	                @longitude decimal(12,9)=-68.1339882,
	                @type int,
	                @distance int=1030,
                    @quantity int=50
                AS
                BEGIN
	                -- SET NOCOUNT ON added to prevent extra result sets from
	                -- interfering with SELECT statements.
	                SET NOCOUNT ON;
                --SET FMTONLY OFF;

                DECLARE @orig_lat DECIMAL(12, 9)
                DECLARE @orig_lng DECIMAL(12, 9)
                DECLARE @dist INT
                DECLARE @puntos INT

                SET @orig_lat=@latitude 
                SET @orig_lng=@Longitude
                SET @dist=@distance
                SET @puntos=@quantity

                DECLARE @orig geography = geography::Point(@orig_lat, @orig_lng, 4326);

	                SELECT Type, Description, Address, Schedule, Latitude, Longitude, Distance, UrlImage FROM
                   --SELECT NOMBRE, DIRECCION, HORARIO, LATITUD, LONGITUD, DISTANCE FROM
                   --SELECT * FROM
                   (
                SELECT *,
                    @orig.STDistance(geography::Point(dest.Latitude, dest.Longitude, 4326)) 
                       AS distance

                --FROM PUNTOS as dest
                FROM [chatbot].[agencies] as dest
                Where Type = @type
                ) as MyTable
                WHERE distance < @dist 
                ORDER BY distance
                OFFSET 0 ROWS
                FETCH NEXT @puntos ROWS ONLY
                END
                GO";

            migrationBuilder.Sql(sp);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sp = "DROP PROCEDURE [chatbot].[GetGeoLocation]";
            migrationBuilder.Sql(sp);
        }
    }
}
