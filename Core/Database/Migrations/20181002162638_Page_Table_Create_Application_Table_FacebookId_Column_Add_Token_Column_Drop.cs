﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Database.Migrations
{
    public partial class Page_Table_Create_Application_Table_FacebookId_Column_Add_Token_Column_Drop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FacebookApplicationId",
                schema: "chatbot",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "Token",
                schema: "chatbot",
                table: "Applications");

            migrationBuilder.AddColumn<string>(
                name: "FacebookId",
                schema: "chatbot",
                table: "Applications",
                maxLength: 20,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "Page",
                schema: "chatbot",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    ModifiedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    FacebookId = table.Column<string>(maxLength: 20, nullable: false),
                    Token = table.Column<string>(maxLength: 200, nullable: true),
                    ApplicationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Page", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Page_Applications_ApplicationId",
                        column: x => x.ApplicationId,
                        principalSchema: "chatbot",
                        principalTable: "Applications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Page_ApplicationId",
                schema: "chatbot",
                table: "Page",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_Page_Token",
                schema: "chatbot",
                table: "Page",
                column: "Token",
                unique: true,
                filter: "[Token] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Page",
                schema: "chatbot");

            migrationBuilder.DropColumn(
                name: "FacebookId",
                schema: "chatbot",
                table: "Applications");

            migrationBuilder.AddColumn<string>(
                name: "FacebookApplicationId",
                schema: "chatbot",
                table: "Applications",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Token",
                schema: "chatbot",
                table: "Applications",
                maxLength: 200,
                nullable: false,
                defaultValue: "");
        }
    }
}
