﻿namespace Database.Core
{
    using Database.Core.Repositories;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading.Tasks;

    public interface IUnitOfWork
    {
        DbContext Context { get; }

        IUserRepository Users { get; }

        ICardRepository Cards { get; }

        ICarouselRepository Carousel { get; }

        IAgencyRepository Agencies { get; }

        ILogRepository Logs { get; }

        IUnknownIntentionRepository UnknownIntentions { get; }

        IApplicationRepository Applications { get; }

        IApplicationUserRepository ApplicationUsers { get; }

        Task<int> CompleteAsync();
    }
}
