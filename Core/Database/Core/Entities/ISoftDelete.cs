﻿namespace Database.Core.Entities
{
    using System;

    public interface ISoftDelete
    {
        DateTime? DeletedAt { get; set; }
    }
}