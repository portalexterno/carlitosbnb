﻿namespace Database.Core.Entities
{
    public class Agency
    {
        public int Code { get; set; }

        public int Type { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        public string Departament { get; set; }

        public string City { get; set; }

        public string Schedule { get; set; }

        public string Ticketing { get; set; }

        public string Promotion { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        public string PhoneInternal1 { get; set; }

        public string PhoneInternal2 { get; set; }

        public string Fax { get; set; }

        public decimal Longitude { get; set; }

        public decimal Latitude { get; set; }

        public bool IsActive { get; set; }

        public int TypeAgency { get; set; }

        public string UrlImage { get; set; }
    }
}
