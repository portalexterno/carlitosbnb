﻿namespace Database.Core.Entities
{
    using System.Collections.Generic;

    public class Carousel : Base
    {
        public string Name { get; set; }

        public string Text { get; set; }

        public string Intent { get; set; }

        public ICollection<Card> Cards { get; set; }
    }
}
