﻿namespace Database.Core.Entities
{
    using System.Collections.Generic;

    public class Application : Base
    {
        public string Name { get; set; }

        public string FacebookId { get; set; }

        public string Token { get; set; }

        public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }
    }
}
