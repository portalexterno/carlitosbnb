﻿namespace Database.Core.Entities
{
    public class ApplicationUser
    {
        public int ApplicationId { get; set; }

        public Application Application { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public string ChannelAccountId { get; set; }

        public string ConversationId { get; set; }
    }
}
