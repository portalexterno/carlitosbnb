﻿namespace Database.Core.Entities
{
    public class UnknownIntention : Base
    {
        public User User { get; set; }

        public string Sentence { get; set; }
    }
}
