﻿namespace Database.Core.Entities
{
    using System;

    public interface ICreatedAt
    {
        DateTime CreatedAt { get; set; }
    }
}