﻿namespace Database.Core.Entities
{
    using System.Collections.Generic;

    public class Card : Base
    {
        public string Name { get; set; }

        public string Title { get; set; }

        public string SubTitle { get; set; }

        public string Text { get; set; }

        public string Type { get; set; }

        public string Intent { get; set; }

        public virtual List<CardMedia> Media { get; set; }

        public virtual List<CardAction> Buttons { get; set; }

        public Carousel Carousel { get; set; }
    }
}