﻿namespace Database.Core.Entities
{
    using System;

    public class Log : IIdentity, ICreatedAt
    {
        public int Id { get; set; }

        public string ChannelAccountId { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string Type { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
