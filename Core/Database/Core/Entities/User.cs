﻿namespace Database.Core.Entities
{
    using System;
    using System.Collections.Generic;

    public class User : Base
    {
        //public string FacebookId { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Gender { get; set; }

        public DateTime? BirthDay { get; set; }

        public string Email { get; set; }

        public string HomeTown { get; set; }

        public string Picture { get; set; }

        public string Link { get; set; }

        public string ServiceUrl { get; set; }

        public bool IsContractAccepted { get; set; }

        public virtual List<UnknownIntention> UnknownIntentions { get; set; }

        public virtual ICollection<ApplicationUser> ApplicationUser { get; set; }
    }

    public class DocumentsConsulted : Base
    {
        public int MyProperty { get; set; }
    }
}
