﻿namespace Database.Core.Entities
{
    using System;

    public interface IModifiedAt
    {
        DateTime? ModifiedAt { get; set; }
    }
}