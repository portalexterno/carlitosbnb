﻿namespace Database.Core.Entities
{
    public class CardMedia : Base
    {
        public string Url { get; set; }

        public virtual CardAction CardAction { get; set; }

        public int? CardActionId { get; set; }

        public virtual Card Card { get; set; }

        //public int CardId { get; set; }
    }
}
