﻿namespace Database.Core.Entities
{
    public interface IIdentity
    {
        int Id { get; set; }
    }
}