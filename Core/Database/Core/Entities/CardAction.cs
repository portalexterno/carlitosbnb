﻿namespace Database.Core.Entities
{
    using System;

    public class CardAction : IIdentity, ICreatedAt, IModifiedAt, ISoftDelete
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public string Type { get; set; }

        public string Title { get; set; }

        public string Value { get; set; }

        public int Order { get; set; }

        public virtual Card Card { get; set; }

        public virtual CardMedia CardMedia { get; set; }
    }
}
