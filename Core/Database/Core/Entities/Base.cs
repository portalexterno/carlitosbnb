﻿using System;

namespace Database.Core.Entities
{
    public class Base : IIdentity, ICreatedAt, IModifiedAt, ISoftDelete
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public DateTime? DeletedAt { get; set; }
    }
}
