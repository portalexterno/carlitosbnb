﻿namespace Database.Core.Repositories
{
    using Database.Core.Entities;
    using System.Threading.Tasks;

    public interface IApplicationRepository : IRepository<Application>
    {
        Task<Application> GetApplicationByName(string name);
    }
}
