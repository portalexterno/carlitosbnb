﻿namespace Database.Core.Repositories
{
    using Database.Core.Entities;
    using System.Threading.Tasks;

    public interface ICarouselRepository : IRepository<Carousel>
    {
        Task<Carousel> GetByIntent(string intent);
    }
}
