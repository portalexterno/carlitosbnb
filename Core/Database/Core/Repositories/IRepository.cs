﻿namespace Database.Core.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IRepository<Entity>
    {
        Task<Entity> GetByIdAsync<Type>(Type id);

        Task<IEnumerable<Entity>> GetAllAsync();

        Task AddAsync(Entity entity);

        void Update(Entity entity);

        void Disable(Entity entity);
    }
}
