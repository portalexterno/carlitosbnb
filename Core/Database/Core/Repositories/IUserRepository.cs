﻿namespace Database.Core.Repositories
{
    using Database.Core.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetByChannelId(string channelId);

        Task<IEnumerable<User>> GetByGender(string gender);

        Task<User> GetByUserName(string userName);

        Task<IEnumerable<User>> GetByAge(int age);

        Task<IEnumerable<User>> GetBetweenAges(int startAge, int endAge);

        Task<IEnumerable<User>> GetByPartialName(string partialName);
    }
}
