﻿namespace Database.Core.Repositories
{
    using Database.Core.Entities;

    public interface IUnknownIntentionRepository : IRepository<UnknownIntention>
    {
    }
}
