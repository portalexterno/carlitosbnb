﻿namespace Database.Core.Repositories
{
    using Database.Core.Entities;

    public interface ILogRepository : IRepository<Log>
    {
    }
}
