﻿namespace Database.Core.Repositories
{
    using Database.Core.Entities;
    using System.Threading.Tasks;

    public interface ICardRepository : IRepository<Card>
    {
        Task<Card> GetByIntent(string intent);
    }
}
