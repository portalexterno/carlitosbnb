﻿namespace Database.Core.Repositories
{
    using Database.Core.Entities;
    using System.Threading.Tasks;

    public interface IApplicationUserRepository : IRepository<ApplicationUser>
    {
        Task<ApplicationUser> GetByChannelAccountId(string channelAccountId);
    }
}
