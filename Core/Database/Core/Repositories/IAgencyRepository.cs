﻿namespace Database.Core.Repositories
{
    using Database.Core.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAgencyRepository : IRepository<Agency>
    {
        Task<IEnumerable<Agency>> GetAgenciesNearby(string latitude, string longitude);

        Task<IEnumerable<Agency>> GetAgenciesByDepartament(string departament);

        Task<IEnumerable<Agency>> GetAtmsNearby(string latitude, string longitude);

        Task<IEnumerable<Agency>> GetAtmsByDepartament(string departament);
    }
}
