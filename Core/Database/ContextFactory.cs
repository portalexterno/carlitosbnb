﻿namespace Database
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;

    public class ContextFactory : IDesignTimeDbContextFactory<CarlitosBnbContext>
    {
        //private IConfiguration config;

        //public ContextFactory(IConfiguration config)
        //{
        //    this.config = config;
        //}

        public CarlitosBnbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<CarlitosBnbContext>();

            //builder.UseSqlServer("Data Source=.;Initial Catalog=SampleCarlitosBnb;Integrated Security=True;");
            builder.UseSqlServer(@"Server=tcp:chatbotmigration.database.windows.net,1433;Initial Catalog=ChatBot;Persist Security Info=False;User ID=UsrChatBot;Password=Bull123/;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=3000;");
            return new CarlitosBnbContext(builder.Options);
        }
    }
}
