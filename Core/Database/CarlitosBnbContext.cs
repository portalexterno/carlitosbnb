﻿namespace Database
{
    using Database.Core.Entities;
    using Database.Persistence.EntityConfigurations;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Linq;

    public class CarlitosBnbContext : DbContext
    {
        public CarlitosBnbContext(DbContextOptions options)
            : base(options)
        {
            this.ChangeTracker.AutoDetectChangesEnabled = false;
            this.ChangeTracker.LazyLoadingEnabled = false;
            //this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            //this.Users.AsNoTracking();
            //this.Applications.AsNoTracking();
            //this.ApplicationUsers.AsNoTracking();
            //this.Cards.AsNoTracking();
            //this.CardMedia.AsNoTracking();
            //this.Agencies.AsNoTracking();
            //this.CardsAction.AsNoTracking();
            //this.Carousel.AsNoTracking();
            //this.Logs.AsNoTracking();
            //this.UnknownIntention.AsNoTracking();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("chatbot");
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new CardActionConfiguration());
            modelBuilder.ApplyConfiguration(new CardMediaConfiguration());
            modelBuilder.ApplyConfiguration(new CardConfiguration());
            modelBuilder.ApplyConfiguration(new CarouselConfiguration());
            modelBuilder.ApplyConfiguration(new AgencyConfiguration());
            modelBuilder.ApplyConfiguration(new LogConfiguration());
            modelBuilder.ApplyConfiguration(new UnknownIntentionConfiguration());
            modelBuilder.ApplyConfiguration(new ApplicationConfiguration());
            modelBuilder.ApplyConfiguration(new ApplicationUserConfiguration());
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Card> Cards { get; set; }

        public DbSet<CardAction> CardsAction { get; set; }

        public DbSet<CardMedia> CardMedia { get; set; }

        public DbSet<Carousel> Carousel { get; set; }

        public DbSet<Agency> Agencies { get; set; }

        public DbSet<Log> Logs { get; set; }

        public DbSet<UnknownIntention> UnknownIntention { get; set; }

        public DbSet<Application> Applications { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        public override int SaveChanges()
        {
            //addTimestamps();
            return base.SaveChanges();
        }

        private void addTimestamps()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => (x.Entity is ICreatedAt || x.Entity is IModifiedAt) && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in entities)
            {
                var now = DateTime.Now;

                if (entity.State == EntityState.Added)
                {
                    ((ICreatedAt)entity.Entity).CreatedAt = now;
                }
                else
                {
                    ((IModifiedAt)entity.Entity).ModifiedAt = now;
                }
            }
        }
    }

    public class LogFactory : ILoggerFactory
    {
        public void AddProvider(ILoggerProvider provider)
        {
            throw new NotImplementedException();
        }

        public ILogger CreateLogger(string categoryName)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
