using Database.Core;
using Database.Core.Entities;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTestDatabase
{
    public class UnitWorkTests : IClassFixture<UnitOfWorkFixture>
    {
        private readonly UnitOfWorkFixture fixture;

        public UnitWorkTests(UnitOfWorkFixture fixture)
        {
            this.fixture = fixture;
            this.userSample = new User()
            {
                BirthDay = new DateTime(1990, 5, 28),
                Email = "joelaltuzarra@gmail.com",
                //FacebookId = "123156456",
                FirstName = "Joel",
                FullName = "Joel Celso Altuzarra Alarcon",
                Gender = "male",
                HomeTown = "La Paz, Bolivia",
                LastName = "Altuzarra Alarcon",
                Link = "https://www.facebook.com/morkterror",
                MiddleName = "Celso",
                Picture = "https://www.facebook.com/morkterror",
                ServiceUrl = "https://www.facebook.com/morkterror",
                UserName = "morkterror"
            };
        }

        private User userSample;

        [Fact]
        public async Task CanAddUserToDatabase()
        {
            //System.Transactions.TransactionManager.beginTransaction();
            // Arrange           

            IEnumerable<User> usersInDatabase;
            int commits = 0;

            // Act

            using (IUnitOfWork unitOfWOrk = fixture.CreateUnitOfWork())
            {
                await unitOfWOrk.Users.AddAsync(this.userSample);
                commits += await unitOfWOrk.CompleteAsync();
                usersInDatabase = await unitOfWOrk.Users.GetAllAsync();
            }

            // Assert
            Assert.Equal(1, commits);
            Assert.Single(usersInDatabase);
        }

        [Fact]
        public async Task CanModifyUserFromDataBase()
        {
            User userFound = null;

            using (IUnitOfWork unitOfWork = fixture.CreateUnitOfWork())
            {
                await unitOfWork.Users.AddAsync(this.userSample);
                await unitOfWork.CompleteAsync();

                var userInDatabase = await unitOfWork.Users.GetByAge(28);
                var user = userInDatabase.FirstOrDefault();
                user.UserName = "morkterror2";
                await unitOfWork.CompleteAsync();
                userFound = await unitOfWork.Users.GetByUserName("morkterror2");
            }

            Assert.NotNull(userFound);
        }

        [Fact]
        public async Task CanAddCardToDatabase()
        {
            var commit = 0;

            var unitOfWork = fixture.CreateUnitOfWork();
            var card = new Card()
            {
                Buttons = new List<CardAction>()
                {
                    new CardAction()
                    {
                        Title = "Cercanas a m�",
                        Type = "imBack",
                        Value = "{ 'Dialog': 'AgenciesNearMe' }"
                    }
                },
                Media = new List<CardMedia>()
                {
                    new CardMedia()
                    {
                        Url = "http://carlitosbotbnbproduccion.azurewebsites.net/Content/images/Agencias/agencia.png"
                    }
                },
                Text = "",
                SubTitle = "",
                Title = "encontre estas opciones para t�",
                Type = "HERO_CARD",
                Intent = "292430b3-30eb-4ef4-8888-2e657f4811db"
            };

            await unitOfWork.Cards.AddAsync(card);
            commit += await unitOfWork.CompleteAsync();

            var cards = await unitOfWork.Cards.GetAllAsync();

            unitOfWork.Dispose();

            //Assert.Equal(3, commit);
            //Assert.Single(cards);
        }

        [Fact]
        public async Task CanCreateCarousel()
        {
            var unitOfWork = fixture.CreateUnitOfWork();

            var carousel = new Carousel();
            carousel.Intent = "448aebaa-239d-4f5e-a3e8-2fcd718daaee";
            carousel.Name = "Men� de Creditos";
            carousel.Text = "Encontr� estas opciones para t�";
            carousel.Cards = new List<Card>()
            {
                new Card()
                {
                    Intent = "0e048e04-a348-42ee-a020-68fcb077f2f6",
                    Type = "HERO_CARD",
                    Buttons = new List<CardAction>()
                    {
                        new CardAction()
                        {
                            Title = "Credito de Consumo",
                            Type = "postBack",
                            Value = "Creditos de Consumo"
                        },
                        new CardAction()
                        {
                            Title = "Tarjetas de Cr�dito",
                            Type = "postBack",
                            Value = "Tarjetas de cr�dito"
                        },
                        new CardAction()
                        {
                            Title = "Cr�dito Vehicular",
                            Type = "postBack",
                            Value = "Cr�dito Vehicular"
                        },
                        new CardAction()
                        {
                            Title = "Cr�dito Vivienda",
                            Type = "postBack",
                            Value = "Cr�dito Vivienda"
                        },
                        new CardAction()
                        {
                            Title = "L�nea de Cr�dito",
                            Type = "postBack",
                            Value = "L�nea de Cr�dito"
                        },
                        new CardAction()
                        {
                            Title = "FAQs",
                            Type = "postBack",
                            Value = "FAQs"
                        }
                    },
                    Media = new List<CardMedia>()
                    {
                        new CardMedia()
                        {
                            Url = "https://picsum.photos/200/300"
                        }
                    }
                }
            };

            await unitOfWork.Carousel.AddAsync(carousel);
            await unitOfWork.CompleteAsync();
            unitOfWork.Dispose();
        }

        [Fact]
        public async Task GetAgencies()
        {
            var unitOfWork = fixture.CreateUnitOfWork();
            var foundAgencies = await unitOfWork.Agencies.GetAgenciesNearby("-16.4987352", "-68.1339882");
            var r = foundAgencies;
        }
    }
}
