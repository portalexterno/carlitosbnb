﻿using Database;
using Database.Core;
using Database.Persistence;
using Database.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace XUnitTestDatabase
{
    public sealed class UnitOfWorkFixture : IDisposable
    {
        private readonly ICollection<IUnitOfWork> unitsOfWork = new HashSet<IUnitOfWork>();

        public IUnitOfWork CreateUnitOfWork([CallerMemberName] string databaseName = "SampleCarlitosBnb")
        {
            try
            {
                DbContextOptions<CarlitosBnbContext> options = new DbContextOptionsBuilder<CarlitosBnbContext>()
                .UseSqlServer("Data Source=.;Initial Catalog=SampleCarlitosBnb;Integrated Security=True;")
                //.UseInMemoryDatabase("TestDatabase")
                .Options;

                var context = new CarlitosBnbContext(options);

                var unitOfWork = new UnitOfWork(context, new UserRepository(context), new CardRepository(context), new CarouselRepository(context), new AgencyRepository(context), new LogRepository(context), new UnknownIntentionRepository(context), new ApplicationRepository(context), new ApplicationUserRepository(context), false);

                unitsOfWork.Add(unitOfWork);
                return unitOfWork;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void Dispose()
        {
            foreach (IUnitOfWork unitOfWork in unitsOfWork)
            {
                unitOfWork.Dispose();
            }
        }
    }
}
